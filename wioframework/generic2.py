# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- coding: utf-8 -*-
import sys
from django import forms
from django.template.loader import render_to_string
from settings import SITE_URL
import datetime

from django.db import models
try:
    from django.utils import six
except ImportError:
    import six


from wioframework import wjson as json


class GenericPkNew(models.Field):    
    def __init__(base_pk_field, old_gpk_field=None ):
        self.base_pk_field=base_pk_field
        self.old_gpk_field=old_gpk_field
        self.editable=False
        
    def contribute_to_class(self,tc,name):
        setattr(tc,name,property(self.get,self.set))
        
    def get(self,instance):
        from django.apps.registry import apps
        get_models = apps.get_models
        models=get_models()
        bpk=getattr(instance,self.base_pk_field)
        if not '#' in bpk:
            ## UPGRADE TO GPK NEW
            gpko=None
            if self.old_gpk_field:
               gpko=getattr(instance,self.old_gpk_field)
            if gpko==None:
                ## EXHAUSTIVE SEARCH
                for m in models:
                   qs=m.objects.filter(id=bpk)     
                   if qs.count()!=0:
                     gpko=qs[0]
            mdlid,objectid=gpko.class_id,gpko.id
            setattr(instance,self.base_pk_field,"%s#%s"%(mdlid,objectid))
            instance.save(fields=[self.base_pk_field])
        mdlid,objectid='#'.split(bpk)
        return self.model_col_get(mdlid).objects.get(id=objectid)

    