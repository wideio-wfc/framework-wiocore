# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
from django.core.files.uploadhandler import *

import uuid
import sys
import os
import traceback

from django.core.files.base import File

import  settings

if settings.USE_ZMQ:
    import wuploader

    class ZMQNetPipeFile(File):

        """
        A abstract uploaded file (``TemporaryUploadedFile`` and
        ``InMemoryUploadedFile`` are the built-in concrete subclasses).

        An ``UploadedFile`` object behaves somewhat like a file object and
        represents some file data that the user submitted with a form.
        """
        DEFAULT_CHUNK_SIZE = 64 * 2 ** 10

        def __init__(self, file=None, name=None, content_type=None,
                     size=None, charset=None, content_type_extra=None):
            super(ZMQNetPipeFile, self).__init__(file, name)
            self.size = size
            self.content_type = content_type
            self.charset = charset
            self.content_type_extra = content_type_extra

        def __repr__(self):
            return str("<%s: %s (%s)>" % (
                self.__class__.__name__, self.name, self.content_type))

        def _get_name(self):
            return self._name

        def _set_name(self, name):
            # Sanitize the file name so that it can't be dangerous.
            if name is not None:
                # Just use the basename of the file -- anything else is
                # dangerous.
                name = os.path.basename(name)

                # File names longer than 255 characters can cause problems on
                # older OSes.
                if len(name) > 255:
                    name, ext = os.path.splitext(name)
                    ext = ext[:255]
                    name = name[:255 - len(ext)] + ext

            self._name = name

        def save():
            # we don't do anything here as the file is not really accessible
            # for queries...
            pass

        name = property(_get_name, _set_name)

    class ZMQNetPipeUploadHandler(FileUploadHandler):

        """
        File uploader that transmits file directly to a target compute node.
        Ideally the target compute node is the one that will be used to perform the computations.
        Otherwise an host "near" than one.
        """

        def match_netpipe(self, x):
            return x.startswith("/compute/execrequest/add/")

        def handle_raw_input(
                self, input_data, META, content_length, boundary, encoding=None):
            """
            Use the content_length to signal whether or not this handler should be in use.
            """
            from cloud.models import Node
            self.netpipemode = self.match_netpipe(META["PATH_INFO"])
            if self.netpipemode:
                try:
                    sys.stderr.write("using NETPIPE uploader\n")
                    sys.stderr.write(
                        "Pyro stack : %r\n" %
                        (traceback.format_tb(
                            sys.exc_info()[2],)))
                    if (not hasattr(self.request, "predefined_id")):
                        self.request.predefined_id = str(uuid.uuid1())
                    self.enckey = self.request.COOKIES.get(
                        "WIOENCKEY",
                        "enckey")
                    self.zmq_context = wuploader.get_zmq_context()
                    self.zsocket = self.zmq_context.socket(wuploader.zmq.REQ)
                    self.zsocket.setsockopt(wuploader.zmq.SNDTIMEO, 500)
                    self.zsocket.setsockopt(wuploader.zmq.RCVTIMEO, 500)
                    # self.request.get_request_container()
                    self.request_container = "arbitrary"
                    self.dhost = (
                        Node.objects.filter(
                            basetype='co').order_by('-last_ping'))[0].hostip  # request_container0
                    self.dport = 10000
                    self.zsocket.connect(
                        "tcp://%s:%s" %
                        (self.dhost, self.dport,))
                except Exception as e:
                    sys.stderr.write("error : %s\n" % (repr(e),))
            else:
                self.zsocket = None
                sys.stderr.write("not using NETPIPE uploader\n")
                return

        def new_file(self, field_name, file_name, content_type,
                     content_length, charset=None, content_type_extra=None):
            #sys.stderr.write("NETPIPE uploader new file %s\n"%(file_name,))
            if self.netpipemode:
                try:
                    self.field_name = field_name
                    self.file_name = file_name
                    self.content_type = content_type
                    self.charset = charset
                    self.content_type_extra = content_type_extra
                except Exception as e:
                    sys.stderr.write(
                        "[NetPipeUploadHandler] error : %s\n" %
                        (repr(e),))
                raise StopFutureHandlers()
            else:
                pass

        def receive_data_chunk(self, raw_data, start):
            """
            Add the data to the BytesIO file.
            """
            if self.netpipemode:
                #sys.stderr.write("uploader A\n")
                try:
                    wuploader.send_bucket(
                        self.zsocket,
                        self.file_name,
                        self.request.predefined_id,
                        start,
                        self.enckey,
                        raw_data)  # self.request_container
                    #sys.stderr.write("uploader B\n")
                    self.zsocket.recv()
                    #sys.stderr.write("uploader C\n")
                except Exception as e:
                    sys.stderr.write(
                        "[NetPipeUploadHandler] error : %s\n" %
                        (repr(e),))
                #raise StopFutureHandlers()
            else:
                return raw_data

        def file_complete(self, file_size):
            """
            Return a file object if we're activated.
            """
            if self.netpipemode:
                #raise StopFutureHandlers()
                return ZMQNetPipeFile(None, self.file_name)
            else:
                pass

        def __del__(self):
            if (self.zsocket is not None):
                self.zsocket.close()

if settings.USE_PYRO:
    import Pyro4
    Pyro4.config.SERIALIZER = "pickle"
    Pyro4.config.COMMTIMEOUT = 1.5

    class PyroNetPipeFile(File):

        """
        A abstract uploaded file (``TemporaryUploadedFile`` and
        ``InMemoryUploadedFile`` are the built-in concrete subclasses).

        An ``UploadedFile`` object behaves somewhat like a file object and
        represents some file data that the user submitted with a form.
        """
        DEFAULT_CHUNK_SIZE = 64 * 2 ** 10

        def __init__(self, file=None, name=None, content_type=None,
                     size=None, charset=None, content_type_extra=None):
            super(PyroNetPipeFile, self).__init__(file, name)
            self.size = size
            self.content_type = content_type
            self.charset = charset
            self.content_type_extra = content_type_extra

        def __repr__(self):
            return str("<%s: %s (%s)>" % (
                self.__class__.__name__, self.name, self.content_type))

        def _get_name(self):
            return self._name

        def _set_name(self, name):
            # Sanitize the file name so that it can't be dangerous.
            if name is not None:
                    # Just use the basename of the file -- anything else is
                    # dangerous.
                name = os.path.basename(name)

                # File names longer than 255 characters can cause problems on
                # older OSes.
                if len(name) > 255:
                    name, ext = os.path.splitext(name)
                    ext = ext[:255]
                    name = name[:255 - len(ext)] + ext

            self._name = name

        def save():
            # we don't do anything here as the file is not really accessible
            # for queries...
            pass

        name = property(_get_name, _set_name)

    class PyroNetPipeUploadHandler(FileUploadHandler):

        """
        File uploader that transmits file directly to a target compute node.
        Ideally the target compute node is the one that will be used to perform the computations.
        Otherwise an host "near" than one.
        """

        def match_netpipe(self, x):
            return x.startswith("/compute/execrequest/add/")

        def handle_raw_input(
                self, input_data, META, content_length, boundary, encoding=None):
            """
            Use the content_length to signal whether or not this handler should be in use.
            """
            from cloud.models import Node
            self.netpipemode = self.match_netpipe(META["PATH_INFO"])
            if self.netpipemode:
                try:
                    sys.stderr.write("using PyroNETPIPE uploader\n")
                    sys.stderr.write(
                        "Pyro stack1 : %r\n" %
                        (traceback.format_tb(
                            sys.exc_info()[2],)))
                    if hasattr(self, "request") and self.request is not None:
                        if (not hasattr(self.request, "predefined_id")):
                            self.request.predefined_id = str(uuid.uuid1())
                        self.request.DISABLE_AFILES = True
                        print "XPyroFileUploadHandler!!! request!=None"
                        self.debug_file = open("/tmp/testpic", "wb")
                        self.dhost = (
                            Node.objects.filter(
                                basetype='co').order_by('-last_ping'))[0]  # request_container0
                        # FIXME SCHEDULER SHOULD BE BASED ON FREE BUCKETS...
                        # (or RANDOM ON AVAILABLE BUCKETS)
                        sys.stderr.write(
                            "[PyroNetPipeUploadHandler] connecting to " +
                            self.dhost._metadata['ftd_uri'] +
                            "\n")
                        self.proxy = Pyro4.Proxy(
                            self.dhost._metadata['ftd_uri'])
                        self.proxy._pyroHmacKey = settings.HMACKEY
                        self.bucketid, self.bucketkey = self.proxy.allocate_bucket()
                        self.request.buckethostid = self.dhost.id
                        self.request.bucketid = self.bucketid
                        self.request.bucketkey = self.bucketkey
                        self.request.buckethostip = self.dhost.hostip
                    else:
                        sys.stderr.write(
                            "[PyroNetPipeUploadHandler] error0 : no request in uploader\n")
                except Exception as e:
                    sys.stderr.write(
                        "Pyro stack2 : %r\n" %
                        (traceback.format_tb(
                            sys.exc_info()[2],)))
                    sys.stderr.write(
                        "[PyroNetPipeUploadHandler] error1 : %s\n" %
                        (repr(e),))
                    if not hasattr(self.request,"add_metadata"):
                        self.request={'err':'upload err'}
                    else:
                        self.request.add_metadata["upload_error"]="upload error pyro transfer 1" 
            else:
                self.zsocket = None
                sys.stderr.write("not using NETPIPE uploader\n")
                return

        def new_file(self, field_name, file_name, content_type,
                     content_length, charset=None, content_type_extra=None):
            #sys.stderr.write("NETPIPE uploader new file %s\n"%(file_name,))
            if self.netpipemode:
                try:
                    self.field_name = field_name
                    self.file_name = file_name
                    self.content_type = content_type
                    self.charset = charset
                    self.content_type_extra = content_type_extra
                except Exception as e:
                    sys.stderr.write(
                        "[NetPipeUploadHandler] error : %s\n" %
                        (repr(e),))
                raise StopFutureHandlers()
            else:
                pass

        def receive_data_chunk(self, raw_data, start):
            """
            Add the data to the BytesIO file.
            """
            if self.netpipemode:
                # sys.stderr.write("uploader A\n")
                try:
                    # wuploader.send_bucket(self.zsocket, self.file_name,
                    # self.request.predefined_id,start, self.enckey, raw_data)
                    # # self.request_container
                    if self.debug_file is not None:
                        self.debug_file.seek(start, 0)
                        self.debug_file.write(raw_data)
                    self.proxy.block_io(
                        self.bucketid,
                        self.file_name,
                        start,
                        "write",
                        raw_data)
                except Exception as e:
                    sys.stderr.write(
                        "[PyroNetPipeUploadHandler] error2 : %s\n" %
                        (repr(e),))
                    if not hasattr(self.request,"add_metadata"):
                        self.request={'err':'upload err'}
                    else:
                        self.request.add_metadata["upload_error"]="upload error pyro transfer 2"
                #raise StopFutureHandlers()
            else:
                return raw_data

        def file_complete(self, file_size):
            """
            Return a file object if we're activated.
            """
            if hasattr(self, "debug_file") and self.debug_file is not None:
                self.debug_file.close()
                self.debug_file = None
            if self.netpipemode:
                #raise StopFutureHandlers()
                return PyroNetPipeFile(None, self.file_name)
            else:
                pass

        def __del__(self):
            if (self.zsocket is not None):
                self.proxy.lock_bucket(self.bucketid)
                del self.proxy
