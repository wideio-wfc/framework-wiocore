# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
from wioframework.serializers.django_json import DjangoJSONEncoder
import django.db.models.query
import django.db.models.options
import json
import datetime
from django.forms import Form, ModelForm, BaseForm


class StartYnJSONEncoder(DjangoJSONEncoder):

    """
    JSONEncoder subclass that knows how to encode date/time and decimal types.
    """

    DATE_FORMAT = "%Y-%m-%d"
    TIME_FORMAT = "%H:%M:%S"

    def default(self, o, *args, **kwargs):
        if isinstance(o, django.db.models.query.QuerySet):
            mdl = o.model
            if (o.count() > 1024):
                raise Exception(
                    str(mdl) +
                    "More than 1024 elements queried, you should paginate your requests")
            if (not(hasattr(mdl, "API")) or not mdl.API):
                raise Exception(str(mdl) + " does not make part of the API")
            if (hasattr(mdl, "API_Form_Class")):
                formclass = mdl.API_Form_Class
            else:
                formclass = mdl.__name__ + "Form"
            module = mdl.__module__.split('.')[:-1]
            mfc = __import__(".".join(module) + "." + 'forms', fromlist=module)
            fc = eval('m.' + formclass, {'m': mfc})
            r = []
            for i in o:
                idi0 = i.__dict__
                f = fc(idi0)
                idi = {}
                for k in f.fields.keys():
                    if k in idi0:
                        idi[k] = idi0[k]
                idi["id"] = i.id
                x = idi
                r.append(x)
            return r

        elif isinstance(o, BaseForm):
            f = o
            idi = {}
            for k in f.fields.keys():
               # if (k not in ["BoundField"]):
                idi[k] = f[k].data
            return idi
        elif isinstance(o, django.db.models.Model):
            mdl = o
            if (not(hasattr(mdl, "API")) or not mdl.API):
                raise Exception(str(mdl) + " does not make part of the API")
            if (hasattr(mdl, "API_Form_Class")):
                formclass = mdl.API_Form_Class
            else:
                if hasattr(mdl, "__name__"):
                    formclass = mdl.__name__ + "Form"
                else:
                    formclass = None
            if (isinstance(formclass, str)):
                module = mdl.__module__.split('.')[:-1]
                mfc = __import__(
                    ".".join(module) +
                    "." +
                    'forms',
                    fromlist=module)
                fc = eval('m.' + formclass, {'m': mfc})
            else:
                fc = formclass
            if ((fc is None) or (fc.__name__ == "TForm") or (hasattr(mdl, "WIDEIO_Meta") and hasattr(
                    mdl.WIDEIO_Meta, "API_EXPORT_ALL") and mdl.WIDEIO_Meta.API_EXPORT_ALL)):
                EL = []
                if hasattr(mdl, "WIDEIO_Meta") and hasattr(
                        mdl.WIDEIO_Meta, "API_EXPORT_EXCLUDE"):
                    EL = getattr(mdl.WIDEIO_Meta, "API_EXPORT_EXCLUDE")
                idi = idi0 = dict(
                    filter(
                        lambda i: (
                            (i[0][0] != "_") and (
                                i[0] not in EL)),
                        o.__dict__.items()))
            else:
                idi0 = o.__dict__  # ALL THE FIELDS
                f = fc(idi0)
                idi = {}
                for k in f.fields.keys():
                    if k in idi0:
                        idi[k] = idi0[k]  # COPY ONLY THE FIELDS OF THE FORM
                idi["_id"] = "B" + str(fc)
            idi["id"] = o.id

            return idi
            # r.append(x)
            # r=fc(o).data
            # return unicode(r)
        elif isinstance(o, django.db.models.options.Options):
            return "OPTION"
        else:
            #raise Exception, self.__dict__
            try:
                return super(StartYnJSONEncoder, self).default(
                    o, *args, **kwargs)
            except Exception as e:
                return {'$nonserializable$': (repr(o), repr(e))}
