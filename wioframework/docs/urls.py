from django.conf.urls import *
from wioframework import utilviews

import views

urlpatterns=patterns('docs.views',
         *utilviews.autourls(views)
)
