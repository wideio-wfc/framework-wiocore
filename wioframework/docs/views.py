import json
import logging
from django.conf import settings
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response, RequestContext
from django.contrib import messages
from django.db.models.fields.related import ForeignKey
import settings
from docutils.core import publish_parts
from django.db.models.fields.files import FileField


def help_modules(request):
    """
    This view return the list of WIDE IO applications and explain succinctly what they do
    """
    LA = []

    blacklist = ['countries', 'backoffice', 'network', 'payment', 'django_cron', 'monitoring',
                 'registration', 'accounts', 'challenges', 'wioframework', 'evaluation']

    for a in settings.INSTALLED_APPS:
        for b in blacklist:
            if a.startswith(b):
                a = ""
                break
        if a:
            try:
                m = __import__(a + ".models", fromlist=a.split("."))
                if hasattr(m, "MODELS"):
                    m2 = __import__(a, fromlist=a.split(".")[:-1])
                    docstr = m2.__doc__
                    if docstr == None:
                        docstr = ""
                    LA.append((m.__name__.split('.')[0], m, publish_parts(docstr, writer_name='html')['body']))

            except ImportError, e:
                logging.warning("autodoc faced exception opening model for module %s :%r" % (a, e))
    context = {
        'modules': LA,
        'request': request,
    }
    return render_to_response("help/modules.html", context, RequestContext(request))


help_modules.default_url = 'modules'


class HelpModel:
    @classmethod
    def _process_model_build_curl(cls, request, modulename, model, fields):
        docs = []
        audlv = {}
        # CURL
        if len(fields['file']) == 0:
            audlv.update({
                             "add": "curl -X POST -u " + request.user.username + ":password " + " -H Content-Type:application/json -F " + "-F ".join(
                                 [e + "=value " for e in fields['_']]) + " " + settings.SITE_URL[
                                                                               :-1] + model.get_base_url() + "/add/"})
        else:
            audlv.update({
                             "add": "curl -X POST -u " + request.user.username + ":password " + " -H Content-Type:application/json -F " + "-F ".join(
                                 [e + "=value " for e in fields['_']]) + " -F " + "-F ".join(
                                 [e + "=@path " for e in fields['file']]) + " " + settings.SITE_URL[
                                                                                  :-1] + model.get_base_url() + "/add/"})
        audlv["add"] = '<pre data-lang="shell">' + audlv["add"] + '</pre>'

        audlv.update({
                         "view": "curl -u " + request.user.username + ":password -H Content-Type:application/json " + settings.SITE_URL[
                                                                                                                      :-1] + model.get_base_url() + "/:ID/"})
        audlv["view"] = '<pre data-lang="shell">' + audlv["view"] + '</pre>'

        audlv.update({
                         "list": "curl -u " + request.user.username + ":password -H Content-Type:application/json " + settings.SITE_URL[
                                                                                                                      :-1] + model.get_base_url() + "/list/"})

        if len(fields['file']) == 0:
            audlv.update({
                             "update": "curl -X POST -u " + request.user.username + ":password " + " -H Content-Type:application/json -F " + "-F ".join(
                                 [e + "=value " for e in fields['_']]) + " " + settings.SITE_URL[:-1] + model.get_base_url() + "/update/:ID/"})
        else:
            audlv.update({
                             "update": "curl -X POST -u " + request.user.username + ":password " + " -H Content-Type:application/json -F " + "-F ".join(
                                 [e + "=value " for e in fields['_']]) + " -F " + "-F ".join(
                                 [e + "=@path " for e in fields['file']]) + " " + settings.SITE_URL[:-1] + model.get_base_url() + "/update/:ID/"})


        audlv["update"] = '<pre data-lang="shell">' + audlv["update"] + '</pre>'

        audlv.update({
                         "view": "curl -u " + request.user.username + ":password -H Content-Type:application/json " + settings.SITE_URL[
                                                                                                                      :-1] + model.get_base_url() + "/:ID/"})



        audlv["list"] = '<pre data-lang="shell">' + audlv["list"] + '</pre>'

        audlv.update({
                         "del": "curl -X POST -u " + request.user.username + ":password -H Content-Type:application/json " + settings.SITE_URL[
                                                                                                                     :-1] + model.get_base_url() + "/del/ID/"})
        audlv["del"] = '<pre data-lang="shell">' + audlv["del"] + '</pre>'

        audlv.update({"isotherviews": False})
        if hasattr(model.WIDEIO_Meta, "other_views"):
            audlv["isotherviews"] = True
            audlv.update({"otherviews": []})
            for view in model.WIDEIO_Meta.other_views:
                f = map(lambda x: x[0], view["inputs"])
                audlv["otherviews"].append((view["name"], view["doc"],
                                            '<pre data-lang="shell">curl  -X POST -u ' + request.user.username + ':password -H Content-Type:application/json -F ' + "-F ".join(
                                                [e + "=value " for e in f]) + " " + settings.SITE_URL[:-1] + view[
                                                "url"] + '</pre>'))

        docs.append(("CURL API", audlv))
        return docs

    @classmethod
    def _process_model_build_python(cls, request, modulename, model, fields):
        docs = []
        audlv = {}
        name = model.__name__
        if len(fields['file']) == 0:
            audlv.update(
                {"add": "import wideio\nwideio." + name.lower() + ".add(" + ", ".join([e + "=value" for e in fields['_']]) + ")"})
        else:
            audlv.update({"add": "import wideio\nwideio." + name.lower() + ".add(" + ", ".join(
                [e + "=value" for e in fields['_']]) + ", " + ", ".join(
                [e + "=@path" for e in fields['python']]) + ")"})

        audlv["add"] = '<pre data-lang="python">' + audlv["add"] + '</pre>'

        audlv.update({"view": "import wideio\nwideio." + name.lower() + ".view(ID)"})
        audlv["view"] = '<pre data-lang="python">' + audlv["view"] + '</pre>'

        audlv.update({"list": "import wideio\nwideio." + name.lower() + ".all()"})
        audlv["list"] = '<pre data-lang="python">' + audlv["list"] + '</pre>'

        audlv.update({"del": "import wideio\nwideio." + name.lower() + ".delete(ID)"})
        audlv["del"] = '<pre data-lang="python">' + audlv["del"] + '</pre>'

        audlv.update({"isotherviews": False})
        if hasattr(model.WIDEIO_Meta, "other_views"):
            audlv["isotherviews"] = True
            audlv.update({"otherviews": []})
            for view in model.WIDEIO_Meta.other_views:
                f = map(lambda x: x[0], view["inputs"])
                audlv["otherviews"].append((view["name"], view["doc"],
                                            '<pre data-lang="python">>>> wideio.' + name.lower() + '.' + view[
                                                "name"] + '(' + ", ".join([e + "=value" for e in f]) + ')</pre>'))

        docs.append(("Python API", audlv))
        return docs

    @classmethod
    def _process_model_build_results(cls, request, modulename, model, fields):
        docs = []
        audlv = {}

        prequest=request
        # class Puser:
        #     username=request.user.username
        #     def is_anonymous(self):
        #         return False
        #     def is_admin(self):
        #         return False
        #     def is_superuser(self):
        #         return False
        #     def is_staff(self):
        #         return False
        #
        # prequest = request
        # prequest.user = Puser()

        # RESULT
        res = {"res": "ID"}
        documentation = []

        if getattr(model.WIDEIO_Meta, "permissions", True) is True:# FIXME(add test): or model.WIDEIO_Meta.permissions["add"](prequest):
            audlv.update({"add": json.dumps(res, sort_keys=True, indent=4, separators=(',', ': '))})
            audlv["add"] = '<pre data-lang="javascript">' + audlv["add"] + '</pre>'
            audlv.update({"add_doc": documentation})

        res = {}
        documentation = []

        res.update(dict(map(lambda x: (x, "value"), fields['all'])))
        if hasattr(model.WIDEIO_Meta, "get_mocks"):
            m = model.WIDEIO_Meta.get_mocks()
            res.update(m[0])

        audlv.update({"view": json.dumps(res, sort_keys=True, indent=4, separators=(',', ': '))})
        audlv["view"] = '<pre data-lang="javascript">' + audlv["view"] + '</pre>'
        # audlv["view"] = highlight(audlv["view"].replace(',',',\n'), JsonLexer(), HtmlFormatter())
        audlv.update({"view_doc": documentation})

        audlv.update({"del": json.dumps({}, sort_keys=True, indent=4, separators=(',', ': '))})
        audlv["del"] = '<pre data-lang="javascript">' + audlv["del"] + '</pre>'
        # audlv["del"] = highlight(audlv["del"].replace(',',',\n'), JsonLexer(), HtmlFormatter())

        audlv.update({"isotherviews": False})
        if hasattr(model.WIDEIO_Meta, "other_views"):
            audlv["isotherviews"] = True
            audlv.update({"otherviews": []})
            for view in model.WIDEIO_Meta.other_views:
                try:
                    res = json.dumps({view["output"]}, sort_keys=True, indent=4, separators=(',', ': '))
                    audlv["otherviews"].append((view["name"], view["doc"], '<pre>' + res + '</pre>'))
                except:
                    audlv["otherviews"].append((view["name"], view["doc"], '<pre>' + view["output"] + '</pre>'))

        docs.append(("Results", audlv))
        return docs

    @classmethod
    def _process_model(cls, request, modulename, model):
        LM = []
        if not hasattr(model, "can_list") or not hasattr(model, "can_add"):
            return

        if not model.can_list(request) and not model.can_add(request):
            return

        docstr = model.__doc__

        if docstr is None:
            docstr = ""

        if model is not None:
            fields = {'_': [], 'all': [], 'python': [], 'file': []}
            try:
                for f in model._meta.fields:
                    if type(f) == ForeignKey:
                        fields['all'].append(f.name + "_id")
                    else:
                        fields['all'].append(f.name)
                    if type(f) == FileField:
                        fields['file'].append(f.name)
                        fields['python'].append("i_" + f.name)
                    elif f.name not in model.WIDEIO_Meta.form_exclude:
                        fields['_'].append(f.name)

                d0 = cls._process_model_build_curl(request, modulename, model, fields)
                d1 = cls._process_model_build_python(request, modulename, model, fields)
                d2 = cls._process_model_build_results(request, modulename, model, fields)
                docs = d0 + d1 + d2
                LM.append((model.__name__, model, publish_parts(docstr, writer_name='html')['body'], docs[:-1], docs[-1]))
            except Exception as e:
                raise
                logging.warning("Exception" + str(e))
            return LM

    @classmethod
    def view(cls, request, modulename):
        """
        This view return the list of WIDE IO applications' models and explain succinctly what they do and what their fields['_'] are.
        """
        LM = []
        try:
            models = __import__(modulename + ".models", fromlist=[str(modulename)])
            m2 = __import__(modulename, fromlist=modulename.split(".")[:-1])
            docstr2 = m2.__doc__
            if docstr2 == None:
                docstr2 = ""
            else:
                docstr2 = publish_parts(docstr2, writer_name='html')['body']

            for model in models.MODELS:
                LM += cls._process_model(request, modulename, model)

            context = {
                'module': modulename,
                'helpmod': docstr2,
                'models': LM,
                'request': request,
                'subreqparam': {'AJAX': 1, 'JSON': 1}
            }

            return render_to_response("help/models.html", context, RequestContext(request))
        except Exception, e:
            if not request.user.is_staff:
                messages.add_message(request, messages.WARNING, "Error in this module")
                return HttpResponseRedirect('../')
            else:
                raise


def help_models(*args,**kwargs):
    return HelpModel.view(*args,**kwargs)

help_models.default_url = 'modules/([^/]+)'

from wioframework.wiotext import _wiotext
from wioframework.decorators import user_passes_test


def dedent(t):
    t = t.split("\n")
    l = filter(lambda x: len(x.strip()), t)
    c = 0
    cr = l[0]
    while (len(cr) - c > 0) and (cr[c].isspace()):
        c += 1
    return "\n".join(map(lambda x: x[c:], t))


@user_passes_test(lambda u: u.is_staff)
def internal_help(request, module, lib, element):
    doc = getattr(__import__(module + "." + lib, fromlist=[module]), element).__doc__
    page_extrahead = ""
    t = "# WIDE IO - Internal documentation\n"
    t += "## %s > %s >  %s \n" % (module, lib, element)
    t += dedent(doc)
    context = {'page_extrahead': page_extrahead, 'thecontent': _wiotext(t, request)}
    return render_to_response("basic.html", context, RequestContext(request))


internal_help.default_url = 'internal_help/([^/]+)/([^/]+)/([^/]+)'

VIEWS = [help_modules, help_models, internal_help]
