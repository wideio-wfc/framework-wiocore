"""
ADAPTED BY WIDE IO

Copyright (c) 2007-2008, Dj Gilcrease
All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*********************************************************************************************************
EXPERIMENTAL
WARNING : modified to work with mongodb , not tested properly (for instance it has been not tested at all)
WARNING : pooling_frequency HAS TO BE modified if some jobs are to be executed more often than every 5 min
********************************************************************************************************

"""

import os
import cPickle
import sys
import time
from threading import Timer
from datetime import datetime

if "DJANGO_SETTINGS_MODULE" not in os.environ:
    os.environ["DJANGO_SETTINGS_MODULE"] = 'settings'

import settings
from django.apps.registry import apps

if __name__ == "__main__":
    apps.populate(settings.INSTALLED_APPS)

import backoffice.lib as debug
from django.dispatch import dispatcher
from django.conf import settings
from signals import cron_done
import models


class CronJob(object):
    # 86400 seconds == 24 hours
    run_every = 86400

    def run(self, *args, **kwargs):
        self.job()
        cron_done.send(sender=self, *args, **kwargs)

    def job(self):
        """
        Should be overridden (this way is cleaner, but the old way - overriding run() - will still work)
        """
        pass


class CronScheduler(object):
    timer = None

    def __init__(self):
        self.cron_models = None
        self.status = None

    def register(self, job_class, *args, **kwargs):
        """
        Register the given Job with the scheduler class
        """
        if self.status is None:
            cron_models = models.Cron.objects.all()
            if cron_models.count():
                self.status, created = cron_models[0], False
            else:
                self.status, created = models.Cron.objects.create(), False
            self.status.executing = False
            self.status.save()

        job_instance = job_class()

        if not isinstance(job_instance, CronJob):
            raise TypeError("You can only register a Job not a %r" % job_class)

        job, created = models.CronJob.objects.get_or_create(name=str(job_instance.__class__))
        if created:
            job.instance = cPickle.dumps(job_instance)
        job.args = cPickle.dumps(args)
        job.kwargs = cPickle.dumps(kwargs)
        job.run_frequency = job_instance.run_every
        print "NEW CRON Job frequency = {0}".format(job.run_frequency), job, job_instance.__class__
        job.queued = True
        job.save()
        #

    def execute(self, autorestart=True):
        """
        Queue all Jobs for execution
        """
        # how often to check if jobs are ready to be run (in seconds)
        # in reality if you have a multithreaded server, it may get checked
        # more often that this number suggests, so keep an eye on it...
        # default value: 300 seconds == 5 min
        try:
            polling_frequency = getattr(settings, "CRON_POLLING_FREQUENCY", 5)
            if self.status is None:
                cron_models = models.Cron.objects.all()
                self.status, created = None, None
                if cron_models.count():
                    self.status, created = cron_models[0], False
                else:
                    self.status, created = models.Cron.objects.create(), True

            # This is important for 2 reasons:
            #     1. It keeps us for running more than one instance of the
            #        same job at a time
            #     2. It reduces the number of polling threads because they
            #        get killed off if they happen to check while another
            #        one is already executing a job (only occurs with
            #		 multi-threaded servers)
            if self.status.executing:
                sys.stderr.write("CRON ALREADY EXECUTING\n")
                debug.log_error("CRON ALREADY EXECUTING\n")
                return

            self.status.executing = True
            try:
                self.status.save()
            except:
                # this will fail if you're debugging, so we want it
                # to fail silently and start the timer again so we
                # can pick up where we left off once debugging is done
                Timer(polling_frequency, self.execute).start()
                return

            jobs = models.CronJob.objects.all()
            i = 0
            print datetime.now(),
            for job in jobs:
                print "Looking at {0} {1}/{2}".format(job, i, jobs.count())
                print "Job queued = {0}".format(job.queued)
                i += 1
                if job.queued:
                    time_delta = datetime.now() - job.last_run
                    #                 print "Checking time delta {0} with job.last_run {1}".format(time_delta, job.last_run)
                    if (time_delta.seconds + 86400 * time_delta.days) > job.run_frequency:
                        #                      print "Ok"
                        inst = cPickle.loads(str(job.instance))
                        args = cPickle.loads(str(job.args))
                        kwargs = cPickle.loads(str(job.kwargs))

                        try:
                            inst.run(*args, **kwargs)
                            job.last_run = datetime.now()
                            job.save()

                        except Exception, e:
                            #                         print "Job execution error : {0}".format(e)
                            import traceback
                            for r in traceback.format_tb(sys.exc_info()[2]):
                                sys.stderr.write("%s\n" % (r,))
                            sys.stderr.write("CRON Job execution error : {0}".format(e))
                            debug.log_error("CRON Job execution error : {0}".format(e))
                            # if the job throws an error, just remove it from
                            # the queue. That way we can find/fix the error and
                            # requeue the job manually
                            job.queued = False
                            job.save()
                            raise
                            # import settings
                            # settings.EMAIL

            self.status.executing = False
            self.status.save()

            # Set up for this function to run again
            if autorestart:
                self.timer = Timer(polling_frequency, self.execute)
                self.timer.start()
        except Exception, e:
            print e
            raise


def autodiscover(autorun=True):
    """
    Auto-discover INSTALLED_APPS cron.py modules and fail silently when
    not present. This forces an import on them to register any cron jobs they
    may want.
    """
    import imp
    from django.conf import settings

    for app in settings.INSTALLED_APPS:
        # For each app, we need to look for an cron.py inside that app's
        # package. We can't use os.path here -- recall that modules may be
        # imported different ways (think zip files) -- so we need to get
        # the app's __path__ and look for cron.py on that path.

        # Step 1: find out the app's __path__ Import errors here will (and
        # should) bubble up, but a missing __path__ (which is legal, but weird)
        # fails silently -- apps that do weird things with __path__ might
        # need to roll their own cron registration.
        try:
            app_path = __import__(app, {}, {}, [app.split('.')[-1]]).__path__
        except AttributeError:
            continue

        # Step 2: use imp.find_module to find the app's admin.py. For some
        # reason imp.find_module raises ImportError if the app can't be found
        # but doesn't actually try to import the module. So skip this app if
        # its admin.py doesn't exist
        try:
            imp.find_module('cron', app_path)
        except ImportError:
            continue

        # Step 3: import the app's cron file. If this has errors we want them
        # to bubble up.
        __import__("%s.cron" % app)

    # Step 4: once we find all the cron jobs, start the cronScheduler
    if autorun:
        cronScheduler.execute()
        time.sleep(3600 * 24 * 365 * 4)


cronScheduler = CronScheduler()

if __name__ == "__main__":
    autodiscover(False)
    while True:
        print "*"
        cronScheduler.execute(False)
        time.sleep(5)
