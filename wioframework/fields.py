# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- coding: utf-8 -*-
import sys
from django import forms
from django.template.loader import render_to_string
from settings import SITE_URL
import datetime
#from django.forms.fields import Field

# Let us override field's default behaviour by permitting preliminary (that is, incomplete or missing) values
# class ExtendedField(Field):
# def __init__(self, *args, **kwargs):
#super(ExtendedField,self).__init__(*args, **kwargs)
#self.preliminary = False
import re
import copy
from django.db import models
from django.core.serializers.json import DjangoJSONEncoder
from django.utils.translation import ugettext_lazy as _
try:
    from django.utils import six
except ImportError:
    import six
    

from django.db.models import *
from wioframework import wjson as json



from django.core import exceptions, validators
from django.utils.translation import ugettext_lazy as _
import django.db.models as models

from django.db.models import *
from django.forms import fields
default_error_messages = {
    'invalid_choice': _('Value %r is not a valid choice.'),
    'null': _('This field cannot be null.'),
    'blank': _('This field cannot be blank.'),
    'unique': _('%(model_name)s with this %(field_label)s '
		'already exists.'),
    'app_null': _('This field cannot be null'),
    'db_null': _('This field cannot be null'),
    'app_blank	': _('This field cannot be blank'),
    'db_blank': _('This field cannot be blank'),
}
models.Field.default_error_messages = default_error_messages

#class Field(models.Field):
#     def __init__(self, *args, **kwargs):
#	self.app_p = dict(map(lambda i: (i[0][5:],i[1]),filter(lambda i: i[0].startswith('app_' ), kwargs.items())))
#	self.db_p = dict(map(lambda i: (i[0][5:],i[1]),filter(lambda i: i[0].startswith('db_' ), kwargs.items())))
#	kwargs = dict(filter(lambda i: not (i[0].startswith('app_') or i[0].startswith('db_')), kwargs.items()))
#	super(Field,self).__init__(*args, **kwargs)
#	#(Field,self).__init__(db_blank,db_null,app_null,app_blank, *args, **kwargs)



class CharField(models.CharField):
    def __init__(self, verbose_name=None, name=None,max_length=512,advanced_help_text=None, *args, **kwargs):
        self.app_p = {'null':True, 'blank':True, 'max_length':max_length}
        self.db_p = {'null':True, 'blank':True, 'max_length':max_length*3}
	self.app_p.update(dict(map(lambda i: (i[0][3:],i[1]),filter(lambda i: i[0].startswith('app_' ), kwargs.items()))))
	self.db_p.update(dict(map(lambda i: (i[0][3:],i[1]),filter(lambda i: i[0].startswith('db_' ) and i[0] not in ["db_index", "db_table", "db_column"], kwargs.items()))))

	kwargs = dict(filter(lambda i: not (i[0].startswith('app_') or i[0].startswith('db_')), kwargs.items()))
        kwargs.update(self.db_p)
        if ('primary_key' in kwargs.keys()):
            kwargs['null']=False
        self.app_validators=[]
        self.db_validators=[]
        self.app_validators.append(validators.MaxLengthValidator(self.app_p["max_length"]))
        self.db_validators.append(validators.MaxLengthValidator(self.db_p["max_length"]))
        self.advanced_help_text=advanced_help_text

	super(CharField,self).__init__(verbose_name, name, *args, **kwargs)

    def run_db_validators(self, value):
        if value in validators.EMPTY_VALUES:
            return

        errors = []
        for v in self.db_validators:
            try:
                v(value)
            except exceptions.ValidationError as e:
                if hasattr(e, 'code') and e.code in self.error_messages:
                    message = self.error_messages[e.code]
                    if e.params:
                        message = message % e.params
                    errors.append(message)
                else:
                    errors.extend(e.messages)
        if errors:
            raise exceptions.ValidationError(errors)

    def run_app_validators(self, value):
        if value in validators.EMPTY_VALUES:
            return

        errors = []
        for v in self.app_validators:
            try:
                v(value)
            except exceptions.ValidationError as e:
                if hasattr(e, 'code') and e.code in self.error_messages:
                    message = self.error_messages[e.code]
                    if e.params:
                        message = message % e.params
                    errors.append(message)
                else:
                    errors.extend(e.messages)
        if errors:
            raise exceptions.ValidationError(errors)



    def deconstruct(self):
        x,a,b,c=super(CharField,self).deconstruct()
        c['max_length']=self.app_p['max_length']
        return x,a,b,c

    def app_validate(self, value, model_instance):
      if value is None and not self.app_p['null']:
        raise exceptions.ValidationError(self.error_messages['null'])
      if not self.app_p['null'] and value in validators.EMPTY_VALUES:
        raise exceptions.ValidationError(self.error_messages['blank'])
      if self._choices and value not in validators.EMPTY_VALUES:
	    for option_key, option_value in self.choices:
		if isinstance(option_value, (list, tuple)):
		    # This is an optgroup, so look inside the group for
		    # options.
		    for optgroup_key, optgroup_value in option_value:
			if value == optgroup_key:
			    return
		elif value == option_key:
		    return
	    msg = self.error_messages['invalid_choice'] % value
	    raise exceptions.ValidationError(msg)
      self.run_app_validators(value)

    def db_validate(self,value,model_instance):
      if value is None and not self.db_p['null']:
	raise exceptions.ValidationError(self.error_messages['null'])
      if not self.db_p['null'] and value in validators.EMPTY_VALUES:
	raise exceptions.ValidationError(self.error_messages['blank'])
      self.run_db_validators(value)

    def validate(self, value, model_instance):
	"""
	Validates value and throws ValidationError. Subclasses should override
	this to provide validation logic.
	"""
	if not self.editable:
	    # Skip validation for non-editable fields.
	    return

	#validate = self.app_validate(self, value, model_instance)
	return self.db_validate(value, model_instance)	##print "DB STUFF", self.get_attname_column()


class TextField(models.TextField):
    def __init__(self, verbose_name=None, name=None,max_length=512,advanced_help_text=None, *args, **kwargs):
        self.app_p = {'null':True, 'blank':True, 'max_length':max_length}
        self.db_p = {'null':True, 'blank':True, 'max_length':max_length*3}
        self.app_p.update(dict(map(lambda i: (i[0][3:],i[1]),filter(lambda i: i[0].startswith('app_' ), kwargs.items()))))
        self.db_p.update(dict(map(lambda i: (i[0][3:],i[1]),filter(lambda i: i[0].startswith('db_' ) and i[0] not in ["db_index", "db_table", "db_column"], kwargs.items()))))
        self.app_validators=[]
        self.db_validators=[]

        kwargs = dict(filter(lambda i: not (i[0].startswith('app_') or i[0].startswith('db_')), kwargs.items()))
        kwargs.update(self.db_p)
        if ('primary_key' in kwargs.keys()):
            kwargs['null']=False
        if "advanced_help_text" in kwargs:
            del kwargs["advanced_help_text"]

        self.app_validators.append(validators.MaxLengthValidator(self.app_p["max_length"]))
        self.db_validators.append(validators.MaxLengthValidator(self.db_p["max_length"]))

        super(TextField,self).__init__(verbose_name, name, *args, **kwargs)

    def deconstruct(self):
        x,a,b,c=super(TextField,self).deconstruct()
        c['max_length']=self.app_p['max_length']
        return x,a,b,c

    def run_db_validators(self, value):
        if value in validators.EMPTY_VALUES:
            return

        errors = []
        for v in self.db_validators:
            try:
                v(value)
            except exceptions.ValidationError as e:
                if hasattr(e, 'code') and e.code in self.error_messages:
                    message = self.error_messages[e.code]
                    if e.params:
                        message = message % e.params
                    errors.append(message)
                else:
                    errors.extend(e.messages)
        if errors:
            raise exceptions.ValidationError(errors)

    def run_app_validators(self, value):
        if value in validators.EMPTY_VALUES:
            return

        errors = []
        for v in self.app_validators:
            try:
                v(value)
            except exceptions.ValidationError as e:
                if hasattr(e, 'code') and e.code in self.error_messages:
                    message = self.error_messages[e.code]
                    if e.params:
                        message = message % e.params
                    errors.append(message)
                else:
                    errors.extend(e.messages)
        if errors:
            raise exceptions.ValidationError(errors)


    def app_validate(self, value, model_instance):
      #if len(value) >= self.app_p['max_length']:
      #  raise exceptions.ValidationError(self.error_messages['max_length'])
      if value is None and not self.app_p['null']:
        raise exceptions.ValidationError(self.error_messages['null'])
      if not self.app_p['null'] and value in validators.EMPTY_VALUES:
        raise exceptions.ValidationError(self.error_messages['blank'])
      self.run_app_validators(value)
      if self._choices and value not in validators.EMPTY_VALUES:
            for option_key, option_value in self.choices:
                if isinstance(option_value, (list, tuple)):
                    # This is an optgroup, so look inside the group for
                    # options.
                    for optgroup_key, optgroup_value in option_value:
                        if value == optgroup_key:
                            return
                elif value == option_key:
                    return
            msg = self.error_messages['invalid_choice'] % value
            raise exceptions.ValidationError(msg)

    def db_validate(self,value,model_instance):
      #if len(value) >= self.db_p['max_length']:
      #  raise exceptions.ValidationError(self.error_messages['max_length'])
      if value is None and not self.db_p['null']:
        raise exceptions.ValidationError(self.error_messages['null'])
      if not self.db_p['null'] and value in validators.EMPTY_VALUES:
        raise exceptions.ValidationError(self.error_messages['blank'])
      self.run_db_validators(value)

    def validate(self, value, model_instance):
        """
        Validates value and throws ValidationError. Subclasses should override
        this to provide validation logic.
        """
        if not self.editable:
            # Skip validation for non-editable fields.
            return

        #validate = self.app_validate(self, value, model_instance)
        return self.db_validate(value, model_instance)  ##print "DB STUFF", self.get_attname_column()

try:
    from south.modelsinspector import add_introspection_rules
    add_introspection_rules([], ["^wioframework\.fields\.CharField"])
except ImportError:
    pass




try:
    from django.forms.utils import ValidationError
except ImportError:
    from django.forms.util import ValidationError


"""
Convenience routines for creating non-trivial Field subclasses, as well as
backwards compatibility utilities.

Add SubfieldBase as the __metaclass__ for your Field subclass, implement
to_python() and the other necessary methods and everything will work seamlessly.
"""


class SubfieldBase(type):

    """
    A metaclass for custom Field subclasses. This ensures the model's attribute
    has the descriptor protocol attached to it.
    """
    def __new__(cls, name, bases, attrs):
        new_class = super(SubfieldBase, cls).__new__(cls, name, bases, attrs)
        new_class.contribute_to_class = make_contrib(
            new_class, attrs.get('contribute_to_class')
        )
        return new_class


class Creator(object):

    """
    A placeholder class that provides a way to set the attribute on the model.
    """

    def __init__(self, field):
        self.field = field

    def __get__(self, obj, type=None):
        if obj is None:
            raise AttributeError('Can only be accessed via an instance.')
        return obj.__dict__[self.field.name]

    def __set__(self, obj, value):
        # Usually this would call to_python, but we've changed it to pre_init
        # so that we can tell which state we're in. By passing an obj,
        # we can definitively tell if a value has already been deserialized
        # More: https://github.com/bradjasper/django-jsonfield/issues/33
        obj.__dict__[self.field.name] = self.field.pre_init(value, obj)


def make_contrib(superclass, func=None):
    """
    Returns a suitable contribute_to_class() method for the Field subclass.

    If 'func' is passed in, it is the existing contribute_to_class() method on
    the subclass and it is called before anything else. It is assumed in this
    case that the existing contribute_to_class() calls all the necessary
    superclass methods.
    """

    def contribute_to_class(self, cls, name):
        if func:
            func(self, cls, name)
        else:
            super(superclass, self).contribute_to_class(cls, name)
        setattr(cls, self.name, Creator(self))

    return contribute_to_class




class JSONFormFieldBase(object):
    def to_python(self, value):
        if isinstance(value, six.string_types):
            try:
                if len(value) == 0:
                    return None
                return json.loads(value, **self.load_kwargs)
            except ValueError:
                #raise ValidationError(_("Enter valid JSON"))
                if value in [ None, "None", "null" ]:
                    return None
                return {'$error':'invalid json value'}
        return value

    def clean(self, value):
        if not value and not self.required:
            return None

        # Trap cleaning errors & bubble them up as JSON errors
        try:
            return super(JSONFormFieldBase, self).clean(value)
        except TypeError:
            raise ValidationError(_("Enter valid JSON"))


class JSONFormField(JSONFormFieldBase, fields.CharField):
    pass


class JSONCharFormField(JSONFormFieldBase, fields.CharField):
    pass


class JSONFieldBase(six.with_metaclass(SubfieldBase, models.Field)):

    def __init__(self, *args, **kwargs):
        self.dump_kwargs = kwargs.pop('dump_kwargs', {
            'cls': DjangoJSONEncoder,
            #'separators': (',', ':')
        })
        self.load_kwargs = kwargs.pop('load_kwargs', {})

        super(JSONFieldBase, self).__init__(*args, **kwargs)

    def pre_init(self, value, obj):
        """Convert a string value to JSON only if it needs to be deserialized.

        SubfieldBase metaclass has been modified to call this method instead of
        to_python so that we can check the obj state and determine if it needs to be
        deserialized"""

        if obj._state.adding:
            # Make sure the primary key actually exists on the object before
            # checking if it's empty. This is a special case for South datamigrations
            # see: https://github.com/bradjasper/django-jsonfield/issues/52
            if getattr(obj, "pk", None) is not None:
                if isinstance(value, six.string_types):
                    try:
                        if (len(value) == 0):
                            return None
                        return json.loads(value, **self.load_kwargs)
                    except ValueError,e:
                        #raise ValidationError(_("Enter valid JSON"))
                        if value in [ None, "None", "null" ]:
                           return None                        
                        return {'$error':"json error",'$serialized':value }

        return value

    def to_python(self, value):
        """The SubfieldBase metaclass calls pre_init instead of to_python, however to_python
        is still necessary for Django's deserializer"""
        return value

    def get_db_prep_value(self, value, connection, prepared=False):
        """Convert JSON object to a string"""
        if self.null and value is None:
            return None
        return json.dumps(value, **self.dump_kwargs)

    def value_to_string(self, obj):
        value = self._get_val_from_obj(obj)
        return self.get_db_prep_value(value, None)

    def value_from_object(self, obj):
        value = super(JSONFieldBase, self).value_from_object(obj)
        if self.null and value is None:
            return None
        return self.dumps_for_display(value)

    def dumps_for_display(self, value):
        return json.dumps(value, **self.dump_kwargs)

    def formfield(self, **kwargs):
        if "form_class" not in kwargs:
            kwargs["form_class"] = self.form_class

        field = super(JSONFieldBase, self).formfield(**kwargs)

        if isinstance(field, JSONFormFieldBase):
            field.load_kwargs = self.load_kwargs

        if not field.help_text:
            field.help_text = "Enter valid JSON"

        return field

    def get_default(self):
        """
        Returns the default value for this field.

        The default implementation on models.Field calls force_unicode
        on the default, which means you can't set arbitrary Python
        objects as the default. To fix this, we just return the value
        without calling force_unicode on it. Note that if you set a
        callable as a default, the field will still call it. It will
        *not* try to pickle and encode it.

        """
        if self.has_default():
            if callable(self.default):
                return self.default()
            return copy.deepcopy(self.default)
        # If the field doesn't have a default, then we punt to models.Field.
        return super(JSONFieldBase, self).get_default()

    def db_type(self, connection):
        if connection.vendor == 'postgresql' and connection.pg_version >= 90300:
            return 'json'
        else:
            return super(JSONFieldBase, self).db_type(connection)



class JSONField(JSONFieldBase, models.TextField):
    """JSONField is a generic textfield that serializes/deserializes JSON objects"""
    form_class = JSONFormField

    def dumps_for_display(self, value):
        kwargs = {"indent": 2}
        kwargs.update(self.dump_kwargs)
        return json.dumps(value, **kwargs)


class JSONCharField(JSONFieldBase, models.CharField):
    """JSONCharField is a generic textfield that serializes/deserializes JSON objects,
    stored in the database like a CharField, which enables it to be used
    e.g. in unique keys"""
    form_class = JSONCharFormField



    
class BField(models.Field):

    def __init__(self):
        pass

    def to_python(self):
        pass

    def get_choices(self, include_blank=True, blank_choice="-"):
        pass

    def value_to_string(self, obj):
        pass

    def save_form_data(self, instance, data):
        setattr(instance, self.name, data)

    def formfield(self, form_class=None, choices_form_class=None, **kwargs):
        defaults = {'required': not self.blank,
                    'label': capfirst(self.verbose_name),
                    'help_text': self.help_text}
        if self.has_default():
            if callable(self.default):
                defaults['initial'] = self.default
                defaults['show_hidden_initial'] = True
            else:
                defaults['initial'] = self.get_default()
        if self.choices:
            # Fields with choices get special treatment.
            include_blank = (self.blank or
                             not (self.has_default() or 'initial' in kwargs))
            defaults['choices'] = self.get_choices(include_blank=include_blank)
            defaults['coerce'] = self.to_python
            if self.null:
                defaults['empty_value'] = None
            if choices_form_class is not None:
                form_class = choices_form_class
            else:
                form_class = forms.TypedChoiceField
            # Many of the subclass-specific formfield arguments (min_value,
            # max_value) don't apply for choice fields, so be sure to only pass
            # the values that TypedChoiceField will understand.
            for k in list(kwargs):
                if k not in ('coerce', 'empty_value', 'choices', 'required',
                             'widget', 'label', 'initial', 'help_text',
                             'error_messages', 'show_hidden_initial'):
                    del kwargs[k]
        defaults.update(kwargs)

    #@cached_property
    # def validators(self):
        # Some validators can't be created at field initialization time.
        # This method provides a way to delay their creation until required.
        # return self.default_validators + self._validators

    # def run_validators(self):
        # pass
        # if form_class is None:
        #form_class = forms.CharField
        # return form_class(**defaults)

        # def value_from_object(self, obj):
        #"""
        # Returns the value of this field in the given model instance.
        #"""
        # return getattr(obj, self.attname)

        
class RemoteForeignKey(models.ForeignKey):
    def __init__(self, *args, **kwargs):
        super(RemoteForeignKey, self).__init__(*args, **kwargs)

    def pre_init(self, value, obj):
        """Convert a string value to JSON only if it needs to be deserialized.

        SubfieldBase metaclass has been modified to call this method instead of
        to_python so that we can check the obj state and determine if it needs to be
        deserialized"""

        if obj._state.adding:
            # Make sure the primary key actually exists on the object before
            # checking if it's empty. This is a special case for South datamigrations
            # see: https://github.com/bradjasper/django-jsonfield/issues/52
            if getattr(obj, "pk", None) is not None:
                if isinstance(value, six.string_types):
                    try:
                        if len(value) == 0:
                            return None
                        return json.loads(value, **self.load_kwargs)
                    except ValueError,e:
                        #raise ValidationError(_("Enter valid JSON"))
                        if value in [None, "None", "null"]:
                           return None                        
                        return {'$error':"json error", '$serialized':value }

        return value

    def to_python(self, value):
        if "@" in value:
            ## FIXME: BAD IDEA WITHOUT GREENLET: DJANGO IS NOT DESIGNED FOR DOING DOWNLOAD ON THE SERVER..? (MOVE TO...)
            k,h="@".split()
            h=web.get("https://%s"+self.get_base_url()+"/view/"+base64.b16encode(base64.b64decode(k))+"/")
        else:
            return super(RemoteForeignKey, self).to_python(value)

    def formfield(self, **kwargs):
        if "form_class" not in kwargs:
            kwargs["form_class"] = self.form_class
        field = super(RemoteForeignKey, self).formfield(**kwargs)

        return field

    def get_default(self):
        return super(RemoteForeignKey, self).get_default()

    def db_type(self, connection):
        return super(RemoteForeignKey, self).db_type(connection) # < A PRIORI STRING
