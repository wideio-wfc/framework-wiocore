# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- coding: utf-8 -*-
from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext, Context, loader
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.http import HttpResponse
import base64
import settings
import urllib
import os
import time
from django.views.decorators.cache import *
try:
    import matplotlib
    matplotlib.use('Agg')
    from matplotlib import pyplot
    from matplotlib.backends.backend_agg import FigureCanvasAgg
except:
    pass


def pyplot_view(title="no title provided", OW=1024, OH=768):
    def fdecorator(fview):
        try:
            import numpy
        except:
            pass
        import PIL
        from PIL import Image
        try:
            import matplotlib
            matplotlib.use('Agg')
            from matplotlib import pyplot
            from matplotlib.backends.backend_agg import FigureCanvasAgg
        except:
            pass

        def _pyplot_view(request, *args, **kwargs):
            if "GET_PYPLOT_IMAGE" in request.REQUEST:
                W = OW
                H = OH
                import cStringIO
                if ("PYPLOT_W" in request.REQUEST):
                    TW = int(request.REQUEST["PYPLOT_W"])
                    W = min(W, TW)
                if ("PYPLOT_H" in request.REQUEST):
                    TH = int(request.REQUEST["PYPLOT_H"])
                    H = min(H, TH)
                mw, mh = W, H
                out = cStringIO.StringIO()
                img_dpi = 72.
                width = float(mw)
                height = float(mh)
                f = pyplot.figure(
                    dpi=img_dpi,
                    figsize=(
                        width /
                        img_dpi,
                        height /
                        img_dpi))
                a = f.gca()
                fview(request, f, a, *args, **kwargs)
                f.suptitle(title)
                canvas = FigureCanvasAgg(f)
                canvas.draw()
                size = (int(canvas.figure.get_figwidth() * img_dpi),
                        int(canvas.figure.get_figheight() * img_dpi))
                buf = canvas.tostring_rgb()
                pyplot.close(f)
                image = PIL.Image.fromstring(
                    'RGB',
                    size,
                    buf,
                    'raw',
                    'RGB',
                    0,
                    1)
                imagestr = cStringIO.StringIO()
                image.save(imagestr, 'png')
                imagestr.seek(0)
                return HttpResponse(imagestr.read(), 'image/png')
            else:
                templatename = "bases/pyplot_view.html"
                #from wioframework.uuid import getuuid
                # uuid=getuuid()
                import uuid
                return render_to_response(templatename,
                                          {'request': request,
                                           'divid': 'div' + str(uuid.uuid1()),
                                           },
                                          context_instance=RequestContext(request))
        return never_cache(_pyplot_view)
    return fdecorator
