# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# encoding: utf-8
#
# Copyright (c) 2009 Thomas Kongevold Adamcik
#
# Snippet is released under the MIT License. So feel free to use it in other
# projects as long as the notice remains intact :)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# See http://www.djangosnippets.org/snippets/1312/

'''
HTML Validation Middleware
==========================

Simple development middleware to ensure that responses validate as HTML.

Dependencies:
-------------

 - tidy (http://utidylib.berlios.de/)

Installation:
-------------

Assuming this file has been place in your PYTHON_PATH (e.g.
djangovalidation/middleware.py), simply add the following
to your middleware settings:

  'djangovalidation.middleware.HTMLValidationMiddleware',

Remember that the order of your middleware settings does matter, this
middleware should be placed before eg. GzipMiddleware, djangologging and
any other middlewares that modify the response's content.

Operation:
----------

Validation only kicks in under to following conditions:
 - DEBUG == True
 - HTML_VALIDATION_ENABLE == True (default)
 - REMOTE_ADDR in INTERNAL_IPS
 - 'html' in Content-Type
 - 'disable-validation' not in GET
 - request.is_ajax() == False
 - type(response) == HttpResponse
 - request.path doesn't match HTML_VALIDATION_URL_IGNORE

To bypass the check any uri can be appended with ?disable-validation

Settings:
---------

 - HTML_VALIDATION_ENABLE     - Turns middleware on/off. Default: True

 - HTML_VALIDATION_ENCODING   - Default: 'utf-8'

 - HTML_VALIDATION_DOCTYPE    - Default: 'strict'

 - HTML_VALIDATION_IGNORE     - Default: ['trimming empty <option>',
                                          '<table> lacks "summary" attribute']

 - HTML_VALIDATION_URL_IGNORE - List of regular expressions to check
                                request.path against when deciding if we should
                                process the request. Default: [],

 - HTML_VALIDATION_XHTML      - Default: True

 - HTML_VALIDATION_OPTIONS    - Options that get passed to tidy, overrides
                                previous settings. Default: based on above
                                settings

For more information about settings use the source and consult tidy s
documentation.

History
-------

December 19, 2009:
 - Fix empty HTML_VALIDATION_URL_IGNORE. Thanks .iqqmuT

July 12, 2009:
 - Ignore ajax request
 - Add HTML_VALIDATION_URL_IGNORE settings

February 6, 2009:
 - Initial relase
'''


import re

import re
import os
import time


from django.conf import settings
from django.core.exceptions import MiddlewareNotUsed
from django.http import HttpResponse, HttpResponseServerError
from django.template import Context, Template


class Indenter:

    def __init__(self):
        self.indent = ""
        self.il = 2

    def pushln(self, x):
        self.indent += " " * self.il
        y = x.strip() + "\n" + self.indent
        return y

    def popln(self, x):
        self.indent = self.indent[:-(self.il)]
        return "\n" + self.indent + x.strip() + "\n" + self.indent

    def writeln(self, x):
        return x.strip() + "\n" + self.indent


class HTMLValidationMiddleware(object):

    '''
        Checks that the response is valid HTML with proper Unicode. In the
        event of a failed check we show an simple page listing the HTML source
        and which errors need to be fixed.
    '''

    # Validation errors to ignore. Can be overridden with VALIDATION_IGNORE
    # setting
    ignore = [
        'trimming empty <option>',
        '<table> lacks "summary" attribute',
    ]

    # Options for tidy. Can be overridden with HTML_VALIDATION_OPTIONS setting
    options = {
        'indent': True,
        'input_xml': getattr(settings, 'HTML_VALIDATION_XHTML', True),
        'output_xhtml': getattr(settings, 'HTML_VALIDATION_XHTML', True),
        'input_encoding': getattr(settings, 'HTML_VALIDATION_ENCODING', 'utf8'),
    }

    def __init__(self):
        if not settings.DEBUG or not getattr(
                settings, 'HTML_VALIDATION_ENABLE', True):
            raise MiddlewareNotUsed

        self.options = getattr(
            settings,
            'HTML_VALIDATION_OPTIONS',
            self.options)
        self.ignore = set(
            getattr(
                settings,
                'HTML_VALIDATION_IGNORE',
                self.ignore))
        self.ignore_regexp = self._build_ignore_regexp(
            getattr(
                settings,
                'HTML_VALIDATION_URL_IGNORE',
                []))
        self.template = Template(self.HTML_VALIDATION_TEMPLATE.strip())

    def process_exception(self, request, exception):
        import sys
        import traceback
        sys.stderr.write("<<< EXCEPTION MIDDLEWARE [\n")
        for i in request.META.items():
            sys.stderr.write("- %s %r\n" % (i))
        sys.stderr.write(
            "REQUEST_V: %r\nNFILES: %r\nFILES: %r\n" %
            (list(
                request.REQUEST.keys()), len(
                request.FILES.keys()), request.FILES))
        sys.stderr.write(str(exception) + "\n")
        sys.stderr.write(
            "\n".join(
                traceback.format_tb(
                    sys.exc_info()[2])) +
            "\n")
        sys.stderr.write(">>> EXCEPTION MIDDLEWARE ]\n")
        raise

    def process_response(self, request, response):
        if not self._should_validate(request, response):
            return response
        ntxt, errors = self._validate(response)

        if not errors:
            if (hasattr(request,"user") and (request.user is not None) and (request.user.is_superuser or request.user.is_staff) and (
                    "_AS_PROD" not in request.REQUEST)):
                from tidylib import tidy_document, tidy_fragment
                xr = response.content
                # xr=ntxt
                # print xr
                # try:
                if True:
                    import re
                    import subprocess

                    def jsopt(s, args="-z2 -A1"):
                        # print "jsopt",s
                        xuuid = "%05d-%f" % (os.getpid(), time.time())
                        p = subprocess.Popen(
                            "cat >/tmp/y" +
                            xuuid +
                            ".js;astyle " +
                            args +
                            " /tmp/y" +
                            xuuid +
                            ".js > /dev/null; cat /tmp/y" +
                            xuuid +
                            ".js; rm -f /tmp/y" +
                            xuuid +
                            ".js",
                            shell=True,
                            stdin=subprocess.PIPE,
                            stdout=subprocess.PIPE)
                        return p.communicate(str(s))[0]

                    def jsind(txt):
                        from mml import transductor
                        i = Indenter()
                        t = transductor.Transductor({
                            'a': [
                                (u'([ \t\n]*)([^\'"{};]*);',
                                 lambda x:i.writeln(x.group(0))),
                                (u'([ \t\n]*){',
                                 lambda x:i.pushln(x.group(0))),
                                (u"'", None, 'e'), (u"\"", None, 'f'),
                                (u'([ \t\n]*)}', lambda x:i.popln(x.group(0))),
                            ],
                            'e': [(u"'", None, 'a')],
                            'f': [(u'"', None, 'a')],
                        })
                        # txt=txt.decode('utf8')#unicode(txt)
                        return t.process(txt, 128)

                    # print "SUB"
                    xr = xr.decode('utf8')
                    xr = re.sub(
                        '<script([^>]*)>(.*?)</script>',
                        lambda x: "<script%s>%s</script>" %
                        (x.group(1),
                         jsind(
                            x.group(2))),
                        xr)
                # except Exception,e:
                    # print "Exception",e

                # .replace("<![CDATA[","/* <![CDATA[ */").replace("]]>","/* ]]> */")
                xr = tidy_document(xr, self.options)[0]
                nr = HttpResponse(xr)
                #nr["X-Frame-Options"] = FRAMEPOLICY
                return nr
            else:
                return response

        context = self._get_context(response, errors, ntxt)

        return HttpResponseServerError(self.template.render(context))

    def _build_ignore_regexp(self, urls):
        if not urls:
            return None

        urls = [r'(%s)' % url for url in urls]
        return re.compile(r'(%s)' % r'|'.join(urls))

    def _should_validate(self, request, response):
        # print response['Content-Type'], type(response),settings.INTERNAL_IPS
        return ('html' in response['Content-Type']  and
                '_DISABLE_VALIDATION' not in request.GET and
                not request.is_ajax() and
                (not self.ignore_regexp or
                 not self.ignore_regexp.search(request.path)) and
                (response.status_code not in [302, 303, 304, 404,500]) and
                # request.META['REMOTE_ADDR'] in settings.INTERNAL_IPS and
                isinstance(response, HttpResponse))

    def _validate(self, response):
        from tidylib import tidy_document, tidy_fragment
        import re
        txt = response.content
        from mml import transductor
        i = Indenter()
        t = transductor.Transductor({
            'a': [
                (u"([ \t\n]*)<script([^>]*)>",
                 lambda x:i.pushln(x.group(0)),
                 'c'),
                (u'([ \t\n]*)<!([^>]*)>', lambda x:i.writeln(x.group(0))),
                (u'([ \t\n]*)<([^>]*)/>', lambda x:i.writeln(x.group(0))),
                (u'([ \t\n]*)<([^/>])>', lambda x:i.pushln(x.group(0))),
                (u'([ \t\n]*)<([^/>])([^>]*)([^/>])>',
                 lambda x:i.pushln(x.group(0))),
                (u'([ \t\n]*)</([^>]*)([^/>])>', lambda x:i.popln(x.group(0))),
            ],

            'c': [(u"</script([^>]*)>", lambda x:i.popln(x.group(0)), 'a'), (u"'", None, 'e'), (u"\"", None, 'f'), (u"([ \t\n]+)", u" ")],
            'e': [(u"'", None, 'c')],
            'f': [(u'"', None, 'c')],
            'b': [(u"%}", None, 'a'), (u'\n', u" ")]

        })
        txt = txt.decode('utf8')  # unicode(txt)
        txt = t.process(txt, 128)
        txt = u"\n".join(filter(lambda l: len(l.strip()), txt.split(u"\n")))
        res = tidy_document(txt, self.options)
        errors = res[1].split("\n")
        errors = self._filter_errors2(errors)
        return txt, errors

    def _filter_errors(self, errors):
        return filter(lambda e: e.message not in self.ignore, errors)

    def _filter_errors2(self, errors):
        return filter(lambda e: "- Error" in e, errors)

    def _get_context(self, response, errors, ntxt):
        lines = []
        # error_dict = dict(map(lambda e: (e.line, e.message)), errors))
        error_dict = dict(
            map(lambda e: (e.split("-")[0], "-".join(e.split("-")[1:])), errors))

        for i, line in enumerate(ntxt.split('\n')):
            lines.append((line, error_dict.get(i + 1, False)))

        return Context({'errors': errors,
                        'lines': lines, })

    HTML_VALIDATION_TEMPLATE = """
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <title>HTML validation error at {{ request.path_info|escape }}</title>
  <meta name="robots" content="NONE,NOARCHIVE" />
  <style type="text/css">
    html * { padding: 0; margin: 0; }
    body * { padding: 10px 20px; }
    body * * { padding: 0; }
    body { font: small sans-serif; background: #eee; }
    body>div { border-bottom: 1px solid #ddd; }
    h1 { font-weight: normal; margin-bottom: 0.4em; }
    table { border: none; border-collapse: collapse; width: 100%; }
    td, th { vertical-align: top; padding: 2px 3px; }
    th { width: 6em; text-align: right; color: #666; padding-right: 0.5em; }
    #info { background: #f6f6f6; }
    #info th { width: 3em; }
    #summary { background: #ffc; }
    #explanation { background: #eee; border-bottom: 0px none; }
    .meta { margin: 1em 0; }
    .error { background: #FEE }
  </style>
</head>
<body>
  <div id="summary">
    <h1>HTML validation error</h1>
    <p>
        Your HTML did not validate. If this page contains user content that
        might be the problem. Please fix the following:
    </p>
    <table class="meta">
      {% for error in errors %}
        <tr>
          <th>Line: <a href="#line{{ error }}">{{ error }}</a> </th>
          <td>{{ error.message|escape }}</td>
        </tr>
      {% endfor %}
    </table>
    <p>
      If you want to bypass this warning, click <a href="?_DISABLE_VALIDATION=1">
      here</a>. Please note that this warning will persist until you fix the
      problems mentioned above.
    </p>
  </div>
  <div id="info">
    <table>
      {% for line,error in lines %}
        <tr{% if error %} class="error"{% endif %}>
          <th id="line{{ forloop.counter }}">
            {{ forloop.counter|stringformat:"03d" }}
          </th>
          <td{% if error %} title="{{ error }}"{% endif %}>
            <pre>{{ line }}</pre>
          </td>
        </tr>
      {% endfor %}
    </table>
  </div>

  <div id="explanation">
    <p>
      You're seeing this error because you have not set
      <code>HTML_VALIDATION_ENABLE = False</code> in your Django settings file.
      Change that to <code>False</code>, and Django will stop validating your
      HTML.
    </p>
  </div>
</body>
</html>"""
