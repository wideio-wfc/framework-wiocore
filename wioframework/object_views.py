# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
from functools import reduce

#
# The next function should be used in URL files to implement automatically
#


def autoviews(modulename):
    from django.conf.urls import url
    m = __import__(modulename, fromlist=modulename.split('.')[:-1])
    nl = filter(
        lambda x: (
            isinstance(
                x[0], type(SYView)) and x[0].__module__ in [
                modulename, 'aiosciweb1.core.list_detail'] and issubclass(
                    x[0], SYView) and (
                        x[0].url is not None)), map(
                            lambda x: (
                                eval(
                                    'm.' + x, {
                                        'm': m}), x), dir(m)))
    r = []
    for x in nl:
        #    print x[0].get_urlname()
        r.append(
            url(x[0].get_url(),
                reduce(lambda x, f: f(x), x[0].view_decorators, x[0].view),
                {},
                **(dict([] + (x[0].urlname and [('name', x[0].get_urlname())] or []))))
        )
        au = x[0].get_additional_urls()
        if (au):
            r.extend(map(lambda y: url(y[0],
                                       reduce(lambda x,
                                              f: f(x),
                                              x[0].view_decorators,
                                              x[0].view),
                                       *y[1],
                                       **y[2]),
                         au))
    return r
