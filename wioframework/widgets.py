#!/usr/bin/env python
# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- coding: utf-8 -*-
"""
WIDE IO
Revision FILE: $Id: widgets.py 841 2009-08-23 13:01:52Z tranx $


#############################################################################################################
  copyright $Copyright$
  @version $Revision: 841 $
  @lastrevision $Date: 2009-08-23 22:01:52 +0900 (Sun, 23 Aug 2009) $
  @modifiedby $LastChangedBy: tranx $
  @lastmodified $LastChangedDate: 2009-08-23 22:01:52 +0900 (Sun, 23 Aug 2009) $
#############################################################################################################
"""
# create widgets related to jquery component for essential displays ...
import datetime
import re
import uuid

from django.db.models.fields import NOT_PROVIDED
from django.forms.widgets import Widget
from django.template import Context, loader
from django.utils.safestring import mark_safe
#from wioframework.uuid import getuuid
#from references.models import BibliographicReference, Image, LinkReference, Keyword

import  settings


def getuuid():
    return str(uuid.uuid1())

import extfields
import os
ep0 = extfields.__path__[0]
for d in os.listdir(ep0):
    if (os.path.isdir(os.path.join(ep0, d))):
        if os.path.exists(os.path.join(ep0, d, "widgets.py")):
            m = __import__(
                "extfields.%s.widgets" %
                (d), fromlist=[
                    "extfields", d])
            __module = locals()
            for f in dir(m):
                if f not in __module:
                    __module[f] = getattr(m, f)

