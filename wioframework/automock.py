import mock


def g(x):
    return "g"

def h(x):
    return "h"

def f(x):
    return g(x)


def cf(*args,**kwargs):
    @mock.patch("__main__.g") # force reimport of the module?
    def cfg(g):
        g.side_effect=getattr(g,"mock",h)
        r=f(*args,**kwargs)
        return r
    return cfg()

if __name__=="__main__":
    print __name__
    print f(3)
    res=cf(3)
    print res
