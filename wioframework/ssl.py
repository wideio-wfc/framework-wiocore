#!/usr/bin/env python
# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- coding: utf-8 -*-

from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect, HttpResponsePermanentRedirect
SSL = 'SSL'


class SSLRedirect:

    def process_view(self, request, view_func, view_args, view_kwargs):
        import time
        request.clock = time.clock()
        request.time = time.time()
        # this is not for SSL but I am was lazy to define a new overload ...
        if hasattr(settings, "DEV_MODE") and settings.DEV_MODE == True:
            request.HTTPS = "http"
        else:
            request.HTTPS = "https"

        #request.HTTPS=bool(request.META.has_key('wsgi.version')) and "http" or "https"
        #log_debug(request,"|view_func:"+str(view_func)+"|view_args:"+str(view_args)+"|view_kwargs:"+str(view_kwargs)+'|req:'+str(request.REQUEST) )
        if 'REMOTE_ADDR' in request.META:
            request.session['REMOTE_ADDR'] = request.META['REMOTE_ADDR']
        else:
            try:
                request.session['REMOTE_ADDR'] = request.META['REMOTE_HOST']
            except:
                request.session['REMOTE_ADDR'] = '0.0.0.0'
                raise Exception("Unknown Server")
        # this is really SSL
        osec = self._is_secure(request)
        
        if settings.REQUIRE_HTTPS:
            secure=True
        else:
          if (hasattr(view_func, SSL)):
            if (getattr(view_func, SSL) == 1):
                secure = True
            elif (getattr(view_func, SSL) == 0):
                secure = osec
            else:
                secure = False
          else:
            if SSL in view_kwargs:
                secure = view_kwargs[SSL]
                del view_kwargs[SSL]
                # view_args[-1]=view_kwargs
            else:
                secure = False
        if 'HTTP_AUTHORIZATION' in request.META:
            secure = True

        
            
            
        if not secure == osec:
                # if (request.META.has_key('wsgi.version')):
            if settings.DEBUG:
                import sys
                sys.stderr.write(str(view_func) + " ignoring SSL flag (WSGI)")
                return None
            return self._redirect(request, secure)

    def _is_secure(self, request):
        if request.is_secure():
            return True

        # Handle the Webfaction case until this gets resolved in the
        # request.is_secure()
        if 'HTTP_X_FORWARDED_SSL' in request.META:
            return request.META['HTTP_X_FORWARDED_SSL'] == 'on'

        return False

    def _redirect(self, request, secure):
        protocol = secure and "https" or "http"
        if settings.DEBUG and request.method == 'POST':
            raise RuntimeError("""Django can't perform a SSL redirect while maintaining POST data.
           Please structure your views so that redirects only occur during GETs.""")
        http_host = (
            request.META["HTTP_HOST"] if (
                "HTTP_HOST" in request.META) else request.META["SERVER_NAME"])
        newurl = "%s://%s%s" % (protocol, http_host, request.get_full_path())
        return HttpResponsePermanentRedirect(newurl)