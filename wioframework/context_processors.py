#!/usr/bin/env python
# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- coding: utf-8 -*-
import settings
import os
from django.contrib.auth.models import AnonymousUser

def usercomm(request):
    import time
    """
    Adds timing info
    """
    ctx = {}
    
    default_base="bases/" +(settings.DEFAULT_BASE if hasattr(settings,
                                                                "DEFAULT_BASE") else "std")

    ##
    ## DEFINING WHICH WEBSITE TO DISPLAY
    ##
    xsn=request.META.get("HTTP_HOST","").split(".")[0]
    xdn=".".join(request.META.get("HTTP_HOST","").split(".")[1:])
    if not len(xsn):
      xsn=request.META.get("SERVER_NAME","").split(".")[0]
    if not len(xdn):
      xdn=".".join(request.META.get("SERVER_NAME","").split(".")[1:])
    xsn=request.GET.get("_TENANT",(xsn.split(":")[0]).split(".")[0])
    if len(xdn) and xdn not in [ "wide.io", "livealgo.com" ]:
        if xsn in [ "www" ]:
            xsn=xdn.split(".")[0]
            

    #try:
    #      ctx['site'] = SiteController.objects.get(is_tenant=False)
    #except:
    #      print "site not set -- this needs to be fixed"            
            
    if xsn not in [ "localhost", "www","penn" ]:
      if not isinstance(xsn,basestring):
          xsn="-non-existent-"
      if os.path.exists("templates/tenants/"+xsn+"/bases/std.htmml"):
        default_base="tenants/"+xsn+"/bases/std"
        #tenant = SiteController.get_by_flat_name(xsn)
        assert(False) # Feature disabled this should be replaced by a direct query in settings (but maybe need to be thought federated too/...)
        setattr(request, 'tenant', tenant)
        ctx['site'] = tenant


      
    if request.GET.get("_AS_ANONYMOUS"):
      request.user=AnonymousUser()
    if request.GET.get("_AS_BUSINESS"):
      request.user.is_customer=True
      request.user.is_scientist=False
    if request.GET.get("_AS_SCIENTIST"):
      request.user.is_customer=False
      request.user.is_scientist=True

    ctx.update({
        'request': request,
        'settings': settings,
        'base':  default_base + ".html"
    })
    
    if '_BASE' in request.REQUEST and not "/" in request.REQUEST["_BASE"]:
        ctx['base'] = "bases/" + request.REQUEST["_BASE"] + ".html"
    if '_AJAX' in request.REQUEST:
        ctx['base'] = "bases/ajax.html"
    ctx['STAFF_DEBUG'] = '_STAFF_DEBUG' in request.REQUEST
    if hasattr(request, "clock"):
        ctx.update({'clock': lambda: str((time.clock() - request.clock)),
                    'time': lambda: str((time.time() - request.time))})
    return ctx
