# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
import sys
import tempfile

PROFILER = "CPROFILE"

if PROFILER == "CPROFILE":
    import cProfile
    import pstats

if PROFILER == "HOTSHOT":
    import hotshot
    import hotshot.stats

from django.conf import settings
from cStringIO import StringIO


class ProfileMiddleware(object):
    """
    Displays hotshot profiling for any view.
    http://yoursite.com/yourview/?prof

    Add the "prof" key to query string by appending ?prof (or &prof=)
    and you'll see the profiling results in your browser.
    It's set up to only be available in django's debug mode,
    but you really shouldn't add this middleware to any production configuration.
    * Only tested on Linux
    """

    def process_request(self, request):
        if settings.DEBUG and '_PROF' in request.GET:
            self.proftmpfile = tempfile.NamedTemporaryFile()
            self.prof = None
            if PROFILER == "HOTSHOT":
                self.prof = hotshot.Profile(self.proftmpfile.name)

    def process_view(self, request, callback, callback_args, callback_kwargs):
        if settings.DEBUG and '_PROF' in request.GET:
            if PROFILER == "HOTSHOT":
                return self.prof.runcall(
                    callback, request, *callback_args, **callback_kwargs)
            if PROFILER == "CPROFILE":
                cProfile.runctx("callback(request,*callback_args,**callback_kwargs)", globals(), locals(),
                                self.proftmpfile.name)

    def process_response(self, request, response):
        if settings.DEBUG and '_PROF' in request.GET:
            if self.prof:
                self.prof.close()

            out = StringIO()
            old_stdout = sys.stdout
            sys.stdout = out

            if PROFILER == "HOTSHOT":
                print "HOTSHOT"
                stats = hotshot.stats.load(self.proftmpfile.name)
                # stats.strip_dirs()
                stats.sort_stats('time', 'calls')
                stats.print_stats(20)

            if PROFILER == "CPROFILE":
                print "CPROFILE"
                s = pstats.Stats(self.proftmpfile.name)
                ss = s.strip_dirs().sort_stats("time")
                ss.print_stats(20)
                ss.print_callers(20)

            sys.stdout = old_stdout
            stats_str = out.getvalue()

            if response and response.content and stats_str:
                response.content += "<pre>" + stats_str + "</pre>"

        return response
