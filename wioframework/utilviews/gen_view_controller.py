# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
from wioframework.utilviews.base import *

def gview_view(DocClass, elem_view=lambda x: x, json_view=lambda x: x,
               template=None, default_perpage=20, decorators=[], name=None, permissions=None):

    def lv(request, perpage=default_perpage):
        print "gview_gview called"
        #oid=filter(lambda x:len(x)!=0,request.path.split("/"))[-1]
        if "oid" in request.REQUEST:
            oid = request.REQUEST["oid"]
        else:
            oid = filter(lambda x: len(x) != 0, request.path.split("/"))[-1]

        obj = solveoid(DocClass, oid)
        
        if (obj.wiostate not in [ 'U0', 'U1', 'V' ]) and (not request.user.is_staff):
          raise Http404

        if hasattr(obj, "can_view") and not obj.can_view(request):
            raise PermissionException()
        if permissions:
            if not permissions["view"](obj, request):
                raise PermissionException()
        addcontext = {}
        if hasattr(obj, "on_view"):
            addcontext = obj.on_view(request)
        data = json_view(obj)
        if hasattr(data, "__dict__"):
            data = dict(
                filter(
                    lambda i: i[0][0] != "_",
                    data.__dict__.items()))
        if addcontext is None:
            addcontext = {}

        if isinstance(data, dict):
            data.update(addcontext)

        if request.data_mode:
            return {"res":data}
        #    return JsonResponse({"res": data})
        #elif "_BSON" in request.REQUEST and request.REQUEST["_BSON"] == "1":
        #    return BsonResponse({"res": data})
        else:
            context = {
                'baseviewid': request.REQUEST.get('baseviewid', "genlist"),
                'object': elem_view(obj),
                'x': elem_view(obj),
                'fields': filter(lambda x:x.name[0]!="_" , obj._meta.fields ),
                'virtual_fields': filter(lambda x:x.name[0]!="_" , obj._meta.virtual_fields ),
                'request': request,
                'settings': settings,
                'permissions': permissions
            }
            context.update(addcontext)
            template_path = "models/" + DocClass.__name__.lower() + "/" + (
                safefilename(
                    request.REQUEST["_VIEW"]) if "_VIEW" in request.REQUEST else "view") + ".html"

            print "gen_view, attempting to find file: ", template_path
            if os.path.exists(
                    os.path.join(settings.CWD, "templates", template_path)):
                template = template_path
            else:
                template = "generic/gen_view.html"
            print template
            context["_template"] = template
            # render_to_response((template if template else
            # "generic/gen_view.html"), context,
            return context
            #                 context_instance=RequestContext(request))
    lv = wideio_view()(lv)
    if len(decorators) > 0:
        for dec in decorators:
            lv = dec(lv)
    nlv = lv
    if name is None:
        name = DocClass.__name__
    nlv.base_name = name
    nlv.view_name = "view"
    nlv.default_url = "%s/view/([^/]+)" % (name, )
    nlv.rest_url = "%s/([^/]+)" % (name,)
    nlv.rest_method = "GET"
    nlv.wadl = [(nlv.base_name + "/" + nlv.view_name, {
        "url": "%s/view/-a-p-i-/" % (name, ),
        "method": "GET",
        "arguments": []
    }
    )]
    return nlv

