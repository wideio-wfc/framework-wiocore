# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
import logging
from wioframework.utilviews.base import *

def gupdate_view(ODataForm, template=None, decorators=[],
                 name=None, permissions=None):
    """
    Generic Update View
    """
    def lv(request, oid=None, *args, **kwargs):
        files_to_afiles(ODataForm.Meta.model, request)
        
        
        DataForm = ODataForm
        context = create_base_form_context(DataForm.Meta.model, settings=settings, request=request)

        ##
        ## GET THE OBJECT
        ##
        if oid is None:
            if "oid" in request.REQUEST:
                oid = request.REQUEST["oid"]
            else:
                oid = filter(
                    lambda x: len(x) != 0, request.path.split("/"))[-1]
        obj = solveoid(DataForm.Meta.model, oid)
        
        ##
        ## CAN WE ACTUALLY UPDATE THAT OBJECT ?
        ##
        if hasattr(obj, "can_update") and not obj.can_update(request):
            raise PermissionException()
        if permissions:
            if not permissions["update"](obj, request):
                raise PermissionException()


        # IN CASE OF SOLVE BROKEN REFERENCE            
        def keep_if_ok(a0):
              try:
                for f in filter(lambda x:type(x).__name__ in ["ForeignKey","OneToOneField"],a0._meta.fields):
                  getattr(a0,f.name)
              except Exception,e:
                logging.warning("failed to solve references... trying automatic fixes %r %f"%(a0,f.name))
                if hasattr(a0,"get_"+f.name):
                    try:
                        setattr(a0,f.name,getattr(a0,"get_"+f.name)())
                        a0.save()                                    
                        return keep_if_ok(a0)
                    except:
                        pass
                if f.name=="image":
                  try:
                    setattr(a0,f.name,None)
                    a0.save()                                    
                    return keep_if_ok(a0)
                  except:
                    pass
                if request.user.is_staff:
                  try:
                    messages.add_message(request, messages.WARNING, "object discarded from listing because of invalid reference:"+str(e))
                  except Exception,e:
                    logging.error("object discarded from listing because of invalid reference")
                return False
              return True
        keep_if_ok(obj)
            
            
            
        # SPECIFIC HANDLERS FOR DRAFTABLE TYPES
        # FIXME: BY USING DRAFT FOR INSTEAD OF DRAFT_ID WE COULD AVOID THE FIELD IS_DRAFT
        # CHECK IF DRAFTABLE
        draft = None
        wiom = (DataForm.Meta.model.WIDEIO_Meta if hasattr(DataForm.Meta.model,"WIDEIO_Meta") else None)
        
        if hasattr(obj, "_draft_for_id") and ((obj._draft_for_id is not None) or (int(request.REQUEST.get("_USE_DRAFT", "0")) != 0)):
            if obj.wiostate.startswith('U'):
                # WE ARE ALREADY WORKING ON THE DRAFT
                draft = obj

            if obj.wiostate == 'V':  # I changed
                if hasattr(obj.WIDEIO_Meta, "OWNER_FIELD"):
                        # check for "my draft"
                    rd = obj.rev_draft.filter(
                        **{obj.WIDEIO_Meta.OWNER_FIELD + "_id": request.user.id})

                    if (rd.count() != 0):
                        draft = rd[0]
                else:
                    # check for "any draft"
                    rd = obj.rev_draft.all()
                    if (rd.count() != 0):
                        draft = rd[0]

                #obj = obj.__class__.objects.get(draft_for_id=obj.id)

                if draft is None:
                    # CREATE A DRAFT
                    draft = DataForm.Meta.model()
                    obj.copy_draft(draft)
                    draft.wiostate = 'U1'  # DRAFT WITH SOME DATA
                    draft._draft_for_id = obj.id
                    if hasattr(obj.WIDEIO_Meta, "OWNER_FIELD"):
                        setattr(
                            draft,
                            obj.WIDEIO_Meta.OWNER_FIELD,
                            request.user)
                    draft.save()
                    # TODO SET THE OWNER IF NECESSARY

            assert(draft is not None)
            
            ##
            # SAVE INTO THE DRAFT UNLESS QUERY SAYS COMMIT
            ##
            try:
                if (request.REQUEST.get("_COMMIT", "")):
                    target_object = draft._draft_for
                    if target_object is None:
                        target_object = draft
                else:
                    target_object = draft
            except:
                from backoffice import lib as debug
                debug.log_error("could not access draft object")
                target_object = obj
        else:
            target_object = obj
            draft = None

            
            
            
            
        # THIS ALLOW THE CREATION OF MICRO FORMS UPDATE FOR EDITABLE
        # if (request.REQUEST.has_key('MFEKEY')):
            # try:
            #from django.forms.models import modelform_factory
            # if hasattr(DataForm.Meta,"exclude"):
            # if (request.REQUEST['MFEKEY'] in DataForm.Meta.exclude):
            # print "PERMISSION EXCEPTION"
            #raise Exception()
            # DataForm=modelform_factory(DataForm.Meta.model,fields=(request.REQUEST['MFEKEY'],))
            # except Exception,e:
            # print "EXCEPT",e

        saved_target = target_object.__dict__

        
        
        ##
        ## CUSTOMIZE TARGET FOR INLINE EDITING (THIS DISABLES DRAFT ?)
        ##
        if ('_KEYS' in request.REQUEST):  # TODO: FIX INLINE EDITORS
            try:
                from django.forms.models import modelform_factory
                if hasattr(DataForm.Meta, "exclude"):
                    if (request.REQUEST['_KEYS'] in DataForm.Meta.exclude):
                        logging.warning("[security] attempt to generate a micro-form with an excluded field")                        
                        raise PermissionException
                dmodel = DataForm.Meta.model
                formfield_callback_fct = get_formfield_callback(dmodel)
                DataForm = modelform_factory(dmodel, fields=tuple(filter( lambda c: c[0] != "_", request.REQUEST['_KEYS'].split(","))), formfield_callback=formfield_callback_fct)
            except Exception as e:
                logging.error("Error generating microform")
                
                

        #
        # OK WE FILL IN THE FORM ACCORDING TO THE REQUEST
        # IT SEEMS THAT WE NEED TO DO THIS AS APPARENTLY AS DJANGO AS MORE BUGS THAN EXPECTED
        #
        if request.method == 'POST':
            try:
                if ((draft) and (draft != target_object)):
                    draft.copy_draft(target_object)
                # FOR SOME STRANGE REASON THIS DOES NOT SET THE INITAL DATA
                # CORRECTLY
                form = DataForm( request.POST, request.FILES, instance=target_object)  # initial=target_object.__dict__,
                # cd1=dict(form.data.items())
                # form.data=target_object.__dict__
                # form.data.update(cd1)
                # print cd1
                # if hasattr(form,"cleaned_data"):
                # cd2=dict(form.cleaned_data.items())
                # form.cleaned_data=target_object.__dict__
                # form.cleaned_data.update(cd2)
                # print cd2
                orig_data=form.data
                form.data = dict(form.data.items())
                form.data["_orig_data"]=orig_data
                hcd = hasattr(form, "cleaned_data")
                logging.debug( ":: cleaning data")
                if hcd:
                    form.cleaned_data = dict(form.cleaned_data.items())
                logging.debug(":: form.fields :: %r"%(form.fields,))
                for x in target_object._meta.fields:
                    k = str(x.name)
                    logging.debug(":: :: %r"%(k,))
                    if k not in form.fields:
                        if k in form.data:
                            form.data[k] = getattr(target_object, k)
                            logging.debug("CP1")
                        else:
                            if k in request.REQUEST:
                                form.data[k] = request.REQUEST[k]
                                logging.debug("HARDINIT")
                            elif hasattr(request, "FILES") and k in request.FILES:
                                form.data[k] = request.FILES[k]
                                logging.debug("HARDINIT F")
                            else:
                                form.data[k] = getattr(target_object, k)
                                logging.debug("CP2")
                        if hcd:
                            if k in form.cleaned_data:
                                form.cleaned_data[k] = getattr(
                                    target_object,
                                    k)
                            else:
                                if k in request.REQUEST:
                                    form.cleaned_data[k] = request.REQUEST[k]
                                else:
                                    form.cleaned_data[k] = getattr(
                                        target_object,
                                        k)
                    else:
                        logging.debug("ORIG %r"%(getattr(target_object, k),))
                        if (k not in request.POST) and (
                                (not hasattr(request, "FILES")) or (k not in request.FILES)):
                            form.data[k] = getattr(target_object, k)
            except Exception as e:
                logging.error("Error setting data from framework data")
            assert(form.instance == target_object)
            # v SEEMS TO CREATE LOTS OF DUPLICATES
            # form.instance=modelforms.construct_instance(form,form.instance)
        else:
            form = DataForm(instance=target_object)

        # CHECK: # for some reason if this is not added having no errors will
        # stop the page from loading
        
        ##
        ## OK GET READY TO GET THE OUTPUT OF THE FORM
        ##
        form.instance.form_app_errors = None
        set_request_in_widgets(form, request)
        set_instance_in_widgets(form, obj) #< WARNING: CONNECTED OBJECTS ARE NOT FULLY COMPATIBLE WITH DRAFT MODE| WE ASSOCIATE THEM WITH LIVE INSTANCE
        
        
        try:
            form.instance = form.save(commit=False)
            ckwargs = {}
            if hasattr(form, 'Meta') and hasattr(form.Meta, 'exclude'):
                if len(form.Meta.exclude):
                   ckwargs['exclude'] = form.Meta.exclude
            form.instance.clean_fields(**ckwargs)
        except ValueError:
            form.instance.form_db_valid = False
            try:
                ckwargs = {}
                if hasattr(form, 'Meta') and hasattr(form.Meta, 'exclude'):
                  if len(form.Meta.exclude):                    
                        ckwargs['exclude'] = form.Meta.exclude
                form.instance.clean_fields(ckwargs)
            except exceptions.ValidationError:
                pass
        except exceptions.ValidationError:
            pass

        # YET ANOTHER BUGFIX... SOMEFIELDS SEEM NOT TO BE SET
        for k in form.data.items():
            if hasattr(form.instance, k[0] + "_id") and (type(k[1]) in [str, unicode]):
                v = getattr(form.instance, k[0] + "_id")
                if v != k[1]:
                    setattr(form.instance, k[0] + "_id", k[1])

                    
                    

        ## OD WE SHALL BE READY
        logging.debug( ":: cleaned data %r"%( form.data,))

        if request.method == 'POST':
            ## OK THIS IS A PROPER POST SO 
            ## SO IF THE DATA VALIDATES IT WILL BE SAVED
            if form.instance.form_db_valid:  # FIXME < HARD/SOFT VALIDATION
                # TODO: SOFT / HARD VALIDATION (WE NEED CLARIFY) # SOFT = required by the database but not enough the app (i.e. can be saved but would be invalid) | HARD  = required by app to run
                # TODO: SECURITY: VALIDATE THAT ONLY MODIFIABLE ITEMS ARE MODIFIED
                # for fi in form.cleaned_data.items(): # <CLEANED DATA CONTAIN
                # BOOLEAN INSTEAD OF VALUES FOR FOREIGN KEY : NEED CHECKS
                for fi in form.data.items():
                    if hasattr(
                            target_object, fi[0] + "_id") and (type(fi[1]) in [str, unicode]):
                        v = getattr(target_object, fi[0] + "_id")
                        if v != fi[1]:
                            setattr(target_object, fi[0] + "_id", fi[1])
                    else:
                        setattr(target_object, fi[0], fi[1])
                        f = getattr(target_object, fi[0])
                        # if hasattr(f,"save"):
                        #    f.save()
                #if hasattr(target_object, "picture"):
                #    print "TO", target_object.picture, form.instance.picture
                if hasattr(target_object, "on_db_update"):
                    target_object.on_db_update(request)
                target_object.save()

                if form.instance.form_app_valid:
                    context = {
                        "res": str(oid),
                        "_signal": "alert('update_success');",
                        'apperrors': form.instance.form_app_errors,
                        'appvalidated': form.instance.form_app_validated
                    }
                    if target_object.wiostate.startswith('U') and target_object._draft_for is None:
                        if (request.REQUEST.get("_COMMIT", "")):                            
                            target_object.wiostate = 'V'
                    if hasattr(target_object, 'request_moderation'):
                            target_object.request_moderation(request)
                    if target_object.wiostate == 'V':
                        logging.info("VALIDATED")
                        if hasattr(target_object, "on_update"):
                          cu = target_object.on_update(request)
                          if cu is not None:
                            context.update(cu)                                            
                    target_object.save()                    
                    if hasattr(obj, "draft_id"):
                        if "_submit" in request.REQUEST and request.REQUEST["_submit"].lower() == "submit":
                            obj.request_moderation(request)
                    if not "_redirect" in context:
                        add_redirect_for_object(target_object, context, request, False)
                    else:
                        add_redirect(context, context["_redirect"], request)
                    return context
                else:
                    try:
                        messages.add_message(request, messages.INFO, 'The submitted input did not validate (APP)')
                    except:
                        pass
            else:
                try:
                    messages.add_message(request, messages.INFO, 'The submitted input did not validate (DB)')
                    messages.add_message(request, messages.INFO, (form.instance.form_app_valid))                    
                    messages.add_message(request, messages.INFO, (form.instance.form_db_valid))                    
                    
                    for a in form.instance.form_app_errors:                      
                       messages.add_message( request, messages.INFO, str(a))
                    for a in form.instance.form_db_errors.items():                      
                       messages.add_message(request, messages.WARNING, str(a))
                       
                except:
                    pass
                # if not (request.REQUEST.has_key("AJAX") and request.REQUEST["AJAX"] == "1"):
                #    #context['prologue']=get_prologue(form)
                #    #set_request_in_widgets(form,request)
                #    return HttpResponseRedirect(obj.get_view_url())
            # elif request.REQUEST.has_key("_AJAX") and request.REQUEST["_AJAX"] == "1":
                # return {"res": str(oid),
                # "$signal":"alert('update_failure');"}
        else:
            #messages.add_message(request, messages.INFO, 'Method is not POST:'+request.method)
            pass
        context = {
            'is_a_post_reply': request.method == 'POST',
            'object': target_object,
            'apperrors': form.instance.form_app_errors,
            'reviewed': hasattr(obj, "draft_id"),
            'ajax': int(request.REQUEST.get("_AJAX", "0"))
        }
        if hasattr(form.instance, "form_app_validated"):
            context['appvalidated'] = form.instance.form_app_validated
        else:
            if not request.data_mode:
                context['appvalidated'] = TrueDict

        if not request.data_mode:
            context["NO_DRAFT"] = (
                wiom and hasattr(wiom, "NO_DRAFT") and wiom.NO_DRAFT)
            context['form'] = form
            context['prologue'] = get_prologue(form)
            pill = filter(len, (request.GET.get("_PIL", "")).split(","))
            if "_PIL" not in request.GET:
                pild = {}
                pild['wiostate'] = 'U1'
                if hasattr(DataForm.Meta.model.WIDEIO_Meta, "owner_field"):
                    pild[
                        DataForm.Meta.model.WIDEIO_Meta.owner_field] = request.user
                    # FIXME: THIS IS ACTUALLY DIRECTLY PIL, THERE IS NO NEED TO
                    # MAP ID TO RECONSTRUCT OBJECTS (PERF)
                    pill = map(
                        lambda x: x.id,
                        form.Meta.model.objects.filter(
                            **pild))
                else:
                    pild['_draft_for_id'] = target_object.id
                    try:
                        pill = form.Meta.model.objects.filter(**pild)
                    except:
                        pill = []
            context['pilc'] = len(pill)
            context['pil'] = pill
        context["_template"] = (template if template else 'generic/gen_update.html')
        return context
    lv = wideio_view()(lv)
    decorators = [permission_based] + decorators
    if (len(decorators) > 0):
        for dec in decorators:
            lv = dec(lv)
    nlv = lv
    if (name is None):
        if (hasattr(ODataForm, "Meta")):
            name = ODataForm.Meta.model.__name__
        else:
            name = ODataForm.__name__
    nlv.base_name = name
    nlv.view_name = "update"
    nlv.default_url = "%s/update/([^/]+)" % (name,)
    nlv.rest_url = "%s/([^/]+)" % (name,)
    nlv.rest_method = "POST"
    nlv.wadl = [(nlv.base_name + "/" + nlv.view_name, {
        "url": "%s/update/-a-p-i-/" % (name,),
        "method": "POST",
        "arguments": [("oid", "str", [])] + map(lambda x:(x.name, str(type(x)), []), ODataForm.Meta.model._meta.fields)
    }
    )]
    return nlv
