# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
from wioframework.utilviews.base import *

def gadd_view(DataForm, template=None, decorators=[], name=None,
              permissions=None):
    """
    This generates a add view for a specific class.

    The add view works as follow:

    - if the object support drafts :
        1) an empty object is created/searched based on a context id -> the user is redirected to the update page to finish its create
    - if the object does not support draft or if the previous failed then a normal add form is displayed

    """
    def lv(request, *args, **kwargs):
        template = False
        commit_valid = False
        model = DataForm.Meta.model
        # CHECK WE ACTUALLY ALLOW ADD BEFORE TO DO ANYTHING
        if hasattr(model, "can_add") and not model.can_add(
                request):
            raise PermissionException()

        if (permissions):
            if not permissions["add"](request):
                raise PermissionException()

        # DOWNLOAD THE FILES OF THE REQUEST
        files_to_afiles(model, request)

        # PREPARE THE INITAL CONTEXT
        context = create_base_form_context(model)

        # GET THE SUBMITTED DATA
        # FIXME: REFACTOR NEEDED TO REMOVE SPAGHETTI
        if "_INVOKE" in request.GET: 
            value = str(request.GET.get("_INVOKE"))
            if hasattr(DataForm.Meta.model, 'invoke'):
                new_obj = DataForm.Meta.model.invoke(value, request)
            elif hasattr(DataForm.Meta.model, 'widget_add_unknown'):
                new_obj = DataForm.Meta.model()
                invoke_extra = wjson.loads(request.GET.get('_INVOKE_EXTRA', '{}'))
                for key, val in invoke_extra.items():
                    new_obj.__setattr__(key, val)
                new_obj.save()
            if new_obj:
                data = {"res": str(new_obj.id)}
                if not "_redirect" in data:
                    add_redirect_for_object(new_obj, data, request)
                else:
                    add_redirect(data, data["_redirect"], request)
                return data
            else:
                raise Http404("Unable to invoke " + str(DataForm.Meta.model) )

        form_class = DataForm
        if hasattr(model.WIDEIO_Meta, "mandatory_fields") and not request.data_mode:
            from django.forms.models import modelform_factory
            formfield_callback_fct = get_formfield_callback(model)
            form_class = modelform_factory(
                DataForm.Meta.model,
                fields=tuple(
                    filter(
                        lambda c: c[0] != "_",
                        model.WIDEIO_Meta.mandatory_fields)),
                formfield_callback=formfield_callback_fct)
        # IS THERE A DIFFERENCE BETWEEN POST AND GET FOR MULTIPLE SELECT FIELDS

        if request.method == 'POST':
            form = form_class(request.POST, request.FILES)
        else:
            form = form_class(request.REQUEST)
        if type(form.data!=dict):
           orig_data=form.data
           form.data=dict(form.data.items())
           form.data["_orig_data"]=orig_data
        # ALLOW TO REMOVE FIELD VIA GET
        # PASSING ARGUMENTS IN QUERY FOR M2M, REL... FIXME:DANGEROUS?
        # (PERMISSIONS)
        for x in request.GET.keys():
            if x in form.fields:
                form.fields[x].widget = django.forms.widgets.HiddenInput()

        set_request_in_widgets(form, request)

        wiom = DataForm.Meta.model.WIDEIO_Meta if hasattr(
            DataForm.Meta.model,
            "WIDEIO_Meta") else None

        if request.method == 'POST':
            # OK ARE THE DATA VALID
            from termcolor import cprint
            try:
                cprint(repr(request.POST), 'green')
                form.instance = form.save(commit=False)
                cprint(repr(form.instance.__dict__), 'green')
            except ValueError as e:
                cprint('ValueError: ' + str(e), 'red')
                form.instance.form_db_valid = False
                pass
            try:
                ckwargs = {}
                if hasattr(form, 'Meta') and hasattr(form.Meta, 'exclude'):
                    ckwargs['exclude'] = form.Meta.exclude
                form.instance.clean_fields(**ckwargs)
            except exceptions.ValidationError as e:
                print "ValidationError", e
                pass

            # FIXME: QUICKHACK -> form.instance may have problems of being not
            # unique
            if form.instance.form_db_valid:
                try:
                    if hasattr(
                            form.instance, "errors") and form.instance.errors:
                        form.instance.form_db_valid = False
                except:
                    form.instance.form_db_valid = False

            if form.instance.form_db_valid:
                if hasattr(form.instance, "errors") and form.instance.errors:
                    raise Exception(
                        form.instance.errors,
                        request.REQUEST.items())
                new_object = form.instance
                additional_context = {}
                if not form.instance.form_app_valid:
                    if wiom and hasattr(wiom, "disable_drafts"):
                        if wiom.disable_drafts == True:
                            commit_valid = False

                if (wiom and hasattr(wiom, "NO_DRAFT")
                        and wiom.NO_DRAFT) or form.instance.form_app_valid:
                    if (int(request.REQUEST.get("_USE_DRAFT", "0")) != 0) and (
                            not (request.REQUEST.get("_COMMIT", ""))):
                        new_object.wiostate = 'U1'
                    else:
                        new_object.wiostate = 'V'
                    commit_valid = True

                if commit_valid:
                    ## FIXME: MAY BE GOOD TO DIFFERENTIATE PRE_MODERATION / POST_MODERATION
                    if new_object.wiostate == 'V' and hasattr(
                            new_object, "on_add"):
                        additional_context = new_object.on_add(request)
                        if additional_context is None:
                            additional_context = {}
                        elif not isinstance(additional_context, dict):
                            raise TypeError(
                                "The function on_add must return a dict in ",
                                new_object.__class__)

                    new_object.save()
                    
                    # assert(DataForm.Meta.model.object.filter(id=new_object.id).count()==1)

                    #]if (hasattr(new_object, "draft_id")):
                    #    new_object.is_draft = True
                    #    new_object.save()

                    if hasattr(new_object, 'request_moderation'):
                            new_object.request_moderation(request)
                            #FIXME: IS MODERATION IS REQUESTED THE STATE OF THE OBJECT SHOULD NOT BE PUBLIC

                    if hasattr(form, "save_m2m"):
                        form.save_m2m()
                        
                    if "_ADD_BACKLINK" in request.GET:
                        from django.apps.registry import apps
                        #mdl=filter(lambda x:new_object.__class__.__name__==x.__name__, apps.get_models())[-1]
                        #mdl=apps.get_model('network.QNA')
                        mdl=apps.get_model(".".join(request.GET["_ADD_BACKLINK"].split(".")[:-1]))
                        to=mdl.objects.get(id=request.GET["_BACKLINK_ID"])
                        #if mdl!=new_object.__class__:
                        #  new_object=mdl.objects.get(id=new_object.id)
                        #  cr=dir(new_object)
                        #getattr(new_object,str(request.GET["_ADD_BACKLINK"].split(".")[-1])).add(self)#request.GET["_BACKLINK_ID"])
                        #new_object.save()
                        #al=new_object._meta.get_all_related_many_to_many_objects()
                        #aln=map(lambda x:x.name,al)
                        #bo=filter(lambda x:x.name==request.GET["_ADD_BACKLINK"],al)[0].model.get(id=request.GET["_BACKLINK_ID"])
                        # FIXME: TODO SECURITY
                        if hasattr(to,"on_backlink"):
                            to.on_backlink(request,new_object)
                        if  request.GET["_ADD_BACKLINK"] in mdl.WIDEIO_Meta.form_exclude:
                            raise Exception, "Permission Exception (BACKLINK/EXCLUDE)"
                        getattr(to,request.GET["_ADD_BACKLINK"].split(".")[-1]).add(new_object.id)
                        if not "_redirect" in  additional_context:
                          additional_context["_redirect"]= to.get_view_url()
                        

                    data = {"res": str(new_object.id)}
                    data.update(additional_context)
                    # messages.add_message(request, messages.INFO, 'Instance
                    # successfully created.') # FIXME: CHANGE TO CLIENT SIGNAL
                    data[
                        "_client_signal"] = "add_message(INFO,'Instance successfully created'.);"
                    if not "_redirect" in data:
                        add_redirect_for_object(new_object, data, request)
                    else:
                        add_redirect(data, data["_redirect"], request)
                    return data

        pil = []

        if request.method != 'POST':
            if (wiom and ((not hasattr(wiom, "NO_DRAFT"))
                          or (not wiom.NO_DRAFT))):
                # either we create an empty instance and we redirect to update
                try:
                    pilk = filter(lambda k: k[0] != "_", request.GET.keys())
                    ni = None
                    if pilk:
                        pild = dict(map(lambda k: (k, request.GET[k]), pilk))
                        if hasattr(form.Meta.model.WIDEIO_Meta, "owner_field"):
                            pild[
                                form.Meta.model.WIDEIO_Meta.owner_field] = request.user
                        pild['wiostate'] = 'U1'
                        pil = form.Meta.model.objects.filter(**pild)
                        if pil.count() != 0:
                            ni = pil[0]
                    else:
                        pild = {}
                        if hasattr(form.Meta.model.WIDEIO_Meta, "owner_field"):
                            pild[
                                form.Meta.model.WIDEIO_Meta.owner_field] = request.user
                            pild['wiostate'] = 'U1'
                            pil = list(form.Meta.model.objects.filter(**pild))
                    if ni is None:
                        # FIXME: WE PROBABLY SHOULD SEPARATE EMPTY OBJECT FROM
                        # PARTIALLY FILLED OBJECTS AS EMPTY OBJECT HAVE NO REAL
                        # VALUE AND
                        ni = form.save(commit=False)
                        # AND SHOULD NOT BE LISTED AS DRAFT IN PIL
                        ni.wiostate = 'U0'
                        ni.save()
                        if has_attr(self,"on_preadd"):
                            ni.on_preadd(request)
                            ni.save()
                        #ni.on_add(request) #< NO ON ADD SHOULD BE CALLED ONLY WHEN VALIDATED FIXME: MAY BE USEFUL TO HAVE A DIFFERENT CALL IN CASE WE WANT TO PRESET OWNERS, ETC...
                        #ni.save()
                    return {'_redirect':  # redirect_url(
                            ni.get_update_url() + "?" +
                            urllib.urlencode(dict([("_PIL", ",".join(pil)) + request.GET.items()]))}  # REDIRECT TO UPDATE OF NEW CANDIDATES
                except Exception as e:
                    # raise
                    # the object is probably not default constructible let's restrict the keys to only mandatory elements
                    # if they are provided
                    if hasattr(
                            wiom, "mandatory_fields"):
                        for x in form.fields:
                            if x not in wiom.mandatory_fields:
                                form.fields[
                                    x].widget = django.forms.widgets.HiddenInput()
                    if hasattr(form.instance, "form_app_validated"):
                        delattr(form.instance, "form_app_validated")
                        delattr(form.instance, "form_app_errors")
                        #delattr( form.instance,"form_db_validated")
                        #delattr( form.instance,"form_db_errors")

        context.update(
            {'form': form, 'is_a_post_reply': request.method == 'POST'})
        #context={'is_a_post_reply':request.method == 'POST'}

        if not request.data_mode:
            context['form'] = form

        context['prologue'] = get_prologue(form)
        context["inst"] = form.instance
        if hasattr(form.instance, "form_app_validated"):
            context['appvalidated'] = form.instance.form_app_validated
            context['apperrors'] = form.instance.form_app_errors
            if hasattr(form.instance, "errors"):
                context['formerrors'] = form.instance.errors
        else:
            if not request.data_mode:
                context['appvalidated'] = TrueDict

        context["_template"] = (
            template if template else 'generic/gen_add.html')
        context["url"] = DataForm.Meta.model.get_add_url(
        ) + "?" + urllib.urlencode(request.GET.items())
        context["ajax"] = int(request.REQUEST.get("_AJAX", "0"))
        context["pil"] = pil
        context["request_get"] = request.GET.items()
        context["request_post"] = request.POST.items()
        context["form_fields"] = request.POST.items()
        context["NO_DRAFT"] = wiom and hasattr(
            wiom,
            "NO_DRAFT") and wiom.NO_DRAFT

        if hasattr(context, "_redirect"):
            add_redirect(context, context["_redirect"], request)

        return context

    lv = wideio_view()(lv)
    decorators = [permission_based] + decorators
    if len(decorators) > 0:
        for dec in decorators:
            lv = dec(lv)
    nlv = lv
    if hasattr(DataForm, "Meta"):
        name = DataForm.Meta.model.__name__
    else:
        name = DataForm.__name__
    nlv.base_name = name
    nlv.view_name = "add"
    nlv.default_url = "%s/add" % (name,)
    nlv.rest_url = "%s/"%(name,)
    nlv.rest_method = "PUT"
    nlv.wadl = [(nlv.base_name + "/" + nlv.view_name, {
        "url": "%s/add/" % (name,),
        "method": "POST",
        "arguments": map(lambda x: (x.name, str(x), []), DataForm.Meta.model._meta.fields)
    })]
    return nlv
