# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
from wioframework.utilviews.base import *

def gdel_view(DocClass, template=None, decorators=[], name=None,
              permissions=None):
    """
    Generic Delete View
    """
    def lv(request, oid=None, *args, **kwargs):
        context = {'request': request, 'settings': settings}
        if oid is None:
            if "oid" in request.REQUEST:
                oid = request.REQUEST["oid"]
            else:
                oid = filter(
                    lambda x: len(x) != 0, request.path.split("/"))[-1]
        obj = DocClass.objects.get(id=oid)
        assert(obj is not None)
        if (obj.wiostate not in [ 'U0', 'U1', 'V' ]) and (not request.user.is_staff):
          raise Http404

        if request.method == 'POST':
            form = DocClass(request.REQUEST)
        else:
            # Delete only accept the post method
            # raise Exception("This URL must be called via a POST request.")
            return {
                '_template': (template if template else 'generic/gen_delete.html'),
                "object": obj,
                "class_s": get_class_singular(DocClass),
                "_signal": "modal('" + obj.get_delete_url() + "');"
            }
            
        obj.wiostate="D" # SCHEDULED FOR DELETION
        obj.save()             

        if hasattr(obj, "can_delete") and not obj.can_delete(request):
            raise PermissionException()
        if (permissions):
            if not permissions["delete"](obj, request):
                raise PermissionException()
        if hasattr(obj, "on_delete"):
            obj.on_delete(request)


        obj.delete()

        ##
        ## TO;DO:
        ## CASCADE THE CHANGE OF STATE BY REMOVING DISCARDING ALL OBJECTS REFERNECING TO THAT OBJECT (NOTIFICATIONS...)
        ## IF NULL REFERENCE ARE ACCEPTED DO SET LINK TO NULL OTHERWISE DISCARD OBJECT
        ## 
        
        r = {'result': 'ok', 'id': oid}
        context.update(r)
        return {"res": r, "_signal":
                "alert('delete_success');", "_redirect": redirect_url(DocClass.get_list_url(), request)}
    lv = wideio_view()(lv)
    decorators = [permission_based] + decorators
    if len(decorators) > 0:
        for dec in decorators:
            lv = dec(lv)

        def nlv(*args, **xargs):
            return lv(*args, **xargs)
    else:
        nlv = lv
    if name is None:
        name = DocClass.__name__
    nlv.base_name = name
    nlv.view_name = "del"
    nlv.default_url = "%s/delete/([^/]+)" % (name,)
    nlv.rest_url = "%s/([^/]+)"%(name,)
    nlv.rest_method = "DELETE"
    
    nlv.wadl = [(nlv.base_name + "/" + nlv.view_name, {
        "url": "%s/del/-a-p-i-/" % (name,),
        "method": "POST",
        "arguments": [("oid", "str")]
    })]
    return nlv

