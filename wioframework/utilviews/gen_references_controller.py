# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
from wioframework.utilviews.base import *

def greferences_view(
        DocClass, template=None, decorators=[], name=None, permissions=None):
    """
    Generic Delete View
    """
    def lv(request, oid=None, *args, **kwargs):
        if not hasattr(DocClass, "get_all_references"):
            return {
                "references": [], "_template": "generic/gen_references.html"}
        if oid is None:
            if "oid" in request.REQUEST:
                oid = request.REQUEST["oid"]
            else:
                oid = filter(
                    lambda x: len(x) != 0, request.path.split("/"))[-1]
        obj = DocClass.objects.get(id=oid)
        assert(obj is not None)
        return {"references": obj.get_all_references(
        ), "_template": "generic/gen_references.html", "x": obj}
    lv = wideio_view()(lv)
    decorators = [permission_based] + decorators
    if (len(decorators) > 0):
        for dec in decorators:
            lv = dec(lv)

        def nlv(*args, **xargs):
            return lv(*args, **xargs)
    else:
        nlv = lv
    if (name is None):
        name = DocClass.__name__
    nlv.base_name = name
    nlv.view_name = "references"
    nlv.default_url = "%s/references/([^/]+)" % (name,)
    nlv.rest_url = "%s/([^/]+)/references" % (name,)
    nlv.rest_method="GET"
    nlv.wadl = [(nlv.base_name + "/" + nlv.view_name, {
        "url": "%s/get/-a-p-i-/" % (name,),
        "method": "GET",
        "arguments": [("oid", "str")]
    }
    )]
    return nlv
