# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
from wioframework.utilviews.base import *

def gschema_view(
        DocClass, template=None, decorators=[], name=None, permissions=None):
    def lv(request):
        #oid=filter(lambda x:len(x)!=0,request.path.split("/"))[-1]
        obj = DocClass
        return {'_template': 'generic/schema.html', 'res':
                map(lambda x: (x.name, ''.join(list(x.description)), (x.__class__.__name__, x.__class__.__module__, ((x.rel.to.__name__, filter(len, x.rel.to.get_base_url().split('/'))) if (hasattr(x, "rel") and x.rel is not None and hasattr(x.rel.to, "get_base_url")) else None))), obj._meta.fields)}
    lv = wideio_view()(lv)
    if len(decorators) > 0:
        for dec in decorators:
            lv = dec(lv)
        # def nlv(*args,**xargs):
        #  return lv(*args,**xargs)
    # else:
    nlv = lv
    if name is None:
        name = DocClass.__name__
    nlv.base_name = name
    nlv.view_name = "schema"
    nlv.default_url = "%s/schema" % (name, )
    nlv.rest_url = "%s/schema" % (name, )
    nlv.method = "GET"
    
    nlv.wadl = [(nlv.base_name + "/" + nlv.view_name, {
        "url": "%s/schema/-a-p-i-/" % (name, ),
        "method": "GET",
        "arguments": []
    }
    )]
    return nlv

