# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# i   mport savmongodbproto
"""
UTILVIEWS: Utility views

These are the generic views that we use to implement all our views or almost.
"""

import os
import re
import hashlib
import random
import sys
import traceback
import urllib
import itertools
import uuid
import math
from django import forms
from django.forms import models as modelforms
from django.core import exceptions
from django.conf.urls import url
from django.contrib.auth.decorators import login_required
from django.contrib import messages
# from django.db import models
import wioframework.fields as models
from django.db.utils import DatabaseError
import django.forms.widgets
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseServerError, Http404
from django.shortcuts import render_to_response, RequestContext, get_object_or_404
from django.template.loader import render_to_string
from django.utils.html import escape
from urlparse import urlparse
from django.core.urlresolvers import resolve, reverse
from django.contrib import messages
import settings
from wioframework.decorators import PermissionException, permission_based
from wioframework.bsonresponse import BsonResponse
from wioframework.jsonresponse import JsonResponse
from accounts.forms import UserAccountFormUser
from wioframework import wjson
from functools import reduce


class TrueDict:
    @staticmethod
    def __getitem__(el):
        return True


def get_class_singular(kls):
    if kls is not None:
        if hasattr(kls, "ctrname_s"):
            return kls.ctrname_s
        else:
            return kls._meta.verbose_name
    return "object"


def get_class_plural(kls):
    if kls is not None:
        if hasattr(kls, "ctrname_p"):
            return getattr(kls, "ctrname_p")
        else:
            if hasattr(kls, "_meta"):
                return kls._meta.verbose_name_plural
    return "objects"


##
# THE GOALS OF THIS VIEW IS TO HANDLE STANDARD JSON / BSON AND OTHER SERIALISATION
# AS WELL AS POSSIBILITY TO ACCESS THE CONTEXT OF TWO VIEWS IN ORDER TO MERGE THEIR RESULT IF NECESSARY ?
##
def redirect_url(url, request):
    persistent_data = ['_AJAX', '_JSON'] + request.GET.keys()
    query = []
    new_url = url + ("&" if url.count("?") else "?")
    for name in persistent_data:
        if name in request.GET:
            query.append(name + "=" + str(request.GET[name]))
        elif name in request.POST:
            query.append(name + "=" + request.POST[name])
    return new_url + "&".join(query)


# def redirect_url(url, request):
# return urllib.urlencode(request.GET)


def add_redirect(context, url, request):
    context["_redirect"] = redirect_url(url, request)


def add_redirect_for_object(obj, data, request, with_update=True):
    # a priori try the following redirect urls in order
    if with_update and obj.can_update(request):
        url = obj.get_update_url()
    elif obj.can_view(request):
        url = obj.get_view_url()
    else:
        url = obj.get_list_url()
    data["_redirect"] = redirect_url(url, request)


def files_to_afiles(docclass, request):
    """
    This function convert all uploaded files to file object that are stored in our database
    and may expire if they are unused.
    """
    from extfields.models import File
    if docclass.__name__ in ['Image', 'File', 'UserAccount']:
        # These types are core files types
        return
    if not hasattr(request, "FILES"):
        return
    if hasattr(request, "DISABLE_AFILES"):
        return
    for f in request.FILES.items():
        o = f[1]
        nf = File()
        nf.name = o.name
        nf.file = o
        # assert(o.file == None) (sometimes :: and this is a BUG !)
        if request.user.is_authenticated():
            nf.owner_id = request.user.id
        nf.save()
        # f[1].close()
        # TO DO FIX ME ADD ABILITY TO CHECK WHAT IS UPLOADED
        # request.POST[ o ] = nf.id
        request.POST[f[0]] = nf.id
        # raise Exception, "test"


def wideio_view(template=None, keep_get_tags=False):
    def dec(f):
        qtemplate = template
        if template is None:
            qtemplate = f.__module__ + "_" + f.__name__ + ".html"

        def xv(request, *args, **kwargs):
            _template = qtemplate
            request.json_mode=request.REQUEST.get("_JSON", "")
            request.data_mode=request.json_mode            
            if request.META.get("CONTENT_TYPE")=="application/json":
                request.data_mode=request.json_mode="1"
            try:
                if (hasattr(f, "is_atomic") and f.is_atomic):
                    with transaction.atomic():
                        ctx = f(request, *args, **kwargs)
                else:
                    ctx = f(request, *args, **kwargs)
            except PermissionException as e:
                ctx = {'err': 'Permission error', "_redirect": redirect_url("/", request),
                       "anonymous": request.user.is_anonymous(), "user": str(request.user)}

                if request.user.is_anonymous:
                    msg = """Please, register or sign to see this page. <a class="btn" href="/accounts/register/"> Register </a><a class="btn" href="/accounts/login/"> Login </a>"""
                else:
                    msg = 'Unfortunately, you don\'t have enough credential for this operation.'
                try:
                    messages.add_message(request, messages.WARNING, msg)
                except:
                    return HttpResponse(msg)
            if not isinstance(ctx, dict):
                print "returning direct reply"
                return ctx
            if "$server_signal" in ctx:
                globals()[ctx['$server_signal']](f, ctx)
            #          if ctx.has_key("$client_signal"):
            #              return HttpResponse(ctx,"application/")
            #            globals()[ctx['$signal']](f,ctx)
            if request.json_mode:
                if "res" in ctx and isinstance(ctx["res"], dict):
                    ctx["res"] = dict(filter(lambda i: i[0][0] != "_",ctx["res"].items()))
                return JsonResponse(ctx)
            if request.REQUEST.get("_BSON", ""):
                if "res" in ctx and isinstance(ctx["res"], dict):
                    ctx["res"] = dict(
                        filter(
                            lambda i: i[0][0] != "_",
                            ctx["res"].items()))
                return BsonResponse(ctx)
            if "_template" in ctx:
                _template = ctx["_template"]
            if "_redirect" in ctx:
                # REDIRECT ARE NOT TO AJAX THEY SHOULD BE MINIMISED...
                print "planned redirect :: ", ctx["_redirect"], hasattr(request, "INTERNAL")
                if not hasattr(request, "INTERNAL"):
                    redirect = ctx["_redirect"]
                    if keep_get_tags:
                        redirect += "?" + urllib.urlencode(dict(request.GET.items()))
                    return HttpResponseRedirect(redirect)
                else:
                    nextv = ctx["_redirect"]

                    class MetaRequest:
                        DPOST = {}
                        DREQUEST = {}
                        DGET = {}
                        POST = dict(
                            {'_AJAX': 1, '_STAFF_DEBUG': request.REQUEST.get("_STAFF_DEBUG", "")})
                        REQUEST = dict(
                            {'_AJAX': 1, '_STAFF_DEBUG': request.REQUEST.get("_STAFF_DEBUG", "")})
                        GET = dict(
                            {'_AJAX': 1, '_STAFF_DEBUG': request.REQUEST.get("_STAFF_DEBUG", "")})
                        path = nextv
                        user = request.user
                        META = request.META.copy()
                        COOKIES = request.COOKIES
                        INTERNAL = True
                        method = "GET"
                        is_request_ajax = True
                        build_absolute_uri = request.build_absolute_uri

                    inrequest = MetaRequest()
                    from django.contrib.messages.storage import default_storage
                    inrequest._messages = default_storage(request)

                    try:
                        view, args, kwargs = resolve(urlparse(nextv)[2])
                    except:
                        raise Exception(
                            'URL Resolution Error : ' + str(urlparse(nextv)[2]))

                    # args['request'] = request
                    args = [inrequest] + list(args)
                    print "ARGS= ", args, nextv
                return view(*args, **kwargs)

            try:
                return render_to_response(
                    _template, ctx, RequestContext(request))
            except Exception as e:
                if request.REQUEST.get("_AJAX", ""):
                    if request.user.is_staff:
                        import traceback
                        return HttpResponse("sys error:" + escape(str(e)) + "|<br /><ul><li>" + '</li><li>\n'.join(
                            map(escape, traceback.format_tb(sys.exc_info()[2]))) + '</li></ul>')
                    else:
                        # TODO: EMAIL STAFF ABOUT THIS
                        return HttpResponse("sys ajax error")
                else:
                    import traceback
                    traceback.print_tb(sys.exc_info()[2])
                    raise

        xv.default_url = f.__name__
        xv.raw = f
        return xv

    return dec


def solveoid(model, oid):
    if len(
            oid) != 36:  # < WHAT ABOUT NAME/TITLE OF LEN 36/ PROBABLY BETTER AS ID.. (FOR THE MOMENT THIS IS AN INNACURATE SPEED UP FOR COMMON NAMES)
        fnl = model._meta.get_all_field_names()
        if "name" in fnl:
            if not hasattr(model.WIDEIO_Meta, "NAME_USE_DASH"):
                r = model.objects.filter(name=oid.replace('-', ' '))
            else:
                r = model.objects.filter(name=oid)
            if r.count() != 0:
                return r[0]
        if "title" in fnl:
            r = model.objects.filter(title=oid.replace('-', ' '))
            if r.count() != 0:
                return r[0]
    try:
        return model.objects.get(id=oid)
    except:
        raise Http404("Unable to resolve")


def safefilename(f):
    if (not re.match("([A-Za-z_0-9])", f)):
        raise PermissionException
    return f


def get_prologue(form):
    p = ""
    for f in form.fields.items():
        if hasattr(f[1].widget, "get_prologue"):
            p += f[1].widget.get_prologue()
    return p


def set_request_in_widgets(form, request):
    for f in form.fields.items():
        if hasattr(f[1].widget, "set_request"):
            f[1].widget.set_request(request)


def set_instance_in_widgets(form, instance):
    for f in form.fields.items():
        if hasattr(f[1].widget, "set_instance"):
            f[1].widget.set_instance(instance)


def get_formfield_callback(model):
    if hasattr(model, "formfield_callback"):
        return model.formfield_callback
    else:
        def formfield_callback_fct(f):
            return f.formfield()
    return formfield_callback_fct


def create_base_form_context(model, **kwargs):
    ctx = {
        'formuuid': hashlib.md5(str(random.random())).hexdigest(),
        'object_class': model,
        'object_s': get_class_singular(model),
        'object_p': get_class_plural(model)
    }
    ctx.update(**kwargs)
    return ctx
