# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
from wioframework.utilviews.base import *

def render_gen_list(DocClass, request, pagedqueryset, permissions, hits, fromidx, toidx, sortby, view_mode="std", perpage=20, html_transform=lambda x: x, elem_view=lambda x: x, list_url=None, wholepage=1,
                    template=None, add_enabled=False, sort_enabled=True, search_enabled=True, sort_default=None, json_view=lambda x: x, _list_mode="", filters=None, **kwargs):
        # RENDER THE RESULT OF THE QUERY
    npages = int(math.ceil(float(hits) / perpage))

    if request.data_mode:
        result = {'npages': npages}
        if request.data_mode == "3":
            result['res'] = [{"id": f.id, "text": unicode(f), "view": f.html(request, request.REQUEST.get('_VIEW_MODE', 'mini'))} for f in pagedqueryset]
        elif request.data_mode == "2":
                    # return JsonResponse([(f.id, unicode(f)) for f in
                    # pagedqueryset])
            result['res'] = [(f.id, unicode(f)) for f in pagedqueryset]
        else:
            result['res'] = [dict(filter(lambda x: x[0][0] != '_', json_view(
            f).__dict__.items())) for f in pagedqueryset]
        return result

    else:
        if request.REQUEST.get("_AJAX", "0") == "2":
            wholepage = 0

        if "_add_enabled" in request.REQUEST:
            Ladd_enabled = int(request.REQUEST["_add_enabled"])
        else:
            Ladd_enabled = add_enabled
        if "_VIEW_MODE" in request.GET:
            view_mode = request.GET["_VIEW_MODE"]
        ##
        # POTENTIALLY TAKE IN ACCOUNTS DIFFERENT MODES
        ##
        cpi = fromidx / perpage
        search_enabled_text  = ''
        if search_enabled == True:
            search_enabled_text = 'Search'
        elif type(search_enabled) in [str, unicode]:
            search_enabled_text = 'Search by ' + search_enabled
        elif type(search_enabled) in [list]:
            search_enabled_text = 'Search by ' + ', '.join(search_enabled[:-1]) + (' or ' if len(search_enabled) > 1 else ' ') + search_enabled[-1]
        context = {
            'ajax': 1,
            '_ajax': 1,
            'sortby': sortby,
            'transform': html_transform,
            'prevrequest': request.REQUEST,
            'counter_s': get_class_singular(DocClass),
            'counter_p': ((DocClass.ctrname_p if hasattr(DocClass, "ctrname_p") else DocClass._meta.verbose_name_plural) if DocClass is not None else "objects"),
            'fields': (DocClass._meta.fields if DocClass is not None else None),
            'object_list': map(elem_view, pagedqueryset),
            'permissions': permissions,
            'is_paginated': request.REQUEST.get("_PAGINATED", (npages > 1)),
            'perpage': perpage,
            'has_next': toidx <= hits,
            'has_previous': fromidx > 0,
            'page': cpi + 1,
            'next': cpi + 2,
            'previous': cpi + 0,
            'first_on_page': fromidx,
            'last_on_page': toidx,
            'pages': npages,
            'hits': hits,
            'page_range': range(max(1, (cpi + 1) - 2), min(npages, (cpi + 1) + 2) + 1),
            'request': request,
            'settings': settings,
            'object_class': DocClass,
            'object_class_meta': (DocClass._meta if DocClass is not None else None),
            'view_mode': view_mode,
            'sort_enabled': sort_enabled,
            'sort_default': sort_default,
            'search_enabled_text': search_enabled_text,
            'search_enabled': search_enabled,
            'filters': filters,
            'wholepage': wholepage,
            'add_enabled': Ladd_enabled,
            'can_add': DocClass.can_add(request) if hasattr(DocClass, "can_add") else False,
            'formuuid': str(uuid.uuid1()),
            'list_url': ((list_url if list_url else DocClass.get_list_url()) if DocClass is not None else request.META["PATH_INFO"])
        }

        context.update(kwargs)
        ctemplate = template
        if (hits == 0):
            if hasattr(DocClass, "html_empty_result"):
                res = DocClass.html_empty_result(request)
                if res:
                    return HttpResponse(res)
        context["_template"] = (
            ctemplate if ctemplate else "generic/gen_list" +
            request.GET.get(
                "_LIST_MODE",
                _list_mode) +
            ".html")  # <FIXME: SECURIY ENSURE _LIST_MODE IS CORRECT
        return context
