# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
import sys
from wioframework.utilviews.base import *

from wioframework.utilviews.gen_list_controller import glist_view, grel_list_view
from wioframework.utilviews.gen_schema_controller import gschema_view
from wioframework.utilviews.gen_add_controller import gadd_view
from wioframework.utilviews.gen_view_controller import gview_view
from wioframework.utilviews.gen_update_controller import gupdate_view
from wioframework.utilviews.gen_del_controller import gdel_view
from wioframework.utilviews.gen_unlink_controller import gunlink_view
from wioframework.utilviews.gen_references_controller import greferences_view


def autourls(module):
    """
    Automatically generates the list of URLs for a specific module.

    based on the variable "VIEWS"
    """
    r = []
    for f in module.VIEWS:
        if (hasattr(f, "default_url")):
            default_url = getattr(f, "default_url")
            basename = getattr(f, "base_name", "wideio")
            viewname = getattr(
                f,
                "view_name",
                (f.__name__ if f.__name__ != "lv" else default_url.replace(
                    '/',
                    '_')))
            #r.append(url('^%s/$'%(default_url,),    view=f))
            r.append(url('^%s/$' %
                         (default_url.lower(),), view=f, name='%s_%s' %
                         (basename, viewname)))

        else:
            import settings
            if settings.DEBUG:
              print "ERR", f, "has no default_url"
            pass
    return r


def autourls2(module):
    """
    Automatically generates the list of URLs for a specific module.

    based on the variable reflexion and attributes.
    """
    r = []
    for m in dir(module):
        f = getattr(module, m)
        if (hasattr(f, "default_url")):
            default_url = getattr(f, "default_url")
            basename = getattr(f, "base_name", "wideio")
            viewname = getattr(
                f,
                "view_name",
                (f.__name__ if f.__name__ != "lv" else default_url.replace(
                    '/',
                    '_')))
            # r.append(url('^%s/$'%(default_url,)))
            r.append(url('^%s/$' %
                         (default_url.lower(),), view=f, name='%s_%s' %
                         (basename, viewname)))
        else:
            import settings
            if settings.DEBUG:
              print "ERR", f, "has no default_url"
            pass
    return r


def AUDLV2(dform, *args, **xargs):
    """
    Creates the add, update, delete, list and view views as well as the related views based on a form or on a model.
    Split arguments to all views intelligently.
    """

    ##
    # Have we received a model ? Then create a default form
    ##
    # print "Is subclass? ", issubclass(dform,models.Model)
    if issubclass(dform, models.Model):  # hasattr(dform,"_meta"):
        dmodel = dform

        class MetaTForm:
            model = dmodel
        if (hasattr(dmodel, "WIDEIO_Meta")):
            for m in filter(
                    lambda m: m.startswith("form_"), dir(dmodel.WIDEIO_Meta)):
                setattr(MetaTForm, m[5:], getattr(dmodel.WIDEIO_Meta, m))

        formfield_callback_fct = get_formfield_callback(dmodel)

        # TODO: Reflect similar logic in django.db.models.fields
        #       [Insert dummy value as a marker to overcome blank field]
        class TForm(forms.ModelForm):
            formfield_callback = formfield_callback_fct
            Meta = MetaTForm
            # def _clean_fields(self):
            # Check that the model allows partial submission, otherwise use the default behaviour for validation
            # if hasattr(dmodel, "allow_partial_submit") and dmodel.allow_partial_submit():
            #from django.core.exceptions import ValidationError
            #from django.forms.fields import FileField
            #vfc = 0
            # for name, field in self.fields.items():
            # value_from_datadict() gets the data from the data dictionaries.
            # Each widget type knows how to retrieve its own data, because some
            # widgets split data over several HTML fields.
            #value = field.widget.value_from_datadict(self.data, self.files, self.add_prefix(name))
            # try:
            # if isinstance(field, FileField):
            #initial = self.initial.get(name, field.initial)
            #value = field.clean(value, initial)
            # else:
            #value = field.clean(value)
            #self.cleaned_data[name] = value
            # if hasattr(self, 'clean_%s' % name):
            #value = getattr(self, 'clean_%s' % name)()
            #self.cleaned_data[name] = value
            # except ValidationError as e:
            # not the neatest way to check whether the error messages match up...
            # if (field.error_messages['required'] is not None and
            #(unicode(field.error_messages['required']) == e.messages[0])):
            #self.cleaned_data[name] = PreliminaryField()
            # else:
            #self._errors[name] = self.error_class(e.messages)
            # if name in self.cleaned_data:
            #del self.cleaned_data[name]
            # else:
            #vfc = vfc + 1
            # print "Fields successfully validated thus far:", vfc
            # print "Total number of fields to match:", len(self.fields.items())
            # else: # Treat this form as mandatory
            #super(forms.ModelForm, self)._clean_fields()
        Meta = None
        if hasattr(dmodel, "WIDEIO_Meta"):
            Meta = dmodel.WIDEIO_Meta
            if hasattr(Meta, "ADD_CAPTCHA"):
                from wioframework.captcha import fields as cfields
                captcha = cfields.ReCaptchaField(
                    attrs={
                        'theme': 'white'},
                    use_ssl=True)
                #raise Exception,dir(TForm)
                TForm.base_fields["capcha"] = captcha
        dform = TForm
        dmodel.API_Form_Class = dform

    else:
        dmodel = dform.Meta.model

    if (hasattr(dmodel.WIDEIO_Meta, "audlv_xargs")):
        xargs.update(dmodel.WIDEIO_Meta.audlv_xargs)

    ##
    # Prepare the arguments
    ##
    xlargs = dict(map(lambda x: (('_'.join(x[0].split('_')[1:]), x[1]) if ('_' in x[0]) else x),
                      filter(lambda it: ('_' not in it[0]) or it[0].startswith("list_") or it[0].startswith("all_"), xargs.items())))
    xvargs = dict(map(lambda x: (('_'.join(x[0].split('_')[1:]), x[1]) if ('_' in x[0]) else x),
                      filter(lambda it: ('_' not in it[0]) or it[0].startswith("view_") or it[0].startswith("all_"), xargs.items())))
    xdargs = dict(map(lambda x: (('_'.join(x[0].split('_')[1:]), x[1]) if ('_' in x[0]) else x),
                      filter(lambda it: ('_' not in it[0]) or it[0].startswith("del_") or it[0].startswith("all_"), xargs.items())))
    xaargs = dict(map(lambda x: (('_'.join(x[0].split('_')[1:]), x[1]) if ('_' in x[0]) else x),
                      filter(lambda it: ('_' not in it[0]) or it[0].startswith("add_") or it[0].startswith("all_"), xargs.items())))
    xuargs = dict(map(lambda x: (('_'.join(x[0].split('_')[1:]), x[1]) if ('_' in x[0]) else x),
                      filter(lambda it: ('_' not in it[0]) or it[0].startswith("update_") or it[0].startswith("all_"), xargs.items())))

    ##
    # Add the standard views
    ##
    L = [
        (dmodel.__name__ + "_list", glist_view(dmodel, *args, **xlargs)),
        (dmodel.__name__ + "_view", gview_view(dmodel, *args, **xvargs)),
        (dmodel.__name__ + "_del", gdel_view(dmodel, *args, **xdargs)),
        (dmodel.__name__ + "_add", gadd_view(dform, *args, **xaargs)),
        (dmodel.__name__ + "_update", gupdate_view(dform, *args, **xuargs)),
        (dmodel.__name__ + "_unlink", gunlink_view(dmodel, *args, **xuargs)),        
        (dmodel.__name__ + "_references",
         greferences_view(dmodel, *args, **xuargs)),
        (dmodel.__name__ + "_schema", gschema_view(dmodel, *args))
    ]

    ##
    # Add the related views
    ##

    # print dmodel.__name__
    # print dir(dmodel._meta)
    # print dmodel._meta.virtual_fields
    # print dmodel._meta.get_all_related_many_to_many_objects()
    # for f in dmodel._meta.get_all_related_objects():
    #  print f,f.get_accessor_name(),f.name#dir(f)#,f.get_reverse_accessor_name(),dir(f)
    #import sys
    # print sys.trac
    #raise Exception,"where is it called from ??"
    # try:
    # print dform.Meta.exclude
    # except:
    # pass
    # these could also be implemented as property on client side for accessing
    # generic views
    for f in dmodel._meta.get_all_related_objects():
        # print "1",dmodel, f
        try:
            L.append(("rel_" + dmodel.__name__ + "_" + f.get_accessor_name(),
                      grel_list_view(dmodel, f.get_accessor_name(), *args, **xvargs)))
        except Exception as e:
            sys.stderr.write("error (a) registering related view :%r\n" % (e,))
    for f in dmodel._meta.get_all_related_many_to_many_objects():
        # print "2",dmodel, f
        try:
            L.append(("rel_" + dmodel.__name__ + "_" + f.get_accessor_name(),
                      grel_list_view(dmodel, f.get_accessor_name(), *args, **xvargs)))
        except Exception as e:
            sys.stderr.write("error (b) registering related view :%r\n" % (e,))
    for f in dmodel._meta.many_to_many:
        # print "3",dmodel, f
        try:
            L.append(("rel_" + dmodel.__name__ + "_" + str(f),
                      grel_list_view(dmodel, f, *args, **xvargs)))
        except Exception as e:
            sys.stderr.write("error (c) registering related view :%r\n" % (e,))
    if Meta:
      if hasattr(Meta, "Actions"):
        for m in dir(Meta.Actions):
            if m[0] != "_":
                act = getattr(Meta.Actions, m)
                act.view.default_url = "%s/action/%s/([^/]+)" % (
                    dmodel.__name__.lower(), act.act_name)
                act.view.base_name = dmodel.__name__
                act.meta["model"] = dmodel
                try:
                  act.view.view_name = "action_" + act.act_name
                  L.append(
                    ("_".join(["action", dmodel.__name__, act.act_name]), act.view) )
                except Exception,e:
                    print e
      if hasattr(Meta, "Views"):
        for m in dir(Meta.Views):
            if m[0] != "_":
                view = wideio_view()(getattr(Meta.Views, m))
                view.default_url = "%s/subview/%s/([^/]+)" % (
                    dmodel.__name__.lower(), m)
                view.base_name = dmodel.__name__                
                view.view_name = "subview_" + m
                L.append(
                    ("_".join(["subview", dmodel.__name__, m]), view) )

    # print dform,map(lambda x:x.default_url,L)
    # for f in dmodel._meta.get_all_related_objects():
        # print dir(f)
        # print f.model, f.name, f.parent_model, f.var_name, f.field, f.opts
        # print f.bind, f.get_accessor_name()
        # print "f", map(lambda f:f.name,dmodel._meta.fields)
        # print "vf", dmodel._meta.virtual_fields
        # print dir(getattr(dmodel,f.get_accessor_name()))
    # print dmodel._meta.get_all_related_many_to_many_objects()
    # for f in dmodel._meta.fields :
        # print f.name, " ",
        # if (f.name=="followers"):
            # print f.name,  f.__class__.__name__ , dir(f)
        # if f.rel!=None:
            # print f.name,f.rel
            # L.append(grel_list_view(dmodel,f.name,*args,**xvargs))
    return L


def AUDLV(c, *args, **kwargs):
    return map(lambda x: x[1], AUDLV2(c, *args, **kwargs))


def api_info(views):
    """
    TODO: FIX ME this view is supposed to create the API information for a class.
    It would be good to stick to WSDL or some kind of  similar standard
    """
    def lv(request):
        return HttpResponse(
            str(dict(reduce(lambda a, b: (a + b.wadl), views, []))))
    lv.base_name = ""
    lv.view_name = "api_info"
    lv.default_url = "api_info"
    lv.wadl = []
    return lv
