# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
import logging
import settings
from wioframework.utilviews.gen_list_view import *


def keep_if_ok(a0, request):
    """
    Returns true if all the reference of an object are not broken.
    Broken reference give bad UX to user and lead to errors so its better not to show objects with broken references.

    This code is not necessary if databse enforces constraint correctly.

    @param a0:
    @param request:
    @return:
    """
    try:
        for f in filter(lambda x: type(x).__name__ in ["ForeignKey", "OneToOneField"], a0._meta.fields):
            getattr(a0, f.name)
    except Exception, e:
        if hasattr(a0, "get_" + f.name):
            try:
                setattr(a0, f.name, getattr(a0, "get_" + f.name)())
                a0.save()
                return keep_if_ok(a0)
            except:
                pass
        if f.name == "image":
            try:
                setattr(a0, f.name, None)
                a0.save()
                return keep_if_ok(a0)
            except:
                pass
        if request.user.is_staff:
            # move this to handler
            try:
                try:
                    messages.add_message(request, messages.WARNING, "object " + str(
                        a0) + " discarded from listing because of invalid reference: " + str(e))
                except:
                    messages.add_message(request, messages.WARNING,
                                         "object discarded from listing because of invalid reference: " + str(
                                             e))
            except Exception, e:
                logging.warning("object discarded from listing because of invalid reference:" + str(e))
        # raise
        return False
    return True


def glist_view(DocClass, base_elem_view=lambda x: x, json_view=lambda x: x, template=None,
               default_perpage=20, decorators=[], name=None, search_enabled=None,
               sort_enabled=None, permissions=None, view_mode="std", base_html_transform=None, **kwargs):
    """
    The list view must be used to retrieve any enumeration of objects.
    It can return in JSON, BSON, and html.
    """
    bsort_enabled = sort_enabled
    Meta = None
    if hasattr(DocClass, "WIDEIO_Meta"):
        Meta = DocClass.WIDEIO_Meta
        if hasattr(Meta, "search_enabled"):
            search_enabled = Meta.search_enabled
        if hasattr(Meta, "sort_enabled"):
            bsort_enabled = Meta.sort_enabled

    def lv(request, perpage=default_perpage, queryset=None, list_url=None,
           html_transform=base_html_transform, elem_view=base_elem_view, _RAW=0, **ykwargs):

        sort_enabled = bsort_enabled
        if html_transform is None:
            html_transform = lambda x: x
        if elem_view is None:
            elem_view = lambda x: x
        if (not kwargs.get("skip_can_list", False)) and hasattr(DocClass, "can_list") and not DocClass.can_list(
                request):
            raise PermissionException()
        elif permissions is not None:
            if not permissions["list"](request):
                raise PermissionException()
        cpage = 0
        include_drafts = int(request.REQUEST.get("_INCLUDE_DRAFTS", "0"))
        wholepage = 1
        is_infinite_view = 1 if ("_INFINITE" in request.REQUEST and request.REQUEST[
            "_INFINITE"]) else 0  # < SHALL BE DEPRECATED USE TEMPLATE
        hide_navbar = 1 if ("_HIDE_NAVBAR" in request.REQUEST and request.REQUEST[
            "_HIDE_NAVBAR"]) else 0
        hide_hits = request.REQUEST.get("_HIDE_NAVBAR", 0)
        if request.REQUEST.get('_MINIMALIST', False):
            hide_navbar = 1
            hide_hits = 1

        ###
        ### GET CURRENT QUERY
        ###            
        cpage = int(
            request.REQUEST.get(
                "_PAGE",
                request.REQUEST.get(
                    "PAGE",
                    1))) - 1
        if cpage < 0:
            cpage = 0

        if "_PERPAGE" in request.REQUEST:
            perpage = int(request.REQUEST["_PERPAGE"])
            if perpage < 1 or perpage > 16384:
                perpage = default_perpage
        if queryset is None:
            queryset = DocClass.objects  # .orderby()

        search_term = unicode(request.REQUEST["_search"]) if ("_search" in request.REQUEST) else None

        filters = dict()
        allow_query = None

        ###       
        ### UPDATE THE QUERY SET BASED ON THE INPUTS
        ###                    

        if Meta:
            if hasattr(Meta, 'allow_query'):
                allow_query = getattr(Meta, 'allow_query')
                for k in allow_query:
                    if k in request.REQUEST:
                        if request.REQUEST[k] == 'bool_false':
                            filters[k] = False
                        elif request.REQUEST[k] == 'bool_true':
                            filters[k] = True
                        else:
                            filters[k] = request.REQUEST[k]
            if hasattr(Meta, 'default_query'):
                dq = Meta.default_query
                override = getattr(Meta, 'allow_default_query_override', False)
                if search_enabled and search_term and override:
                    pass
                else:
                    if callable(dq):
                        dq = dq(request)
                    if isinstance(dq, dict):
                        filters.update(**dq)
                    else:
                        # UPDATE DIRECTLY BECAUSE MIGHT BE DJANGO Q OBJECT (not
                        # iterable)
                        try:
                            queryset = queryset.filter(dq)
                        except Exception as e:
                            print e
            # ALLOW SEARCH
            # if request.REQUEST.has_key("_exclude_drafts") and
            # request.REQUEST['_exclude_drafts'] != '':
            if (include_drafts == 0) or (not request.user.is_authenticated()):
                if not hasattr(Meta, "NO_DRAFT"):
                    queryset = queryset.filter(wiostate='V')
                    # else:
                    # if
                    # queryset = queryset.filter(reduce( lambda ,Q(wiostate='V')))
            else:
                if (not request.user.is_staff) and (hasattr(Meta, "OWNER_FIELD")):
                    if not hasattr(Meta, "NO_DRAFT"):
                        from django.db.models import Q
                        queryset = queryset.filter(Q(wiostate='V') | Q(
                            wiostate__startswith='U', **{Meta.OWNER_FIELD: request.user}))

        # < DEBUG THE NEXT LINE TO SOLVE THE LIFTFIELDS ISSUE
        # print "LST",list(queryset.iterator())
        # print "OBJECT=", queryset.all()
        # print "CNT=", queryset.count()



        # messages.add_message(request, messages.INFO, 'draft mode:'+str(include_drafts))
        if search_enabled is not None:
            basic_search = False
            oq = None
            if search_term:
                basic_search = True
                if (re.match("([A-Za-z0-9_]+):(.+)", search_term)):
                    basic_search = False
                    try:
                        oq = wjson.loads("{" + search_term + "}")
                    except Exception as e:
                        print e
                        pass
                else:
                    s_val = str(search_term)
                    if type(search_enabled) not in (list, tuple):
                        s_key = str(search_enabled) + "__icontains"
                        filters[s_key] = s_val
                    else:
                        lq = [
                            queryset.filter(**{sv + "__icontains": s_val}) for sv in search_enabled]
                        # queryset = itertools.chain(*lq)
                        queryset = reduce(lambda x, y: x | y, lq[1:], lq[0])
            if not basic_search:
                if type(search_enabled) not in (list, tuple):
                    se = [search_enabled]
                else:
                    se = search_enabled
                for k in request.REQUEST.keys():
                    if k.replace("__", "&").split("&")[0] in se:
                        filters[k] = request.REQUEST[k]
                if oq is not None:
                    for k in oq.keys():
                        if k.replace("__", "&").split("&")[0] in se:
                            filters[k] = oq[k]

        # APPLY ALL THE FILTERS
        # sort_enabled=False
        try:
            queryset = queryset.filter(**filters)
            if hasattr(DocClass, "list_queryset_filter") and (
                        (not request.user.is_superuser) or ("_ADMIN_SEE_ALL" not in request.REQUEST)):
                queryset = DocClass.list_queryset_filter(queryset, request)
            # sort0=sort[0]
            sortby = DocClass._meta.fields[1].name
            sort_default = {'created_at': 1}
            if Meta:
                if hasattr(Meta, 'sort_default'):
                    sort_default = getattr(Meta, 'sort_default')
            # else:
            #    sort_enabled=False
            if sort_enabled:
                if "_sortby" in request.REQUEST and len(
                        request.REQUEST["_sortby"]):
                    sortby = request.REQUEST.get(
                        "_sortby",
                        getattr(Meta, 'sortby_default')
                        if hasattr(Meta, 'sortby_default')
                        else {}
                    )
                for s in sort_enabled:
                    sort_default[s] = 0
                order_by = [str(s)
                            for s in sortby.strip(',').split(',') if s != '']
                if len(order_by) > 0:
                    queryset = queryset.order_by(*order_by)
                    for s in order_by:
                        if s[0] == '-':
                            sort_default[s[1:]] = 1
                        else:
                            sort_default[s] = 0

            ### PAGINATE AND VALIDATE THE QUERYSET

            if _RAW:
                return queryset

            try:
                hits = queryset.count()
            except DatabaseError:
                hits = -1
            fromidx = cpage * perpage
            toidx = (cpage + 1) * perpage
            pagedqueryset = queryset.all()[fromidx:toidx]
            if pagedqueryset is None:
                pagedqueryset = queryset.all()
                if pagedqueryset is None:
                    pagedqueryset = queryset
                    if pagedqueryset is None:
                        pagedqueryset = []
            pagedqueryset = list(pagedqueryset)

            if getattr(settings, "WIDEIO_REVALIDATE_REFS", False) and not int(request.GET.get("_WITH_BROKEN_REFS", "0")):
                pagedqueryset = [keep_if_ok(e, request) for e in pagedqueryset]

        except Exception as e:
            logging.exception("Error while generating results")
            raise

        xkwargs = dict(kwargs)
        xkwargs.update(ykwargs)
        xkwargs.update({
            'sort_enabled': sort_enabled,
            'search_enabled': search_enabled,
            'sort_default': sort_default,
            'is_infinite_view': is_infinite_view,
            'hide_navbar': hide_navbar,
            'view_mode': view_mode,
            'hide_hits': hide_hits})
        if "_VIEW_OPTIONS" in request.GET:
            xkwargs.update(wjson.loads(request.GET["_VIEW_OPTIONS"]))
            xkwargs['view_options'] = request.GET["_VIEW_OPTIONS"]
        if "elem_view" not in xkwargs:
            xkwargs["elem_view"] = elem_view
        return render_gen_list(DocClass, request, pagedqueryset, permissions, hits, fromidx, toidx, sortby,
                               perpage=perpage,
                               html_transform=html_transform, list_url=list_url, wholepage=wholepage, template=template,
                               json_view=json_view, filters=filters, **xkwargs)

    decorators = [permission_based] + decorators
    lv = wideio_view()(lv)
    if len(decorators) > 0:
        for dec in decorators:
            lv = dec(lv)
        nlv = lv
    else:
        nlv = lv

    if name is None:
        name = DocClass.__name__
    nlv.base_name = name
    nlv.view_name = "list"
    nlv.default_url = "%s/list" % (name,)
    nlv.rest_url = "%s/" % (name,)
    nlv.rest_method = "GET"
    nlv.wadl = [(nlv.base_name + "/" + nlv.view_name, {
        "url": "%s/list/" % (name,),
        "method": "GET",
        "arguments": []
    })]
    return nlv


def grel_list_view(
        DocClass, field, decorators=[], name=None, permissions=None, **kwargs):
    # print "grel_list_view called"
    if (isinstance(field, str)):
        fieldname = field.lower()
    else:
        fieldname = field.name.lower()
    M2M = False
    if (hasattr(getattr(DocClass, fieldname), "through")):
        through = getattr(DocClass, fieldname).through
        M2M = True
    else:
        if (hasattr(getattr(getattr(DocClass, fieldname).related.field.model, getattr(
                DocClass, fieldname).related.field.name), "through")):
            through = getattr(
                getattr(
                    DocClass, fieldname).related.field.model, getattr(
                    DocClass, fieldname).related.field.name).through
            M2M = True
    Meta = DocClass.WIDEIO_Meta
    if "add_enabled" not in kwargs:
        kwargs["add_enabled"] = (getattr(Meta, "add_enabled") if hasattr(Meta, "add_enabled") else False)
    if "view_mode" not in kwargs:
        kwargs["view_mode"] = "rel_" + fieldname
    kwargs["skip_can_list"] = True
    if M2M:
        base_lv = glist_view(
            through.objects.model,
            **kwargs)
    else:
        base_lv = glist_view(
            getattr(
                DocClass,
                fieldname).related.model,
            **kwargs)

        # base_lv=glist_view( getattr(DocClass,field.name).through.objects,**kwargs)

    def lv(request, oid=None, **xkwargs):
        if "oid" in request.REQUEST:
            oid = request.REQUEST["oid"]
        else:
            if oid is None:
                oid = filter(
                    lambda x: len(x) != 0, request.path.split("/"))[-1]
        cbase_lv = base_lv

        CM2M = M2M

        if int(request.GET.get("_NO_M2M", "1")):
            CM2M = False
            try:
                x = DocClass._meta.get_field_by_name(fieldname)[0]
                if hasattr(x, "rel"):
                    cmodel = x.rel.to  # or DocClass
                else:
                    cmodel = x.model
                # if cmodel==None:
                #  cmodel=DocClass
                cbase_lv = glist_view(
                    cmodel,
                    **kwargs)
            except:
                pass

        oid = str(oid)
        # -----------------------------------------------------
        if CM2M:
            if (hasattr(getattr(DocClass, fieldname), "field")):
                fi = getattr(DocClass, fieldname).field
                fname = str(fi.m2m_field_name())
                rname = str(fi.m2m_reverse_name())
                oid = solveoid(DocClass, oid).id
                queryset = through.objects.filter(**{fname + "_id": oid})
            else:
                oid = solveoid(DocClass, oid)
                queryset = through.objects.filter(
                    **{str(getattr(DocClass, fieldname).related.field.m2m_field_name()): oid})
        else:
            cobject = solveoid(DocClass, oid)
            queryset = getattr(cobject, fieldname).all()

        #from django.core.urlresolvers import reverse
        #list_url = reverse(DocClass.__name__ +"_rel_list_" +fieldname, args=[oid]) #< remove reverse !
        list_url = DocClass.get_list_url()

        try:
            return cbase_lv(request, queryset=queryset, list_url=list_url, **xkwargs)
        except NameError as n:
            return HttpResponse(
                escape("Error: There are no %r %r %r to display" % (n, type(n), queryset)))
        except Exception as e:
            if request.user.is_staff:
                return HttpResponse(
                    "/ Error in view : %r %r /" % (e, traceback.format_tb(sys.exc_info()[2])))
            else:
                return HttpResponse("/ Error in rel_list view ")

    decorators = [permission_based] + decorators
    if len(decorators) > 0:
        for dec in decorators:
            lv = dec(lv)

        def nlv(*args, **xargs):
            return lv(*args, **xargs)
    else:
        nlv = lv
    if name is None:
        name = DocClass.__name__
    nlv.base_name = name
    nlv.view_name = "rel_list_" + fieldname
    nlv.default_url = "%s/rel_list/%s/([^/]+)" % (name, fieldname)
    nlv.rest_url = "%s/([^/]+)/%s" % (name, fieldname)
    nlv.rest_method = "GET"

    nlv.wadl = [(nlv.base_name + "/" + nlv.view_name, {
        "url": "%s/rel_list/%s/-a-p-i-" % (name, fieldname),
        "method": "GET",
        "arguments": []
    }
                 )]

    return nlv
