# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################

"""
AMODEL : ADVANCED MODELS

This contains the decorator for components the components of WIDE IO
"""

import logging
from django.http import Http404
from django.http import HttpResponse
from wioframework.decorators import PermissionException


def default_action_view(f, get_fcls=lambda: None, possible=lambda o, r: True):
    from wioframework import decorators

    def view(request, oid):
        cls = get_fcls.meta["model"]
        o = cls.objects.filter(id=oid)
        if not len(o):
            raise Http404
        o = o[0]
        if not (possible(o, request)):
            raise PermissionException()

        def av(req):
            r = f(o, req)
            if type(r) == dict:
                return r
            if type(r) in [str, unicode]:
                return HttpResponse(r, get_fcls.meta["mimetype"])
            return r

        import wioframework.utilviews
        return wioframework.utilviews.wideio_view(keep_get_tags=True)(lambda r: av(r))(request)

    return decorators.permission_based(view)


def wideio_action(tname=None, possible=lambda o, r: True, level=1, anonymous_possible=False, thref=None, **meta):
    def dec(of):
        from backoffice import lib as debug
        name = tname
        cmeta = dict(meta.items())
        rname = of.__name__

        if rname == "<lambda>":
            rname = tname

        if name is None:
            name = of.__name__

        if "mimetype" not in cmeta:
            cmeta["mimetype"] = "application/javascript"

        def f(*args, **xargs):
            if len(args) > 1 and hasattr(args[1], "user"):
                request = args[1]
                debug.log_info("executing action %s" % (name,), request.user)
            else:
                debug.log_info("executing action %s" % (name,))
            return of(*args, **xargs)

        if thref != None:
            f.thref = thref
        f.act_name = name
        f.possible = lambda x, r: (((not r.user.is_anonymous()) or anonymous_possible) and possible(x, r))
        f.level = level
        f.meta = cmeta
        f.view = default_action_view(f, get_fcls=f, possible=possible)

        def register_into_class(tc):
            if not (hasattr(tc.WIDEIO_Meta.Actions, "_act_list")):
                setattr(tc.WIDEIO_Meta.Actions, "_act_list", [])
            if rname not in tc.WIDEIO_Meta.Actions._act_list:
                tc.WIDEIO_Meta.Actions._act_list.append(rname)

        def contribute_to_class(tc):
            if isinstance(tc, str):
                from django.apps import registry
                tc = registry.apps.get_model(tc)
                #tc=eval(tc)  # < FIXME: SOMETHING MORE EVOLVED WOULD BE BETER

            if not hasattr(tc, "WIDEIO_Meta"):
                class WIDEIO_Meta:
                    pass
                setattr(tc, "WIDEIO_Meta", WIDEIO_Meta)

            if not hasattr(tc.WIDEIO_Meta, "Actions"):
                class Actions(object):
                    pass
                setattr(tc.WIDEIO_Meta, "Actions", Actions)

            setattr(tc.WIDEIO_Meta.Actions, rname, f)
            register_into_class(tc)

        f.contribute_to_class = contribute_to_class
        f.register_into_class = register_into_class
        f.of = of
        return f

    return dec


def wideio_relaction(tclass, *args, **kwargs):
    """
    A related action - an action that is linked/inserted in another class.

    @param tclass: The class in which we inject the action (it can be a string if the action is part of a model)
    @param args: argument of the action
    @param kwargs: keyword based arguments for the action
    @return:
    """
    def dec(of):
        if hasattr(of, "on_load_postprocess"):
            pf = of.on_load_postprocess  # < FIXME: MAYBE NOT EXACTLY THERE
        else:
            pf = None

        def rnf(*args,**kwargs):
            act = wideio_action(*args, **kwargs)(of)
            act.contribute_to_class(tclass)
            if pf is not None:
                pf()
            return None

        if isinstance(tclass, str):
            of.on_load_postprocess = staticmethod(rnf)
        else:
            rnf(*args,**kwargs)
        return of

    return dec


def wideio_inject_extension(tclass):
    def dec(oc):
        if hasattr(oc, "_on_load_postprocess"):
            pf = oc._on_load_postprocess  # < FIXME: MAYBE NOT EXACTLY THERE
        else:
            pf = None

        def nf(self,*args,**kwargs):
            #print "oc 2 ! _on_load_postprocess", tclass
            tc = tclass
            if isinstance(tc, str):
                try:
                    from django.apps import registry
                    tc = registry.apps.get_model(tc)
                except:
                    if len(args):
                        m=args[0]
                        for x in m:
                            print "oc x ?",tc,x.__module__,x.__name__
                            if tc.split(".")[-1]==x.__name__:
                                tc=x
                                break
                    else:
                        print "arguments",args,kwargs
                            
            assert not isinstance(tc, str)
                
            for m in dir(oc):
                if m[0] != '_' or m[-1] != '_':
                    #print "oc 3  " ,tc,m
                    try:
                        setattr(tc, m, getattr(oc, m))
                    except:
                        logging.exception("could not set subattribute")
            if pf is not None:
                pf(self,*args,**kwargs)
            return None

        if isinstance(tclass, str):
            oc._on_load_postprocess = staticmethod(nf)
            #print "oc",oc, "_on_load_postprocess <-"
        else:
            nf()
        return oc

    return dec