# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################

"""
AMODEL : ADVANCED MODELS

This contains the decorator for components the components of WIDE IO
"""


import django.db.models as omodels
from django.db.models.fields.related import RelatedField

from django.http import Http404
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.utils.html import escape

from django.core.exceptions import ValidationError

from django.conf import settings

import wioframework.fields as models

from wioframework.atomic_ops import atomic_incr
from wioframework import decorators as dec
from wioframework.decorators import PermissionException
from wioframework.decorators import permission_based
import wioframework.fields as lib_fields

import wioframework.utils as utils
import markdown


from actions import wideio_action, wideio_relaction

def wideio_admin_owned():
    def dec(tc):
        return tc
    return dec


def wideio_owned(field_name="owner", max_instances=-1,
                 public=True, personal=0, is_public_field=None, *xargs, **xxargs):
                     
    def owned_default_can_write(obj, request):
        return (request.user.is_superuser) or (
            request.user.id == getattr(obj, field_name + "_id"))

    def owned_default_can_view(obj, request):
        return (((personal==0) and ((public == True) or ((isinstance(public, str)) and getattr(obj, public)))) or (
            request.user.is_superuser) or (request.user.id == getattr(obj, field_name + "_id")))

    def owned_default_can_add(request, ti, tcn):
        # by default you can create a new instance
        if (max_instances == -1):
            return True
        # except is the maximum number of instance for that user has already
        # been reached
        om = getattr(request.user, tcn)
        return (om.all().count() < max_instances)

    def default_on_add(obj, request):
        if request!=None:
          setattr(obj, field_name + "_id", request.user.id)
          obj.save()


    def make_gdefault_query(is_public_field):
        @staticmethod
        def gdefault_query(request):
            from django.db.models import Q
            if request.user.is_staff:
                return Q(wiostate='V')
            else:
                if public==True:
                   return Q(wiostate='V')
                else: 
                  if is_public_field:
                    return Q(**{is_public_field:True}) | Q(**{field_name:request.user})                      
                  else:
                    return Q(**{field_name:request.user})
        return gdefault_query
                                                                          
                                                                          
        
        
    def dec(tc):
        # setattr(tc,field_name,models.ForeignKey("UserAccount",*xargs,**xxargs))
        # print "OM"
        #import settings
        if (not hasattr(tc, field_name)):
                # print "***" ,field_name
                #from science.models import UserAccount
                # print  tc.__name__.lower()+"_rev_"+field_name
                # try:
                #  #from accounts.models import UserAccount
                #  field=models.ForeignKey(UserAccount, editable=False, null=True, related_name=tc.__name__.lower()+"_rev_"+field_name, *xargs, **xxargs)
                # except ImportError:
            field = models.ForeignKey(
                'accounts.UserAccount',
                editable=False,
                null=True,
                related_name=tc.__name__.lower() +
                "_rev_" +
                field_name,
                db_index=True,
                *
                xargs,
                **xxargs)
            #field=models.ForeignKey("UserAccount",related_name=tc.__name__.lower()+"_rev_"+field_name, *xargs,**xxargs)
            # print tc.__name__.lower()+"_rev_"+field_name
            field.contribute_to_class(tc, field_name)
            
            c_is_public_field=is_public_field
            if c_is_public_field==None:
                if hasattr(tc,"is_public"):
                    c_is_public_field="is_public"
            
            #~ if (hasattr(tc,"CanWrite")):
            #~ otc=getattr(tc,"CanWrite")
            #~ setattr(tc,"CanWrite",lambda o,r:(otc(o,r) and owned_default_can_write(o,r)))
            #~ else:
            #~ setattr(tc,"CanWrite",staticmethod(owned_default_can_write))

            def transfer(self,request):
                from network.models import GiveAction
                return {"_redirect":GiveAction.get_add_url()+"?from_user="+request.user.id+"&object_pk="+self.id+"&content_type="+str(self.get_content_type_id())}
            
            if getattr(tc.WIDEIO_Meta,"CAN_TRANSFER",True):
              wideio_action(
                possible=lambda o, r: (r.user.is_superuser or (getattr(o,field_name)==r.user)),icon="ion-paper-airplane",mimetype="text/html",xattrs=" data-modal-link ")(transfer).contribute_to_class(tc)
            
            
            if (hasattr(tc, "can_add")):
                otc4 = getattr(tc, "can_add")

                def can_add_cumul(r):
                    #import inspect
                    #raise Exception, (inspect.getfile(otc4),otc4.func_code)
                    if not otc4(r):
                        return False
                    return owned_default_can_add(
                        r, tc, tc.__name__.lower() + "_rev_" + field_name)
                tc.can_add = staticmethod(can_add_cumul)
            else:
                tc.can_add = staticmethod(
                    lambda r: (
                        owned_default_can_add(
                            r,
                            tc,
                            tc.__name__.lower() +
                            "_rev_" +
                            field_name)))

            if (public):
                if (hasattr(tc, "can_view")):
                    otc0 = getattr(tc, "can_view")
                    setattr(
                        tc, "can_view", lambda o, r: (
                            otc0(
                                o, r) and owned_default_can_view(
                                o, r)))
                else:
                    setattr(tc, "can_view", (owned_default_can_view))

            if (hasattr(tc, "can_update")):
                otc1 = getattr(tc, "can_update")
                setattr(
                    tc, "can_update", lambda o, r: (
                        otc1(
                            o, r) and owned_default_can_write(
                            o, r)))
            else:
                setattr(tc, "can_update", (owned_default_can_write))

            if (hasattr(tc, "can_delete")):
                otc2 = getattr(tc, "can_delete")
                setattr(
                    tc, "can_delete", lambda o, r: (
                        otc2(
                            o, r) and owned_default_can_write(
                            o, r)))
            else:
                setattr(tc, "can_delete", (owned_default_can_write))

            if (hasattr(tc, "can_add")):
                otc55 = getattr(tc, "can_add")
                setattr(
                    tc,
                    "can_add",
                    staticmethod(
                        lambda r: (
                            otc55(r) and r.user.is_authenticated())))
            else:
                setattr(
                    tc,
                    "can_add",
                    staticmethod(
                        lambda r: r.user.is_authenticated()))

            if (hasattr(tc, "on_add")):
                otc3 = getattr(tc, "on_add")

                def Eon_add(self, o):
                    r = otc3(self, o)
                    default_on_add(self, o)
                    return r
                setattr(tc, "on_add", Eon_add)
            else:
                setattr(tc, "on_add", default_on_add)

        if (not hasattr(tc, "WIDEIO_Meta")):
            class WIDEIO_Meta(object):
                form_exclude = [field_name]
                default_query = make_gdefault_query(c_is_public_field)
            tc.WIDEIO_Meta = WIDEIO_Meta
        else:
            if not hasattr(tc.WIDEIO_Meta, "form_exclude"):
              tc.WIDEIO_Meta.form_exclude = [field_name]
            else:
              tc.WIDEIO_Meta.form_exclude.append(field_name)
            if not hasattr(tc.WIDEIO_Meta, "default_query"):              
              tc.WIDEIO_Meta.default_query = make_gdefault_query(c_is_public_field)
              
        if (not hasattr(tc.WIDEIO_Meta, "sort_enabled")):
            tc.WIDEIO_Meta.sort_enabled = []
        tc.WIDEIO_Meta.sort_enabled += [field_name]
        tc.WIDEIO_Meta.OWNER_FIELD = field_name
        tc.WIDEIO_Meta.PERSONAL = personal
        if personal>1:
            if not hasattr(tc,"Meta"):
              class _Meta(object):
                pass
              tc.Meta=_Meta
            tc.Meta.db="personal_data"
            if personal>2:
                tc.WIDEIO_Meta.ENCRYPT_FIELDS=True
        return tc
    return dec



def wideio_multi_owned(owner_field_name="admins", member_field_name="admins", max_instances=-1,
                 public=True, personal=0, *xargs, **xxargs):
    def owned_default_can_write(obj, request):
        return (request.user.is_superuser) or (
            request.user.id in getattr(obj, owner_field_name ).all())

    def owned_default_can_view(obj, request):
        return (((personal==0) and ((public == True) or ((isinstance(public, str)) and getattr(obj, public)))) or (
            request.user.is_superuser) or (request.user.id in getattr(obj, owner_field_name).all()))

    def owned_default_can_add(request, ti, tcn):
        if (max_instances == -1):
            return True
        om = getattr(request.user, tcn)
        return (om.all().count() < max_instances)

    def default_on_add(obj, request):
        getattr(obj, owner_field_name).add(request.user.id)
        getattr(obj, member_field_name).add(request.user.id)
        
    def dec(tc):
        if (not hasattr(tc, field_name)):
            owner_field = models.ManyToManyField(
                'accounts.UserAccount',
                related_name=tc.__name__.lower() +
                "_rev_" +
                owner_field_name,
                *
                xargs,
                **xxargs)
            owner_field.contribute_to_class(tc, owner_field_name)
            member_field = models.ManyToManyField(
                'accounts.UserAccount',
                related_name=tc.__name__.lower() +
                "_rev_" +
                member_field_name,
                *
                xargs,
                **xxargs)
            member_field.contribute_to_class(tc, member_field_name)

            if (hasattr(tc, "can_add")):
                otc4 = getattr(tc, "can_add")

                def can_add_cumul(r):
                    #import inspect
                    #raise Exception, (inspect.getfile(otc4),otc4.func_code)
                    if not otc4(r):
                        return False
                    return owned_default_can_add(
                        r, tc, tc.__name__.lower() + "_rev_" + field_name)
                tc.can_add = staticmethod(can_add_cumul)
            else:
                tc.can_add = staticmethod(
                    lambda r: (
                        owned_default_can_add(
                            r,
                            tc,
                            tc.__name__.lower() +
                            "_rev_" +
                            owner_field_name)))

            if (public):
                if (hasattr(tc, "can_view")):
                    otc0 = getattr(tc, "can_view")
                    setattr(
                        tc, "can_view", lambda o, r: (
                            otc0(
                                o, r) and owned_default_can_view(
                                o, r)))
                else:
                    setattr(tc, "can_view", (owned_default_can_view))

            if (hasattr(tc, "can_update")):
                otc1 = getattr(tc, "can_update")
                setattr(
                    tc, "can_update", lambda o, r: (
                        otc1(
                            o, r) and owned_default_can_write(
                            o, r)))
            else:
                setattr(tc, "can_update", (owned_default_can_write))

            if (hasattr(tc, "can_delete")):
                otc2 = getattr(tc, "can_delete")
                setattr(
                    tc, "can_delete", lambda o, r: (
                        otc2(
                            o, r) and owned_default_can_write(
                            o, r)))
            else:
                setattr(tc, "can_delete", (owned_default_can_write))

            if (hasattr(tc, "can_add")):
                otc55 = getattr(tc, "can_add")
                setattr(
                    tc,
                    "can_add",
                    staticmethod(
                        lambda r: (
                            otc55(r) and r.user.is_authenticated())))
            else:
                setattr(
                    tc,
                    "can_add",
                    staticmethod(
                        lambda r: r.user.is_authenticated()))

            if (hasattr(tc, "on_add")):
                otc3 = getattr(tc, "on_add")

                def Eon_add(self, o):
                    r = otc3(self, o)
                    default_on_add(self, o)
                    return r
                setattr(tc, "on_add", Eon_add)
            else:
                setattr(tc, "on_add", default_on_add)
                
            if (hasattr(tc, "on_preadd")):
                otc3bp = getattr(tc, "on_preadd")

                def Eon_preadd(self, o):
                    r = otc3bp(self, o)
                    default_on_add(self, o)
                    return r
                setattr(tc, "on_preadd", Eon_preadd)
            else:
                setattr(tc, "on_preadd", default_on_add)                

        if (not hasattr(tc, "WIDEIO_Meta")):
            class WIDEIO_Meta(object):
                form_exclude = [field_name]
            tc.WIDEIO_Meta = WIDEIO_Meta
        elif not hasattr(tc.WIDEIO_Meta, "form_exclude"):
            tc.WIDEIO_Meta.form_exclude = [owner_field_name,member_field_name]
        else:
            tc.WIDEIO_Meta.form_exclude.append(field_name)
        if (not hasattr(tc.WIDEIO_Meta, "sort_enabled")):
            tc.WIDEIO_Meta.sort_enabled = []
        tc.WIDEIO_Meta.sort_enabled += [field_name]
        tc.WIDEIO_Meta.OWNER_FIELD = field_name
        tc.WIDEIO_Meta.PERSONAL = personal

        
        # CREATE OWNERSHIP TRANSFER
        #wideio_action(
        #        possible=lambda o, r: (
        #            o.is_owner(
        #                r.user)))(do_give).contribute_to_class(tc)                                
        
        if personal>1:
          raise Exception, "Multply owned elements can not be considered as confidential"

        return tc
    return dec




