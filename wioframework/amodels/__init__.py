# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################

"""
AMODEL : ADVANCED MODELS

This contains the decorator for components the components of WIDE IO
"""
import os
import re
import sys
import datetime
import traceback
import hashlib
import uuid
# from django.db import models
import django.db.models as omodels
from django.db.models.fields.related import RelatedField
from django.http import Http404
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.utils.html import escape
#
from django.core.exceptions import ValidationError
from django.conf import settings
# from django.contrib.auth.models import  AbstractUser

import wioframework.fields as models
from wioframework.atomic_ops import atomic_incr
from wioframework import decorators as dec
from wioframework.decorators import PermissionException
from wioframework.decorators import permission_based
import wioframework.fields as lib_fields
from mml.transductor import Transductor
import wioframework.utils as utils
import markdown


# from wioframework.mongo import mongo_db, ObjectId


def name_to_ctr_s(n):
    return re.sub(
        "([a-z])([A-Z])", lambda x: x.group(1) + " " + x.group(2), n).lower()


def name_to_ctr_p(n):
    return re.sub(
        "([a-z])([A-Z])", lambda x: x.group(1) + " " + x.group(2), n).lower() + "s"


REV_APP = {
    'accounts': 'accounts',
    'network': 'network',
    'extfields': 'ext',
}


def with_advanced_help_text(ht):
    def dec(field):
        if field is None:
            return None
        field.advanced_help_text = ht
        return field

    return dec


def with_app_validate(ht):
    def dec(field):
        if field is None:
            return None
        field.app_validate = ht
        return field

    return dec


def with_db_validate(ht):
    def dec(field):
        if field is None:
            return None
        field.db_validate = ht
        return field

    return dec


class Model(omodels.Model):
    """
    PSEUDO CLASS THAT WILL BE USED TO EXTEND CLASSICAL DJANGO MODELS
    """

    @staticmethod
    def get_base_url(cls):
        """
        All urls to access the views related to that model  are under this.
        """
        mn = cls.__module__.split('.')
        if mn[0] == "aiosciweb1":
            mn = mn[1:]
        mn = mn[0].lower()
        cn = cls.__name__.lower()
        return "/" + REV_APP.get(mn, mn) + "/" + cn
        # return reverse(cls.__name__+"_list")[:-5]

    @staticmethod
    def clean_fields(self, exclude=None):
        """
        Cleans all fields and raises a ValidationError containing message_dict
        of all validation errors if any occur.
        """
        if exclude is None:
            exclude = []
            if hasattr(self, "WIDEIO_Meta"):
                if hasattr(self.WIDEIO_Meta, "form_exclude"):
                    exclude = self.WIDEIO_Meta.form_exclude

        self.form_app_valid = True
        self.form_db_valid = True
        self.form_app_errors = {}
        self.form_app_validated = {}
        self.form_db_errors = {}

        errors = {}
        for f in self._meta.fields:
            print f.name, "(" + f.attname + ")"
            if f.name in exclude:
                # self.form_app_validated[f.name] = True
                continue
            # Skip validation for empty fields with blank=True. The developer
            # is responsible for making sure they have a valid value.

            try:
                raw_value = getattr(self, f.attname)
            except:
                raw_value = getattr(self, f.name)
            # print raw_value
            # if (f.soft_p['blank'] or f.app_p['blank']) and raw_value in
            # validators.EMPTY_VALUES:
            if f.blank and (type(raw_value) in [str, unicode]) and len(
                    raw_value) == 0:  # raw_value in app_validators.EMPTY_VALUES:
                self.form_app_validated[f.name] = True
                continue
            if isinstance(f, RelatedField):
                if hasattr(self, f.attname + "_id"):
                    raw_key = getattr(self, f.attname + "_id")
                    if f.null and raw_key is None:
                        self.form_app_validated[f.name] = True
                        continue
                else:
                    raw_key = getattr(self, f.attname)
                    if f.null and raw_key is None:
                        self.form_app_validated[f.name] = True
                        continue
            try:
                try:
                    if hasattr(f, "app_validate"):
                        f.app_validate(raw_value, self)
                    else:
                        f.validate(raw_value, self)
                    self.form_app_validated[f.name] = True
                except ValidationError as e:
                    self.form_app_valid = False
                    self.form_app_validated[f.name] = False
                    self.form_app_errors[f.name] = e.messages
                if (hasattr(f, "db_validate")):
                    f.db_validate(raw_value, self)
                setattr(self, f.attname, f.clean(raw_value, self))
            except ValidationError as e:
                self.form_db_valid = False
                self.form_db_errors[f.name] = e.messages

        if not self.form_db_valid:
            raise ValidationError(self.form_db_errors)

    @staticmethod
    def get_model_name(cls):
        return cls.__name__

    @staticmethod
    def as_tdh(self):
        r = ""
        for f in self._meta.fields:
            r += "<th data-options=\"width:100\">%s</th>" % (f.name,)
            # r+="<th data-options=\"width:100\">%s</th>"%(f.name,)
        return r

    @staticmethod
    def as_td(self):
        r = ""
        for f in self._meta.fields:
            r += "<td>%s</td>" % (getattr(self, f.name),)
        return r

    @staticmethod
    def as_extjson_tdh(self):
        r = ""
        for f in self._meta.fields:
            r += "<th data-options=\"field:'%s',width:100\">%s</th>" % (f.name, f.name)
        return r

    @staticmethod
    def as_extjson(self):
        r = []
        for f in self._meta.fields:
            r += "%s:%s" % (f.name, json.dumps(getattr(self, f.name)),)
        return "{%s}" % (",".join(r))

    @staticmethod
    def archive(self):
        self.wiostate = 'A'
        self.save()

    @staticmethod
    def unarchive(self):
        self.wiostate = 'V'
        self.save()

    @staticmethod
    def html(self, request, mode="std", context=None, capture_errors=True):
        """
        This looks for a standard way to display an element of this type. So we look for a template that may be available.

        DARWIN: See if we can uniformise this template system with modern JAVASCRIPT client-side framework such as angular
        """
        from django.shortcuts import RequestContext
        from django.template.loader import render_to_string
        prefix = ""
        #        import settings
        if len(sys._current_frames()) > 10:
            if capture_errors:
                return "[TOO DEEP IN STACK]"
            else:
                raise Exception("[TOO DEEP IN STACK]")
        c = {'x': self, 'request': request, 'settings': settings, 'base': 'bases/ajax.html'}
        try:
            p = "models/" + \
                self.__class__.__name__.lower() + "/" + mode + ".html"
            if context != None:
                c.update(context)
            if (os.path.exists(os.path.join(settings.CWD, "templates", p))):
                return render_to_string(
                    p, c, context_instance=RequestContext(request))
            else:
                p = "models/" + mode + ".html"
                if (os.path.exists(
                        os.path.join(settings.CWD, "templates", p))):
                    return render_to_string(
                        p, c, context_instance=RequestContext(request))
                if (request.user.is_superuser):
                    prefix = "Could not find template! " + p + " "
        except Exception as e:
            if capture_errors:
                sys.stderr.write(str(e) + "\n")
            else:
                raise
        mode = "std"
        p = "models/" + self.__class__.__name__.lower() + "/" + mode + ".html"
        try:
            if (os.path.exists(os.path.join(settings.CWD, "templates", p))):
                return '<i class="icon-star %s wio-staff-only" title="%s"> </i>' % (
                (self.get_icon() if hasattr(self, "get_icon") else ""), prefix) + render_to_string(
                    p, c, context_instance=RequestContext(request))
            else:
                return '<i class="icon-star %s wio-staff-only" title="%s"> </i>' % (
                    (self.get_icon() if hasattr(self, "get_icon") else ""), prefix) + str(p)
        except Exception as e:
            if capture_errors:
                etxt = '<i class="icon-warning" title="Exceptional error while using HTML VIEW!">Error ' + str(
                    e) + '</i>'
                if request and (request.user.is_staff or request.user.is_superuser) and (
                            request.GET.get("_show_stacktrace") or not settings.PRODUCTION_MODE):
                    etxt += '<div class="wio-staff-only">' + str(e) + ":<ul><li>" + '</li><li>'.join(
                        map(escape, traceback.format_tb(sys.exc_info()[2]))) + "</li></ul></div>"
                return etxt
            else:
                raise

    @staticmethod
    def html_empty_result(cls, request, mode="std"):
        """
        This looks for a standard way to display an element of this type. So we look for a template that may be available.

        DARWIN: See if we can uniformise this template system with modern JAVASCRIPT client-side framework such as angular
        """
        from django.shortcuts import RequestContext
        from django.template.loader import render_to_string
        prefix = ""
        import settings
        p = "models/" + cls.__name__.lower() + "/empty_" + mode + ".html"
        if (os.path.exists(os.path.join(settings.CWD, "templates", p))):
            return render_to_string(
                p, {'x': cls, 'request': request, 'settings': settings}, context_instance=RequestContext(request))
        return None

    @staticmethod
    def _base_url(self):
        mn = self.__class__.__module__.split('.')
        if mn[0] == "aiosciweb1":
            mn = mn[1:]
        mn = mn[0].lower()
        cn = self.__class__.__name__.lower()
        return "/" + REV_APP.get(mn, mn) + "/" + cn

    @staticmethod
    def get_absolute_url(self):
        # return reverse(self.__class__.__name__ + "_view", args=[self.id])
        mn = self.__class__.__module__.split('.')
        if mn[0] == "aiosciweb1":
            mn = mn[1:]
        mn = mn[0].lower()
        cn = self.__class__.__name__.lower()
        return "/" + REV_APP.get(mn, mn) + "/" + \
               cn + "/view/" + str(self.id) + "/"

    @staticmethod
    def get_view_url_basic(self):
        # return reverse(self.__class__.__name__ + "_view", args=[self.id])
        mn = self.__class__.__module__.split('.')[0].lower()
        cn = self.__class__.__name__.lower()
        return "/" + REV_APP.get(mn, mn) + "/" + \
               cn + "/view/" + str(self.id) + "/"

    @staticmethod
    def get_view_url(self):
        if hasattr(self, "name"):
            if not hasattr(self.WIDEIO_Meta, "NAME_USE_DASH"):
                if (not hasattr(self, "wiostate") or self.wiostate == 'V') and self.__class__.objects.filter(
                        name=self.name).count() == 1 and self.name:
                    # return reverse(self.__class__.__name__ + "_view",
                    # args=[self.name.replace(' ','_')])
                    try:
                        self.name.encode('utf8').decode('ascii')
                        cn = self.__class__.__name__.lower()
                        t = self.name.replace(' ', '-')
                        return self._base_url() + "/view/" + str(t) + "/"
                    except:
                        pass

            else:
                if (not hasattr(self, "wiostate") or self.wiostate == 'V') and self.__class__.objects.filter(
                        name=self.name).count() == 1 and self.name:
                    # return reverse(self.__class__.__name__ + "_view",
                    # args=[self.name])
                    cn = self.__class__.__name__.lower()
                    t = self.name
                    return self._base_url() + "/view/" + str(t) + "/"
        cn = self.__class__.__name__.lower()
        return self._base_url() + "/view/" + str(self.id) + "/"

    @staticmethod
    def get_content_type_id(self):
        from django.contrib.contenttypes.models import ContentType
        try:
            return ContentType.objects.get(
                app_label=self._meta.app_label, model=self._meta.model_name).id
        except Exception, e:
            ct = ContentType()
            ct.app_label = self._meta.app_label
            ct.model = self._meta.model_name
            ct.save()
            return ct.id

    @staticmethod
    def get_rel_list_url(self, field_name):
        return reverse(
            self.__class__.__name__ + "_rel_list_" + field_name, args=[self.id])

    @staticmethod
    def get_del_url(self):
        return self._base_url() + "/delete/" + self.id + "/"

    @staticmethod
    def get_update_url(self):
        return self._base_url() + "/update/" + self.id + "/"

    @staticmethod
    def get_references_url(self):
        return self._base_url() + "/references/" + self.id + "/"

    @staticmethod
    def get_list_url(cls):
        return cls.get_base_url() + "/list/"

    @staticmethod
    def get_add_url(cls):
        return cls.get_base_url() + "/add/"

    @staticmethod
    def get_action_url(self, action_name):
        return self._base_url() + "/action/" + action_name +  "/" + self.id + "/"

    @staticmethod
    def get_state(self):
        # we shall allow saving even if state is not validated
        # Comprises three states: 0 (empty), 1 (incomplete), 2 (complete)
        return self._state

    @staticmethod
    def as_document(self):
        """
        Use mongo API directly to retrieve the fields rather than Django gaz factory
        TODO: Add support for caching
        """
        try:
            db_table = self._meta.db_table
            col = getattr(mongo_db, db_table)
            cid = ObjectId(self.id)
            return col.find({u'_id': cid})[0]
        except:
            # FIXME: add logging there.
            return self.__dict__


def wideio_get_icon(cls):
    return (cls.WIDEIO_Meta.icon if (
        hasattr(cls, "WIDEIO_Meta") and hasattr(cls.WIDEIO_Meta, "icon")) else None)


##
# SUGGESTIONS IT MAY BE A GOOD THING TO OVERRIDE SOME OF THE FIELD AND DJANGO BEHAVIORS
##


def wideio_formfield_callback0(f):
    """
    This call back is used to upgrade the forms that are displayed so that they use the correct display.
    """
    from django import forms
    from wioframework import widgets
    # if hasattr(f, 'model') and hasattr(f.model, 'allow_partial_submit') and f.model.allow_partial_submit():
    # sys.stderr.write("form cb %r -> %r\n"%( f, f.formfield() ))

    try:
        if isinstance(f, models.ForeignKey) and f.rel.get_related_field(
        ).model.__name__ == "Image":
            return forms.ModelChoiceField(f.rel.get_related_field().model.objects, widget=widgets.AImageWidget(
            ), required=(not (f.blank or f.null)), help_text=f.help_text)
        if isinstance(f, models.ForeignKey) and f.rel.get_related_field(
        ).model.__name__ == "File":
            return forms.ModelChoiceField(f.rel.get_related_field().model.objects, widget=widgets.AFileWidget(
            ), required=(not (f.blank or f.null)), help_text=f.help_text)
        # ADD FORM ON ALGORITHM DOES REQUIRE TO UPLOAD PACKAGE USING THE BRIDGE NOW
        # if isinstance(f, models.ForeignKey) and f.rel.get_related_field().model.__name__ == "Package":
        # return
        # forms.ModelChoiceField(f.rel.get_related_field().model.objects,widget=widgets.APackageWidget(),
        # required=(not (f.blank or f.null)), help_text=f.help_text)
        if isinstance(f, models.ForeignKey) and f.rel.get_related_field(
        ).model.__name__ == "Country":
            return forms.ModelChoiceField(f.rel.get_related_field().model.objects, widget=widgets.SelectWidget(
                model=f.rel.get_related_field().model), required=(not (f.blank or f.null)), help_text=f.help_text)
    except Exception as e:
        sys.stderr.write("#~#~# %r\n" % (e,))
        return None
    if (f.__class__.__name__ == "CountryField"):
        return forms.CharField(
            widget=widgets.WIOJQVMapWidget(placeholder=f.help_text, default=f.default, choices=f.choices),
            required=(not (f.blank or f.null)),
            help_text=f.help_text
            )
    if isinstance(f, models.URLField):
        if "video" in f.name:
            return forms.URLField(widget=widgets.VideoURLWidget(), required=(
                not (f.blank or f.null)), help_text=f.help_text)
        else:
            return forms.URLField(required=(not (
                f.blank or f.null)), help_text=f.help_text, widget=widgets.WIOCharFieldWidget(fieldtype="url"))

    if (f.__class__.__name__ == "CharField"):
        return forms.CharField(widget=widgets.WIOCharFieldWidget(
            maxlength=(f.app_p["max_length"] if hasattr(f, "app_p") else f.max_length),
            placeholder=f.help_text, default=f.default, choices=f.choices),
                               required=(not (f.blank or f.null)),
                               help_text=f.help_text
                               )
    if (f.__class__.__name__ == "TextField"):
        return forms.CharField(widget=widgets.WIOCharFieldWidget(
            maxlength=(f.app_p["max_length"] if hasattr(f, "app_p") else f.max_length),
            placeholder=f.help_text, default=f.default),
                               required=(not (f.blank or f.null)),
                               help_text=f.help_text
                               )
    if isinstance(f, models.IntegerField):
        if f.choices is not None:
            return f.formfield()
        else:
            return forms.IntegerField(widget=widgets.WIOCharFieldWidget(maxlength=f.max_length, default=(
                0 if f.default == django.db.models.fields.NOT_PROVIDED else f.default)),
                                      required=(not (f.blank or f.null)), help_text=f.help_text)

    if isinstance(f, models.DateField):
        return forms.DateField(widget=forms.widgets.DateInput(
            attrs={'class': 'date-input'}), required=(not (f.blank or f.null)), help_text=f.help_text)

    from wioframework.fields import JSONField
    if isinstance(f, JSONField):
        stdargs = {
            'required': not (f.blank or f.null),
            'help_text': f.help_text
        }
        return forms.CharField(
            widget=widgets.JSONEditorWidget(maxlength=(f.app_p["max_length"] if hasattr(f, "app_p") else f.max_length),
                                            placeholder=f.help_text, default=f.default),
            **stdargs
            )

    if isinstance(f, models.ManyToManyField) and (
                f.rel.get_related_field().model.__name__ == "Keyword"):
        return forms.ModelMultipleChoiceField(
            f.rel.get_related_field().model.objects,
            widget=widgets.KeywordWidget(
                model=f.rel.get_related_field().model),
            required=(not (f.blank or f.null)), help_text=f.help_text
        )

    if isinstance(f, models.ManyToManyField):  # and False:
        wim = f.model.WIDEIO_Meta
        required = not (f.blank or f.null)
        if hasattr(wim, "widgets") and (f.name in wim.widgets):
            widget = wim.widgets[f.name](f)
        else:
            widget = widgets.DynamicAutocompleteSelectWidget(
                model=f.rel.get_related_field().model)
        return forms.ModelMultipleChoiceField(
            f.rel.get_related_field().model.objects,
            widget=widget,
            required=required,
            help_text=f.help_text
        )
    if isinstance(f, models.ForeignKey):
        required = not (f.blank or f.null)
        return forms.ModelChoiceField(
            f.rel.get_related_field().model.objects,
            widget=widgets.DynamicSelectWidget(
                model=f.rel.get_related_field().model,
                default=f.default,
                required=required),
            required=required,
            help_text=f.help_text
        )
        # print "FIELD MODEL", f.model
        # print "FIELD MODEL CONTENTS", dir(f)
        # if f is not None and f.rel is not None:
        # print "RELATED FIELD", f.rel.get_related_field()
        # print "REL", dir(f.rel)

        # print "FORMFIELD",f.formfield()
        # for i in dir(f.formfield()):
        # print "FF", i
        # try:
        # print "VALUE_FROM_OBJECT", f.value_from_object(f)
        # except Exception as e:
        # print "EX", e

        # for i in dir(f):
        # print "FORM CONTENTS", i, getattr(f, i)
        # if i == 'value_to_string':
        # print "VALUE_TO_STRING",getattr(f,'value_to_string')(f)
    # return form field with your custom widget here...
    # print "FORMFIELD", dir(f.formfield())

    return f.formfield()


# FIXME: MOVE STYLE INFORMATION OUT OF THERE
def process_advanced_help_text(text):
    if text is None:
        return None
    t = Transductor({
        'a': [
            ("\n([ \t]+)", "\n"),
            ("([ \t]+)", " "),
            (r"-1:", "<div class=\"wio-hint-good\"><i class='ion-alert-circled'></i>&nbsp;", "+1"),
            (r"\+1:", "<div class=\"wio-hint-bad\"><i class='ion-checkmark-circled'></i>&nbsp;", "+1"),
        ],
        "+1": [
            ("\n", "</div>", "a"),
        ]
    })
    return markdown.Markdown().convert(t.process(text, 128))


def wideio_formfield_callback(f):
    if f is None:
        return f
    ff = wideio_formfield_callback0(f)
    if ff is None:
        return ff
    if hasattr(f, "advanced_help_text"):
        ff.advanced_help_text = process_advanced_help_text(f.advanced_help_text)
    return ff


uuid1 = lambda: str(uuid.uuid1())


def mirror_class_and_reparent(c, p):
    import inspect
    nc = type(
        c.__name__, (p,), {
            "__name__": c.__name__, "__module__": c.__module__})
    # --------------------------------
    for x in inspect.classify_class_attrs(c):
        if (not (hasattr(nc, x.name))):
            a = getattr(c, x.name)
            if x.kind == "static method":
                setattr(nc, x.name, staticmethod(a))
            else:
                setattr(nc, x.name, a)
    # --------------------------------
    nc.__name__ = c.__name__
    nc.__module__ = c.__module__
    return nc


def get_default(d):
    v = d.get("default", None)
    if (callable(v)):
        v = v()
    return v


ALLOWED_FIELDS = ['TextField', 'CharField', 'ForeignKey',
                  'IntegerField', 'FloatField', 'BooleanField', 'JSONField']

from wioframework.fields import JSONField

ALLOWED_FIELDS_D = {}
for f in ALLOWED_FIELDS:
    try:
        ALLOWED_FIELDS_D[f] = getattr(models, f)
    except:
        ALLOWED_FIELDS_D[f] = eval(f)


def xwideiomodel(c):
    """
    Instantiate the fields and provide compatibility with WIDE IO web framework.
    """
    nc = mirror_class_and_reparent(c, omodels.Model)
    nc._meta.object_name = c.__name__
    nc._meta.db_table = nc._meta.app_label + "_" + nc.__name__.lower()
    if hasattr(c, "FIELDS"):
        for f in c.FIELDS.items():
            if f[1][0] in ALLOWED_FIELDS:
                # setattr(nc,f[0],get_default(f[1][1]))
                ((ALLOWED_FIELDS_D[f[1][0]])(**f[1][1])
                 ).contribute_to_class(nc, f[0])
    return nc


def default_allm(n):
    import settings
    from django.apps.registry import apps
    # NOT YET IMPLEMENTED
    for app in apps.all_models:
        if n in apps.all_models[app]:
            return apps.all_models[app][n]
    return None


def upgrade_mock(cls, i, m, allm=None, scheme=None):
    if allm is None:
        allm = default_allm
    if not '@id' in m:
        m['@id'] = '%s-%04d' % (cls.__name__, i)
    for f in cls._meta.fields:
        if f.name not in m:
            if hasattr(f, "default") and f.default is not None:
                m[f.name] = f.default
            elif isinstance(f, models.ForeignKey):
                mdl = allm(f.rel.to.__name__)
                if mdl is not None:
                    m[f.name] = mdl.get_mocks(scheme)[0]
                else:
                    m[f.name] = None
    return m


def get_mocks(cls, scheme="default", allm=None):
    raw_mocks = getattr(cls.WIDEIO_Meta, "MOCKS", {})
    raw_mocks = raw_mocks.get(scheme, raw_mocks.get("default", [{}]))
    return [upgrade_mock(cls, i, m, scheme=scheme, allm=None) for i, m in enumerate(raw_mocks)]


def wideiomodel(c):
    """
    This decorator transforms the model so it becomes a WIDE IO model.

    We want to simplify the way everything works.

    * WIDE IO Model must have standard attribute that allow to find easily tthe view/delete/update and list views..
    * Widgets must define the way everything is displayed
    * To provide good UX we will allow any model to be draftable
    * Elements that are not be drafted

    There are different states in which the elements can be :

    - U0: UNVALIDATED AND BLANK
    - U1: UNVALIDATED AND WITH SOME CONTENT, NEVEV ADDED
    - U2: UNVALIDATED AND WITH SOME CONTENT, PREVIOUSLY ADDED
    - V : VALIDATED
    - A : ARCHIVED

    As a general principle it seems that
    """
    if hasattr(c, "FIELDS"):
        c = xwideiomodel(c)

    if hasattr(c, "id"):
        delattr(c, "id")
    # if hasattr(c,"wiostate"):
    #  delattr(c, "wiostate")

    if hasattr(c, "_meta"):
        c._meta.local_fields = filter(
            lambda x: x.name != "id",
            c._meta.local_fields)
        c._meta.concrete_fields = filter(
            lambda x: x.name != "id",
            c._meta.concrete_fields)

    id = models.CharField(
        default=uuid1,
        max_length=38,
        primary_key=True,
        unique=True)
    c.uuid = c.id = getattr(c, "id", id)
    # c._attrs = {"uuid":getattr(c,"uuid", uuid)}
    id.contribute_to_class(c, "id")
    if hasattr(c, "_meta"):
        # c._meta.local_fields.append(id)
        c._meta.concrete_fields.append(id)

    if not hasattr(c, "wiostate") and ((not hasattr(c, "_meta")) or (
                len(filter(lambda x: x.name == 'wiostate', c._meta.fields)) == 0)):
        # possible choice = V(validated) U(unvalidated/draft), D(Deleted),
        # A(Archived)
        wiostate = models.CharField(default="V", max_length=2, db_index=True)
        c.wiostate = getattr(c, "wiostate", wiostate)
        wiostate.contribute_to_class(c, "wiostate")

    if not hasattr(c, "tenant") and ((not hasattr(c, "_meta")) or (
                len(filter(lambda x: x.name == 'tenant', c._meta.fields)) == 0)):
        tenant = models.CharField(max_length=12, db_index=True, default="")
        c.tenant = getattr(c, "tenant", tenant)
        tenant.contribute_to_class(c, "tenant")

    if not hasattr(c, "_metadata") and ((not hasattr(c, "_meta")) or (
                len(filter(lambda x: x.name == '_metadata', c._meta.fields)) == 0)):
        # possible choice = V(validated) U(unvalidated/draft), D(Deleted),
        # A(Archived)
        _metadata = lib_fields.JSONField(blank=True, null=True)
        c._metadata = getattr(c, "_metadata", _metadata)
        _metadata.contribute_to_class(c, "_metadata")
    c.metadata = property(lambda self: self._metadata)

    def metadata_setter(x, o):
        x._metadata = o
        return o

    c.metadata = c.metadata.setter(metadata_setter)

    c.get_base_url = staticmethod(lambda: Model.get_base_url(c))
    c.get_list_url = staticmethod(lambda: Model.get_list_url(c))
    c.get_add_url = staticmethod(lambda: Model.get_add_url(c))
    c.get_model_name = staticmethod(lambda: Model.get_model_name(c))
    c.get_mocks = classmethod(get_mocks)
    c.html = Model.html
    c.as_td = Model.as_td
    c.as_tdh = Model.as_tdh
    c.archive = Model.archive
    c.unarchive = Model.unarchive
    c._base_url = Model._base_url
    c.get_absolute_url = Model.get_absolute_url
    c.html_empty_result = staticmethod(lambda r: Model.html_empty_result(c, r))
    c.get_view_url = Model.get_view_url
    c.get_update_url = Model.get_update_url
    c.get_del_url = Model.get_del_url
    c.get_delete_url = Model.get_del_url
    c.get_references_url = Model.get_references_url
    c.get_rel_list_url = Model.get_rel_list_url
    c.get_references_url = Model.get_references_url
    c.get_action_url = Model.get_action_url
    c.as_document = Model.as_document
    c.get_content_type_id = Model.get_content_type_id
    c.class_id = hashlib.md5(c.__name__).hexdigest()[::2]

    if not hasattr(c, "get_icon"):
        c.get_icon = classmethod(wideio_get_icon)
    c.formfield_callback = staticmethod(wideio_formfield_callback)
    if not hasattr(c, "API"):
        c.API = True
    if not hasattr(c, "ctrname_s"):  # TODO: refactor to verbose_name
        c.ctrname_s = name_to_ctr_s(c.__name__)
    if not hasattr(c, "ctrname_p"):
        c.ctrname_p = name_to_ctr_p(c.__name__)
    if not hasattr(c, "WIDEIO_Meta"):
        class WIDEIO_Meta(object):
            pass

        c.WIDEIO_Meta = WIDEIO_Meta

    if not hasattr(c, "Meta"):
        class Meta(object):
            pass

        c.Meta = Meta

    c.Meta.default_permissions = ()

    if not hasattr(c.WIDEIO_Meta, "DISABLE_VIEW_ACCOUNTING"):
        if not hasattr(c, "_nviews") and ((not hasattr(c, "_meta")) or (
                    len(filter(lambda x: x.name == '_nviews', c._meta.fields)) == 0)):
            # possible choice = V(validated) U(unvalidated/draft), D(Deleted),
            # A(Archived)
            _nviews = models.IntegerField(default=0, db_index=True)
            c._nviews = getattr(c, "_nviews", _nviews)
            _nviews.contribute_to_class(c, "_nviews")

        def accounting_on_view(self, request):
            self._nviews = self._nviews + 1

            ## < BUT WITHOUT CALLING PRESAVE TO AVOID UPDATE THE DATE
            self.save(update_fields=["_nviews"])

        c.nviews = property(lambda self: self._nviews)

        if hasattr(c, "on_view"):
            actc0 = getattr(c, "on_view")

            def Eon_view(self, o):
                r = actc0(self, o)
                accounting_on_view(self, o)
                return r

            setattr(c, "on_view", Eon_view)
        else:
            setattr(c, "on_view", accounting_on_view)

        if not hasattr(c, "_nupdates") and ((not hasattr(c, "_meta")) or (
                    len(filter(lambda x: x.name == '_nupdates', c._meta.fields)) == 0)):
            # possible choice = V(validated) U(unvalidated/draft), D(Deleted),
            # A(Archived)
            _nupdates = models.IntegerField(default=0, db_index=True)
            c._nupdates = getattr(c, "_nupdates", _nupdates)
            _nupdates.contribute_to_class(c, "_nupdates")

        def accounting_on_update(self, request):
            # if not request.user.is_anonymous:
            self._nupdates = self._nviews + 1
            self.save()

        c.nupdates = property(lambda self: self._nupdates)

        if (hasattr(c, "on_update")):
            actc1 = getattr(c, "on_update")

            def Eon_update(self, o):
                r = actc1(self, o)
                accounting_on_update(self, o)
                return r

            setattr(c, "on_update", Eon_update)
        else:
            setattr(c, "on_update", accounting_on_update)

    if not hasattr(c.WIDEIO_Meta, "Actions"):
        class Actions(object):
            pass

        c.WIDEIO_Meta.Actions = Actions

    if not hasattr(c.WIDEIO_Meta.Actions, "_act_list"):
        setattr(c.WIDEIO_Meta.Actions, "_act_list", [])

    if not hasattr(c.WIDEIO_Meta, "permissions"):
        perm = dec.perm_read_logged_users_write_for_admin_only
    else:
        perm = c.WIDEIO_Meta.permissions

    if not hasattr(c, "can_add"):
        c.can_add = staticmethod(perm["add"])

    if not hasattr(c, "can_update"):
        c.can_update = lambda o, r: (perm["update"](o, r))
        # raise Exception,perm["update"](None,None)

    if not hasattr(c, "can_delete"):
        c.can_delete = lambda o, r: (perm["delete"](o, r))

    if not hasattr(c, "can_view"):
        c.can_view = lambda o, r: (perm["view"](o, r))

    if not hasattr(c, "can_list"):
        c.can_list = staticmethod(perm["list"])

    if not hasattr(c.WIDEIO_Meta.Actions, "update"):
        wideio_action(
            "edit",
            possible=lambda o,
                            x: c.can_update(
                o,
                x),
            icon="icon-edit",
            thref="href",
            mimetype="text/html")(lambda o, r: {'_redirect':o.get_update_url()}).contribute_to_class(c)

    if (not hasattr(c.WIDEIO_Meta.Actions, "delete")):
        wideio_action(
            "delete",
            possible=lambda o,
                            x: c.can_delete(
                o,
                x),
            icon="icon-remove",
            mimetype="text/html")(
            lambda o, r: {'_redirect':
                              o.get_delete_url()}).contribute_to_class(c)

    if (not hasattr(c.WIDEIO_Meta.Actions, "archive")):
        wideio_action(
            "archive",
            possible=lambda o, r: (r.user.is_staff) and (o.wiostate != 'A'),
            icon="icon-remove")(
            lambda o, r: o.archive() or " window.alert('done');").contribute_to_class(c)

    if (not hasattr(c.WIDEIO_Meta.Actions, "unarchive")):
        wideio_action(
            "unarchive",
            possible=lambda o, r: (r.user.is_staff) and (o.wiostate == 'A'),
            icon="icon-remove")(
            lambda o, r: o.unarchive() or " window.alert('done');").contribute_to_class(c)

    if (not hasattr(c.WIDEIO_Meta, "views_kwargs")):
        c.WIDEIO_Meta.views_kwargs = {}

    for fn in filter(lambda n: n[0] != '_', dir(c.WIDEIO_Meta.Actions)):
        getattr(c.WIDEIO_Meta.Actions, fn).register_into_class(c)

    if (not hasattr(c.WIDEIO_Meta, "icon")):
        c.WIDEIO_Meta.icon = "icon-puzzle-piece"
    if (not hasattr(c.WIDEIO_Meta, "form_exclude")):
        c.WIDEIO_Meta.form_exclude = [
            "id",
            "wiostate",
            "tenant",
            "_draft_for",
            "_metadata",
            "_nviews",
            "_nupdates"
        ]
    else:
        c.WIDEIO_Meta.form_exclude.append("id")
        c.WIDEIO_Meta.form_exclude.append("wiostate")
        c.WIDEIO_Meta.form_exclude.append("tenant")
        c.WIDEIO_Meta.form_exclude.append("_draft_for")
        c.WIDEIO_Meta.form_exclude.append("_metadata")
        c.WIDEIO_Meta.form_exclude.append("_nviews")
        c.WIDEIO_Meta.form_exclude.append("_nupdates")

    c.clean_fields = Model.clean_fields
    try:
        # for d in c._meta.get_all_field_names():
        # in list(c._meta.get_all_related_m2m_objects_with_model()) +
        # list(c._meta.get_all_related_objects_with_model()) +
        # list(c._meta.get_m2m_with_model()) +
        for f, model in list(c._meta.get_fields_with_model()):
            if f.__class__.__name__ == "ListField":
                c.WIDEIO_Meta.form_exclude.append(f.name)
    except Exception as e:
        print e

    # call the decorators for wideio_timestamped_autoexpiring and draftable
    if (not hasattr(c.WIDEIO_Meta, "NO_DRAFT")) or (
            not c.WIDEIO_Meta.NO_DRAFT):
        c = wideio_timestamped_autoexpiring(
            expire=datetime.timedelta(
                21,
                0,
                0),
            delete_database_entry_on_expire=True)(c)
        c = wideio_draftable(c)
        c.is_draft = lambda s: (
            s._draft_for is not None or s.wiostate[0] == "U")
    else:
        c.is_draft = lambda s: False

    # print c._meta
    return c


def wideio_setnames(singular="object", plural=None):
    if plural is None:
        plural = singular + 's'

    def dec(tc):
        tc.ctrname_s = singular
        tc.ctrname_p = plural
        return tc

    return dec


def get_dependent_models(m):
    dm = []
    for i in range(len(m._meta.many_to_many)):
        n = m._meta.many_to_many[i]
        if not hasattr(n, "get_rel_list_url") and not isinstance(getattr(m, n.name).through, str):
            if hasattr(settings, "WIO_IMPLICIT_MODEL_FOR_EDGES") and settings.WIO_IMPLICIT_MODEL_FOR_EDGES:
                nm = wideiomodel(getattr(m, n.name).through)
                # setattr(m,n.name,nm)
                # m._meta.many_to_many[i]=nm
                dm.append(nm)
    return dm


from actions import wideio_action, wideio_relaction, wideio_inject_extension
from buyable import wideio_buyable
from commentable import wideio_commentable
from followable import wideio_followable
from historytrack import wideio_history_track
from invokable import wideio_name_invokable
from likeable import wideio_likeable
from moderated import wideio_moderated
from draftable import wideio_draftable
from owned import wideio_owned, wideio_multi_owned
from publishable import wideio_publishable
from tagged import wideio_tagged
from timestamped import wideio_timestamped, wideio_timestamped_autoexpiring

# def wideio_analytics():
#    time_displayed=models.IntegerField()
# allow sort py popularity
# get the number of time the object is displayed...
