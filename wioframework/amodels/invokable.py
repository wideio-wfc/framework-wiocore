# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################

"""
AMODEL : ADVANCED MODELS

This contains the decorator for components the components of WIDE IO
"""

import django.db.models as omodels
from django.db.models.fields.related import RelatedField
from django.http import Http404
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.utils.html import escape
from django.core.exceptions import ValidationError
from django.conf import settings
import wioframework.fields as models
from wioframework.atomic_ops import atomic_incr
from wioframework import decorators as dec
from wioframework.decorators import PermissionException
from wioframework.decorators import permission_based
import wioframework.fields as lib_fields
import wioframework.utils as utils
import markdown
from actions import wideio_action, wideio_relaction


def wideio_name_invokable(add_fields_constructor={}, name_field="name"):
    """
    Makes possible to create a valid entry just providing a name
    """

    def dec(tc):
        if not hasattr(tc, "invoke_by_name"):
            def invoke_by_name(name, request=None, update_ni=None):
                args = dict()
                args[name_field] = name
                if update_ni is None:
                    r = tc.objects.filter(**args)  # < we accept drafts
                    if r.count() != 0:
                        return r[0]
                    ni = tc()
                    setattr(ni, name_field, name)
                else:
                    ni = update_ni
                try:
                    for attr, fn in add_fields_constructor.items():
                        a = getattr(ni, attr)
                        b = fn(name, request)
                        if callable(a):
                            a(b)
                        else:
                            setattr(ni, attr, b)
                    if request is not None:
                        if hasattr(ni.WIDEIO_Meta, "OWNER_FIELD"):
                            if request.user.is_authenticated():
                                setattr(
                                    ni, getattr(ni.WIDEIO_Meta, "OWNER_FIELD"), request.user)
                    ni.wiostate = 'V'
                    ni.save()
                    return ni
                except Exception as e:
                    print e
                    return None

            def reinvoke(self, request):
                invoke_by_name(getattr(ni, name_field), request, self)

            wideio_action(possible=lambda x, request: request.user.is_staff)(reinvoke).contribute_to_class(tc)
            tc.invoke_by_name = staticmethod(invoke_by_name)

            if not hasattr(tc, "invoke"):
                tc.invoke = staticmethod(invoke_by_name)
        return tc

    return dec
