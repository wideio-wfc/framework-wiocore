# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################

"""
AMODEL : ADVANCED MODELS

This contains the decorator for components the components of WIDE IO
"""
import os

import django.db.models as omodels
from django.db.models.fields.related import RelatedField

from django.http import Http404
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.utils.html import escape

#
from django.core.exceptions import ValidationError

from django.conf import settings

#from django.contrib.auth.models import  AbstractUser

import wioframework.fields as models

from wioframework.atomic_ops import atomic_incr
from wioframework import decorators as dec
from wioframework.decorators import PermissionException
from wioframework.decorators import permission_based
import wioframework.fields as lib_fields

import wioframework.utils as utils
import inspect

from actions import wideio_action, wideio_relaction

def wideio_publishable():
    def dec(tc):
        if not (hasattr(tc, "get_all_references")):
            raise Exception(
                "wideio_publishable models must implement get_all_references")

        ogr = tc.get_all_references
        if len(inspect.getargs(ogr.__code__).args) == 1:
            def ngr(self, stack=None):
                if stack is None:
                    stack = []
                else:
                    if self in stack:
                        return []
                return ogr(self)
        else:
            def ngr(self, stack=None):
                if stack is None:
                    stack = []
                else:
                    if self in stack:
                        return []
                return ogr(self, stack)
        setattr(tc, "get_all_references", ngr)

        def publish_and_save(self, request, overwrite=True):
            if hasattr(self,"_draft_for_id") and self._draft_for_id:
                return 'window.alert("draft copies may not be saved as such")'
            import yaml
            folder = os.path.join(
                "initial_fixture",
                "data",
                self._meta.db_table)
            if not os.path.exists(folder):
                os.mkdir(folder)
            filename = os.path.join(folder, self.id) + ".yaml"
            d = self.__dict__
            d = dict(filter(lambda i: i[0][0] != "_", d.items()))
            open(filename, "w").write(yaml.dump(d))
            os.system("git add %s" % (filename,))
            for f in self.get_all_references():
                if hasattr(f, "WIDEIO_Meta") and hasattr(
                        f.WIDEIO_Meta, "publish_and_save"):
                    f.WIDEIO_Meta.publish_and_save(f, request, False)
            return 'window.alert("done")'
        wideio_action(
            icon="icon-save",
            possible=lambda o,
            r: (
                r.user is not None and r.user.is_staff))(publish_and_save).contribute_to_class(tc)

        def unpublish(self, request):
            folder = os.path.join(
                "initial_fixture",
                "data",
                self._meta.db_table)
            filename = os.path.join(folder, self.id) + ".yaml"
            os.system("git rm %s" % (filename,))
        wideio_action(
            icon="icon-trash",
            possible=lambda o,
            r: (
                r.user is not None and r.user.is_staff and os.path.exists(
                    os.path.join(
                        os.path.join(
                            "initial_fixture",
                            "data",
                            tc._meta.db_table),
                        o.id) +
                    ".yaml")))(unpublish).contribute_to_class(tc)

        def see_references(self, request):
            return HttpResponseRedirect(self.get_references_url())
        wideio_action(icon="icon-star",
                      possible=lambda o,
                      r: (r.user is not None and r.user.is_staff),
                      mimetype="text/html")(see_references).contribute_to_class(tc)
        return tc

    return dec
