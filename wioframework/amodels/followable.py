# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################

"""
AMODEL : ADVANCED MODELS

This contains the decorator for components the components of WIDE IO
"""
import sys


import django.db.models as omodels
from django.db.models.fields.related import RelatedField

from django.http import Http404
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.utils.html import escape

from django.core.exceptions import ValidationError

from django.conf import settings


import wioframework.fields as models

from wioframework.atomic_ops import atomic_incr
from wioframework import decorators as dec
from wioframework.decorators import PermissionException
from wioframework.decorators import permission_based
import wioframework.fields as lib_fields

import wioframework.utils as utils
import markdown
from actions import wideio_action, wideio_relaction

def wideio_followable(Rthrough):
    """
    This implements the follow behaviours.
    Currently it is recommended to pass the class that is used to store the follow relationship.
    """

    def dec(tc):
        if not hasattr(tc, "followers"):
            from accounts.models import UserAccount
            followers = models.ManyToManyField(
                UserAccount,
                related_name=tc.__name__ +
                "_followed",
                blank=True,
                null=True,
                through=Rthrough)
            followers.contribute_to_class(tc, "followers")
            followers_count = models.IntegerField(default=0, editable=False)
            followers_count.contribute_to_class(tc, "followers_count")

            def follow(self, request):
                rel = self.followers.through()
                rel.useraccount = request.user
                setattr(rel, tc.__name__.lower(), self)
                rel.save()
                rel.on_add(request)
                return 'alert("ok");'
            wideio_action(
                possible=lambda o,
                r: ((not r.user.is_anonymous()) and not (
                    r.user in o.get_followers())),
                icon="icon-plus-sign-alt")(follow).contribute_to_class(tc)

            def unfollow(self, request):
                kwargs = {
                    tc.__name__.lower() +
                    '_id': self.id,
                    'useraccount_id': request.user.id}
                tx = self.followers.through.objects.filter(**kwargs)[0]
                tx.on_delete(request)
                tx.delete()
                return 'alert("ok");'                
            # FIXME GET A MORE STRAIGHTFORWARD "IN" TEST DONE IN DB
            wideio_action(possible=lambda o,
                          r: ((not r.user.is_anonymous()) and  (r.user in o.get_followers())),
                          icon="icon-unlink")(unfollow).contribute_to_class(tc)
            # print tc,tc.WIDEIO_Meta.Actions._act_list

            def get_followers(self):
                kwargs = {tc.__name__.lower() + '_id': self.id}
                return map(
                    lambda x: x.useraccount, self.followers.through.objects.filter(**kwargs))
            setattr(tc, "get_followers", get_followers)

            if Rthrough is None:
                # TODO FIXME: THAT MAY BE MORE CONVENIENT THAN THE OTHER
                # ALTERNATIVE
                @wideio_owned("useraccount")
                @wideiomodel
                class RelFollow(omodels.Model):

                    def on_add(self, request):
                        getattr(
                            self,
                            tc.__name__.lower()).followers_count = getattr(
                            self,
                            tc.__name__.lower()).followers_count + 1
                        getattr(self, tc.__name__.lower()).save()

                    def on_delete(self, request):
                        getattr(
                            self,
                            tc.__name__.lower()).followers_count = getattr(
                            self,
                            tc.__name__.lower()).followers_count - 1
                        getattr(self, tc.__name__.lower()).save()

                    class WIDEIO_Meta:
                        NO_DRAFT = True
                RelFollow.__name__ = "RelFollow" + tc.__name__
                attr = models.ForeignKey(tc, db_index=True)
                print attr
                attr.contribute_to_class(RelFollow, "followers")
                from django.db.models.loading import cache
                cache.app_models[u'science']["RelFollow" +tc.__name__] = RelFollow
                from django.apps.registry import apps
                apps.register_model('science', RelFollow)
                setattr(sys.modules[tc.__module__],"RelFollow" +tc.__name__,RelFollow)
                # print hasattr(sys.modules[tc.__module__],
                # "RelFollow"+tc.__name__)

            else:
                #from django.db.models.loading import cache
                # relmodel=cache.app_models[u'science'][Rthrough]
                import sys
                sys.stderr.write("%r\n" % (tc,))
                relmodel = getattr(sys.modules[tc.__module__], Rthrough)

            def on_add(self, request):
                r = atomic_incr(
                    getattr(
                        self,
                        tc.__name__.lower()),
                    'followers_count')
                setattr(
                    getattr(
                        self,
                        tc.__name__.lower()),
                    "followers_count",
                    r.followers_count)

            def on_delete(self, request):
                r = atomic_incr(
                    getattr(
                        self, tc.__name__.lower()), 'followers_count', -1)
                setattr(
                    getattr(
                        self,
                        tc.__name__.lower()),
                    "followers_count",
                    r.followers_count)
            setattr(relmodel, "on_add", on_add)
            setattr(relmodel, "on_delete", on_delete)
            if hasattr(tc, "WIDEIO_Meta"):
                if not hasattr(tc.WIDEIO_Meta, "form_exclude"):
                    tc.WIDEIO_Meta.form_exclude = []
                tc.WIDEIO_Meta.form_exclude += ['followers', 'followers_count']
        return tc
    return dec

