# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################

"""
AMODEL : ADVANCED MODELS

This contains the decorator for components the components of WIDE IO
"""
import sys


#from django.db import models
import django.db.models as omodels
from django.db.models.fields.related import RelatedField

from django.http import Http404
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.utils.html import escape

#
from django.core.exceptions import ValidationError

from django.conf import settings

#from django.contrib.auth.models import  AbstractUser

import wioframework.fields as models

from wioframework.atomic_ops import atomic_incr
from wioframework import decorators as dec
from wioframework.decorators import PermissionException
from wioframework.decorators import permission_based
import wioframework.fields as lib_fields

import wioframework.utils as utils
import markdown


def wideio_likeable(Rthrough):
    """
    Anything that may be liked should be an instance of this class.
    """

    def dec(tc):
        if (not hasattr(tc, "liking")):
            likers = models.ManyToManyField(
                'accounts.UserAccount',
                related_name=tc.__name__ +
                "_liked",
                blank=True,
                null=True,
                through=Rthrough)
            likers.contribute_to_class(tc, "like")
            likers_count = models.IntegerField(default=0)
            likers_count.contribute_to_class(tc, "like_count")

            def like(self, request):
                rel = self.likers.through()
                rel.useraccount = request.user
                setattr(rel, tc.__name__.lower(), self)
                rel.save()
                rel.on_add(request)
            # print "X"
            try:
                wideio_action(
                    possible=lambda o,
                    r: not (
                        r.user in o.get_likers()),
                    icon="icon-thumbs-up")(like).contribute_to_class(tc)
            except Exception as e:
                print e

            def unlike(self, request):
                kwargs = {
                    tc.__name__.lower() +
                    '_id': self.id,
                    'useraccount_id': request.user.id}
                tx = self.likers.through.objects.filter(**kwargs)[0]
                tx.on_delete(request)
                tx.delete()
            wideio_action(
                possible=lambda o, r: (
                    r.user in o.get_likers()))(unlike).contribute_to_class(tc)
            # print tc,tc.WIDEIO_Meta.Actions._act_list

            def get_likers(self):
                kwargs = {tc.__name__.lower() + '_id': self.id}
                return map(
                    lambda x: x.useraccount, self.likers.through.objects.filter(**kwargs))
            setattr(tc, "get_likers", get_likers)

            if False:
                @wideiomodel
                @wideio_owned("useraccount")
                class RelFollow(omodels.Model):

                    def on_add(self, request):
                        getattr(
                            self,
                            tc.__name__.lower()).followers_count = getattr(
                            self,
                            tc.__name__.lower()).followers_count + 1
                        getattr(self, tc.__name__.lower()).save()

                    def on_delete(self, request):
                        getattr(
                            self,
                            tc.__name__.lower()).followers_count = getattr(
                            self,
                            tc.__name__.lower()).followers_count - 1
                        getattr(self, tc.__name__.lower()).save()
                attr = models.ForeignKey(tc)
                attr.contribute_to_class(RelLike, tc.__name__.lower())
                #from django.db.models.loading import cache
                # cache.app_models[u'science']["RelLike"+tc.__name__]=RelFollow
                # setattr(sys.modules[tc.__module__],"RelLike"+tc.__name__,RelFollow)

            #from django.db.models.loading import cache
            # relmodel=cache.app_models[u'science'][Rthrough]
            relmodel = getattr(sys.modules[tc.__module__], Rthrough)

            def on_add(self, request):
                r = atomic_incr(
                    getattr(
                        self,
                        tc.__name__.lower()),
                    'like_count')
                setattr(
                    getattr(
                        self,
                        tc.__name__.lower()),
                    "like_count",
                    r.like_count)

            def on_delete(self, request):
                r = atomic_incr(
                    getattr(
                        self, tc.__name__.lower()), 'like_count', -1)
                setattr(
                    getattr(
                        self,
                        tc.__name__.lower()),
                    "like_count",
                    r.like_count)
            setattr(relmodel, "on_add", on_add)
            setattr(relmodel, "on_delete", on_delete)
            if (hasattr(tc, "WIDEIO_Meta")):
                if (not hasattr(tc.WIDEIO_Meta, "form_exclude")):
                    tc.WIDEIO_Meta.form_exclude = []
                tc.WIDEIO_Meta.form_exclude += ['likers', 'like_count']
        return tc
    return dec


