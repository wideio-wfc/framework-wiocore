# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################

"""
AMODEL : ADVANCED MODELS

This contains the decorator for components the components of WIDE IO
"""


import django.db.models as omodels
from django.db.models.fields.related import RelatedField

from django.http import Http404
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.utils.html import escape

#
from django.core.exceptions import ValidationError
from django.conf import settings
import wioframework.fields as models

from wioframework.atomic_ops import atomic_incr
from wioframework import decorators as dec
from wioframework.decorators import PermissionException
from wioframework.decorators import permission_based
import wioframework.fields as lib_fields

import wioframework.utils as utils
import markdown
from actions import wideio_action, wideio_relaction


def wideio_ndaable(require_nda=True):
    def dec(tc):
        ## We add a boolean field "requires_nda"
        
        
        ## 1. ENSURES THAT OWNER FIELD EXISTS
        
        
        ## 2. DO WE HAVE A CALLABLE TO DECIDE IF AN OBJECT REQUIRES A NDA        
        
        ## If requires NDA, on_view will fail if the user has not agreed a NDA with the owner of the object for the purpose of this document, or a universal NDA between the two parties.
        ## The NDA nees to be accepted by both party to be valid and needs to be still in force. If it is not a page inviting the user to reconnect later shall be displayed.
        def X_on_view(s,r):
            if r.user.is_anonymous():
                raise PermissionException()
            if r.user==getattr(s,tc.WIDEIO_Meta.OWNER_FIELD):
                return 
            from network.models import NDA
            existing_nda=NDA.objects.filter(object_pk=s.id,owner=request.user,signature__neq=None)
            if existing_nda.count():
                ## CHECK IF ALREADY SIGNED...
                assert(False) #< NOT YET IMPLEMENTED
            else:
                nda=NDA()
                nda.owner=request.user()
                nda.content_object=self
                nda.save()
                return {'_redirect':nda.get_view_url()}
            
        ## LIST MODE JSON MUST RETURN ONLY MINIMAL INFORMATION
        
        
        return tc
    return dec

