# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################

"""
AMODEL : ADVANCED MODELS

This contains the decorator for components the components of WIDE IO
"""
import time
import datetime
import django.db.models as omodels
from django.db.models.fields.related import RelatedField
from django.conf import settings
import wioframework.fields as models
from wioframework import decorators as dec
import wioframework.fields as lib_fields


def wideio_history_track(lfields,
                         length=datetime.timedelta(1),
                         max_update_interval=60,
                         history_suffix="_history"
                         ):
    """
    Allows to memorize the temporal evolution of various fields
    @param lfields: list of fields for which we want to create a history
    @param length: how long do we keep track of activity for
    @param max_update_interval:(seconds) used to avoid to update the history too frequently
    @param history_suffix: suffix used to declare fields that are related to data history.
    @return:
    """

    def dec(tc):
        ts = (length.total_seconds() if hasattr(length, "total_seconds") else length)
        mui = max_update_interval
        if hasattr(mui, "total_seconds"):
            mui = mui.total_seconds()

        for f in lfields:
            nf = lib_fields.JSONField(null=True, blank=True)
            nf.contribute_to_class(tc, f + history_suffix)
            tc.WIDEIO_Meta.form_exclude.append(f + history_suffix)
        ooa = (tc.on_add if hasattr(tc, "on_add") else lambda s, r: None)

        def ht_on_add(self, request):
            res = ooa(self, request)
            n = time.time()
            for f in lfields:
                pv = getattr(self, f + history_suffix)
                if type(pv) not in [list]:
                    pv = []
                if (len(pv) == 0) or ((n - pv[-1][0]) > mui):
                    pv.append((n, getattr(self, f)))
                pv = filter(lambda t: (n - t[0] < ts), pv)
                setattr(self, f + history_suffix, pv)
            return res

        tc.on_add = ht_on_add
        oou = ooa = (
            tc.on_update if hasattr(tc, "on_update") else lambda s, r: None)

        def ht_on_update(self, request):
            res = oou(self, request)
            n = time.time()
            for f in lfields:
                pv = getattr(self, f + history_suffix)
                if type(pv) not in [list]:
                    pv = []
                if (len(pv) == 0) or ((n - pv[-1][0]) > mui):
                    pv.append((n, getattr(self, f)))
                pv = filter(lambda t: (n - t[0] < ts), pv)
                setattr(self, f + history_suffix, pv)
            return res

        tc.on_update = ht_on_update

        return tc

    return dec
