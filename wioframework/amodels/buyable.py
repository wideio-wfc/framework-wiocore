# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################

"""
AMODEL : ADVANCED MODELS

This contains the decorator for components the components of WIDE IO
"""
import django.db.models as omodels
from django.db.models.fields.related import RelatedField

from django.http import Http404
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.utils.html import escape

from django.core.exceptions import ValidationError

from django.conf import settings

import wioframework.fields as models

from wioframework.atomic_ops import atomic_incr
from wioframework import decorators as dec
from wioframework.decorators import PermissionException
from wioframework.decorators import permission_based
import wioframework.fields as lib_fields
from mml.transductor import Transductor

import wioframework.utils as utils
import markdown


def wideio_buyable(academic_price=lambda self: self.price *
                   0.6, business_price=lambda self: self.price, description=None):
    #~ buyable_analytics=EmbeddedModelField(BuyableAnalytics)
    """
    Anything  that can be bought should be an instance of this class
    """
    def dec(tc):
        if (not hasattr(tc, "price")):
            price = models.IntegerField(default=0, blank=True)
            price.contribute_to_class(tc, "price")

            def buy(self, request):
                from payment.models import CreditAccount, Transaction
                # obviously it will require a confirm...
                # TODO: CREATE A FORM THAT WILL SEND A POST IF VALIDATED (TO GENERALIZE AS A DECORATOR)
                #~ if request.method=="POST":
                buyer = request.user

                transaction = Transaction()
                transaction.from_account = CreditAccount.objects.get(
                    owner=buyer)
                transaction.to_account = self.owner  # NULL means of wide io
                transaction.description = ((description(self) if callable(description) else description)
                                           if description is not None else "N/A")
                if buyer.is_verified_scientist:
                    transaction.amount = academic_price(self)
                else:
                    transaction.amount = business_price(self)
                transaction.save()
                transaction.on_add(request)
                transaction.save()
            wideio_action(icon="icon-credit-card")(buy).contribute_to_class(tc)

            def history_save(self):
                self.dates_history.add()
                self.times_bought_history.add()
                self.amount_bought_history.add()

            class WIDEIO_Meta:
                ignore_field = [
                    'times_bought',
                    'amount_bought',
                    'dates_history',
                    ' times_bought_history',
                    'amount_bought_history']

        return tc
    return dec

