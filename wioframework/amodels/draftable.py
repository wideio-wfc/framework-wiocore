# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################

"""
AMODEL : ADVANCED MODELS

This contains the decorator for components the components of WIDE IO
"""


#from django.db import models
import django.db.models as omodels
from django.db.models.fields.related import RelatedField

from django.http import Http404
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.utils.html import escape

#
from django.core.exceptions import ValidationError

from django.conf import settings

#from django.contrib.auth.models import  AbstractUser

import wioframework.fields as models

from wioframework.atomic_ops import atomic_incr
from wioframework import decorators as dec
from wioframework.decorators import PermissionException
from wioframework.decorators import permission_based
import wioframework.fields as lib_fields


import wioframework.utils as utils
import markdown



from actions import wideio_action, wideio_relaction


def wideio_draftable(tc):
    if not hasattr(tc, "_draft_for"):
        if not hasattr(tc, "WIDEIO_Meta"):
            class WIDEIO_Meta(object):
                pass
            tc.WIDEIO_Meta = WIDEIO_Meta
        if not hasattr(tc.WIDEIO_Meta, "form_exclude"):
            tc.WIDEIO_Meta.form_exclude = []
        if not hasattr(tc.WIDEIO_Meta, "allow_query"):
            tc.WIDEIO_Meta.allow_query = []
        tc.WIDEIO_Meta.allow_query.append('_draft_for')
        tc.WIDEIO_Meta.allow_query.append('_draft_for__neq')

        _draft_for = models.ForeignKey(
            tc.__name__,
            null=True,
            blank=True,
            related_name="rev_draft")
        _draft_for.contribute_to_class(tc, "_draft_for")

        def copy_fields(self, dst):
            copied_fields = tc._meta.get_all_field_names()
            L1 = map(lambda f: f.name, tc._meta.many_to_many)
            L2 = map(
                lambda f: f.get_accessor_name(),
                tc._meta.get_all_related_many_to_many_objects())
            L3 = map(
                lambda f: f.get_accessor_name(),
                tc._meta.get_all_related_objects())

            for field in copied_fields:
                if field in [
                        '_draft_for', 'id', 'uuid', 'wiostatus', 'created_at', 'updated_at']:
                    continue
                if field in L1:
                    continue
                if field in L2:
                    continue
                if field in L3:
                    continue
                if hasattr(self, field):
                    setattr(dst, field, getattr(self, field))
        tc.copy_draft = copy_fields

        # def see_draft_version(self, request):
        #    return HttpResponseRedirect(self.draft.get_view_url())
        #wideio_action(mimetype="text/html", possible=lambda o, r: (is_moderator(r.user, o) and o.is_submitted_for_review))(see_draft_version).contribute_to_class(tc)
        # if not hasattr(tc.WIDEIO_Meta,"default_query"):
        #    #default_query={'is_draft':False}
        #    from django.db.models import Q
        #    default_query = lambda r: (Q(is_published=True) | Q(owner=r.user) | Q(is_draft=False))
        #    setattr(tc.WIDEIO_Meta, "default_query", staticmethod(default_query))
    return tc

