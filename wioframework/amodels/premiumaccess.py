# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################

"""
AMODEL : ADVANCED MODELS

This contains the decorator for components the components of WIDE IO
"""

# from django.db import models
import django.db.models as omodels
from django.db.models.fields.related import RelatedField
from django.http import Http404
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.utils.html import escape
#
from django.core.exceptions import ValidationError
from django.conf import settings
# from django.contrib.auth.models import  AbstractUser

import wioframework.fields as models
from wioframework.atomic_ops import atomic_incr
from wioframework import decorators as dec
from wioframework.decorators import PermissionException
from wioframework.decorators import permission_based
import wioframework.fields as lib_fields
import wioframework.utils as utils
import markdown
from actions import wideio_action, wideio_relaction


def wideio_admin_owned():
    def dec(tc):
        return tc

    return dec


def wideio_premium(premium_duration=datetime.timedelta(10)):
    def owned_default_can_view(obj, request):
        return request.user.is_superuser or (
            request.user.id == getattr(obj, field_name + "_id")
        )

    def make_gdefault_query(is_public_field):
        @staticmethod
        def gdefault_query(request):
            from django.db.models import Q
            if request.user.is_staff:
                return Q(wiostate='V')
            else:
                if public:
                    return Q(wiostate='V')
                else:
                    if is_public_field:
                        return Q(**{is_public_field: True}) | Q(**{field_name: request.user})
                    else:
                        return Q(**{field_name: request.user})

        return gdefault_query

    def dec(tc):
        if hasattr(tc, "can_view"):
            otc44 = getattr(tc, "can_view")

            def can_view_cumul(r):
                if not otc44(r):
                    return False
                return owned_default_can_add(
                    r, tc, tc.__name__.lower() + "_rev_" + field_name)

            tc.can_view = staticmethod(can_add_cumul)
        else:
            tc.can_view = staticmethod(
                lambda r: (
                    owned_default_can_add(
                        r,
                        tc,
                        tc.__name__.lower() +
                        "_rev_" +
                        field_name)))

        if not hasattr(tc, "WIDEIO_Meta"):
            class WIDEIO_Meta(object):
                default_query = make_gdefault_query(c_is_public_field)

            tc.WIDEIO_Meta = WIDEIO_Meta
        else:
            if not hasattr(tc.WIDEIO_Meta, "default_query"):
                tc.WIDEIO_Meta.default_query = make_gdefault_query(c_is_public_field)

        if not hasattr(tc.WIDEIO_Meta, "sort_enabled"):
            tc.WIDEIO_Meta.sort_enabled = []
        return tc

    return dec
