# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################

"""
AMODEL : ADVANCED MODELS

This contains the decorator for components the components of WIDE IO
"""


import django.db.models as omodels
from django.db.models.fields.related import RelatedField

from django.http import Http404
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.utils.html import escape

from django.core.exceptions import ValidationError

from django.conf import settings

import wioframework.fields as models

from wioframework.atomic_ops import atomic_incr
from wioframework import decorators as dec
from wioframework.decorators import PermissionException
from wioframework.decorators import permission_based
import wioframework.fields as lib_fields

import wioframework.utils as utils
import markdown



from actions import wideio_action, wideio_relaction


def wideio_moderated(moderated_by=lambda o: None,no_owner_assigns_to_moderator=True):
    # NEW STATE=S
    def dec(tc):
        if not hasattr(tc, "moderation_accept"):
            OWNER_FIELD=getattr(tc.WIDEIO_Meta,"OWNER_FIELD",None)
            
            def is_moderator(user, obj):
                from accounts.models import UserAccount
                moderator = moderated_by(obj)
                if moderator is None:
                    moderator = UserAccount.objects.filter(is_superuser=True)
                if isinstance(moderator,UserAccount):
                    moderator=[moderator]
                return (user in moderator) or user.is_staff
 
            
            #def see_draft_version(self, request):
            #    return HttpResponseRedirect(self.draft.get_view_url())
            #wideio_action(
            #    mimetype="text/html",
            #    possible=lambda o,
            #    r: (
            #        is_moderator(
            #            r.user,
            #            o) and o.wiostate == 'S'))(see_draft_version).contribute_to_class(tc)

            def moderation_accept(self, request):
                self.wiostate = 'V'

                owner=None
                if OWNER_FIELD:
                    if getattr(self,OWNER_FIELD):
                      owner=getattr(self,OWNER_FIELD)
                    else:
                      if no_owner_assigns_to_moderator:
                        setattr(self,OWNER_FIELD,request.user)
                        owner=request.user
                self.save()
                
                
                if owner:
                #  print 'moderation accept called'
                #  utils.wio_send_email(
                #    request,
                #    consent="moderation_accepted",
                #    subject='Your contribution has been published %s' %
                #    (str(self),
                #     ),
                #    template='mails/moderation_rep.html',
                #    context={
                #        'x': self ,
                #        'type': self.__class__.__name__ ,
                #        'response' : 'accepted'
                #        },
                #    to_users=[owner],
                #    url_notification=None)

                  recipient_email_list = []
                  recipient_email_list.append(owner.email)
                  subject = (self.name if hasattr(self,"name") else str(self))
                  utils.wio_send_email(
                     request,
                     consent="moderation_accept",
                     subject=subject,
                     template="mails/moderation_accepted.html",
                     context={
                         'x': self,
                         'type': self.ctrname_s or self.__class__.__name__,
                         'request': request
                     },
                     to_users=recipient_email_list)
            
            def moderation_reject(self, request):
                self.wiostate = 'U1'
                self.save()

                print 'moderation reject called -> returning as draft..'
                subject = (self.name if hasattr(self,"name") else str(self))                
                #message = "Your contribution on the " + self.get_model_name() + " " + self.name + \
                #        " has been accepted on the WIDE IO website. Thank you for your contribution."

                #wio_send_email(request, "moderation", subject, "generic/email_moderation.htmml", {'x':self}, [x.owner])

                owner=None
                if OWNER_FIELD:
                    if getattr(self,OWNER_FIELD):
                      owner=getattr(self,OWNER_FIELD)
                if owner:
                  recipient_email_list = []
                  recipient_email_list.append(owner.email)
                  subject = (self.name if hasattr(self,"name") else str(self))
                  utils.wio_send_email(
                     request,
                     consent="moderation_accept",
                     subject=subject,
                     template="mails/moderation_rejected.html",
                     context={
                         'x': self,
                         'type': self.ctrname_s or self.__class__.__name__,
                         'request': request
                     },
                     to_users=recipient_email_list)                
                #utils.wio_send_email(
                #    request,
                #    consent="moderation_accepted",
                #    subject='Your contribution has been published %s' %
                #    (str(self),
                #     ),
                #    template='generic/email_moderation_response.html',
                #    context={
                #        'x': self ,
                #        'type': self.__class__.__name__ ,
                #        'response': 'rejected'
                #        },
                #    to_users=[self.owner],
                #    url_notification=None)

            wideio_action(
                possible=lambda o, r: (
                    is_moderator(
                        r.user, o) and o.wiostate == "S"))(moderation_accept).contribute_to_class(tc)
                        

            wideio_action(
                possible=lambda o, r: (
                    is_moderator(
                        r.user, o) and o.wiostate == "S"))(moderation_reject).contribute_to_class(tc)                        
                        
                        
                        
            def request_moderation(self, request):
                from network.models import Notification, Announcement
                from accounts.models import UserAccount

                #    recipient_email_list = []
                #    recipient_email_list.append(self.owner.email)
                #    subject = (self.name if hasattr(self,"name") else str(self))
                #    utils.wio_send_email(
                #               request,
                #               consent="moderation_request",
                #               subject=subject,
                #               template="mails/moderation_req.html",
                #               context={
                #                 'x': self,
                #                 'type': self.ctrname_s or self.__class__.__name__,
                #                 'request': request
                #               },
                #               to_users=recipient_email_list)                                                            
                
                utils.wio_send_email(
                    request,
                    consent="moderation_requested",
                    subject='New object for moderation %s' %
                    (str(self),
                     ),
                    template='generic/email_gen_moderation.html',
                    context={
                        'x': self ,
                        'type': self.__class__.__name__ 
                        },
                    to_users=[
                        moderated_by(self)],
                    url_notification=None)

                target_users = moderated_by(self)
                if type(target_users)==list:
                    target_users=[target_users]
                    
                self.wiostate = 'S' # FOR SUBMITTED
                self.save()
                
                if target_users is None:
                    target_users = UserAccount.objects.filter(is_superuser=True)
                    
                if request.user in target_users:
                    for target_user in target_users:
                        ann = Announcement()
                        ann.content_object = self
                        kls = self.__class__
                        object_type = kls.WIDEIO_Meta.ctrname_s if (hasattr(kls,'WIDEIO_Meta') and hasattr(kls.WIDEIO_Meta,'ctrname_s')) else kls.__name__
                        ann.body = "New " + object_type + " to review."
                        ann.save()
                        n = Notification()
                        n.notified_user = target_user
                        n.content_object = ann # self
                        n.save()
                else:
                    moderation_accept(self, request)

            tc.request_moderation = request_moderation

            #~ #def on_update(self,request):
            #~ #self.request_moderation()
            #~ if (hasattr(tc,"on_update")):
            #~ on_update_orig = getattr(tc,"on_update")
            #~ def on_update(self,request):
            #~ on_update_orig(draft, request)
            #~ self.request_moderation()
            #~ setattr(tc,"on_update", on_update)

            #~ #def on_add(self,request):
            #~ #self.request_moderation()
            #~ if (hasattr(tc,"on_add")):
            #~ on_add_orig = getattr(tc,"on_add")
            #~ def on_add(self,request):
            #~ on_add_orig(draft, request)
            #~ self.request_moderation()
            #~ setattr(tc,"on_add", on_add)
            
            #
            # MODERATOR COMMENT CAN BE STORED IN _METADATA["moderator_comment"]
            # 
            
            if not hasattr(tc.WIDEIO_Meta, "default_query"):
                # default_query={'is_draft':False}
                from django.db.models import Q
                default_query = lambda r: (Q(wiostate='V') if r.user.is_anonymous() else (Q(wiostate='V') | Q(wiostate='S',owner=r.user)))
                setattr(
                    tc.WIDEIO_Meta,
                    "default_query",
                    staticmethod(default_query)
                    )
        return tc
    return dec
