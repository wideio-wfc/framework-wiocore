@wideio_name_invokable(add_fields_constructor={}, name_field="name")
class TestModel1(Model, object):
    name = StringField()
    len_char = StringField()
    #
    # class Meta:
    #     db = "testdb"


# basic invokable behaviour....
def test_invokable_provides_invoke_method():
    assert callable(TestModel1.invoke)


def test_invokable_provides_invoke_allows_to_create_new_instance():
    NEWNAME = "newname"
    assert (TestModel1.objects.filter(name=NEWNAME).count() == 0)
    i = TestModel1.invoke(NEWNAME)
    assert isinstance(i, TestModel1)
    assert (TestModel1.objects.filter(name=NEWNAME).count() == 1)


def test_invokable_provides_invoke_allows_to_create_only_one_instance():
    NEWNAME = "newname"
    assert (TestModel1.objects.filter(name=NEWNAME).count() == 0)
    i = TestModel1.invoke(NEWNAME)
    j = TestModel1.invoke(NEWNAME)
    assert isinstance(i, TestModel1)
    assert i == j
    assert (TestModel1.objects.filter(name=NEWNAME).count() == 1)

# invokable allows initialise fields
