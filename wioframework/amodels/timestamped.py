# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################

"""
AMODEL : ADVANCED MODELS

This contains the decorator for components the components of WIDE IO
"""
import datetime


import django.db.models as omodels
from django.db.models.fields.related import RelatedField

from django.http import Http404
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.utils.html import escape

from django.core.exceptions import ValidationError

from django.conf import settings

import wioframework.fields as models

from wioframework.atomic_ops import atomic_incr
from wioframework import decorators as dec
from wioframework.decorators import PermissionException
from wioframework.decorators import permission_based
import wioframework.fields as lib_fields

import wioframework.utils as utils
import markdown



def wideio_timestamped(tc):
    """
    Add the default fields to be sure that every entry is timestamped.
    """
    if not hasattr(tc, "_created_at"):
        created_at = models.DateTimeField(
            auto_now=False,
            auto_now_add=True,
            db_index=True,
            null=True)   #
        created_at.contribute_to_class(tc, "created_at")
        updated_at = models.DateTimeField(
            auto_now=True,
            auto_now_add=False,
            db_index=True,
            null=True)   #
        updated_at.contribute_to_class(tc, "updated_at")
        setattr(tc, "_created_at", None)
    if not hasattr(tc.WIDEIO_Meta, "form_exclude"):
        tc.WIDEIO_Meta.form_exclude = ["created_at", "updated_at"]
    else:
        tc.WIDEIO_Meta.form_exclude.extend(["created_at", "updated_at"])
    if not hasattr(tc.WIDEIO_Meta, "sort_enabled"):
        tc.WIDEIO_Meta.sort_enabled = []
    for f in ['created_at', 'updated_at']:
        if not f in tc.WIDEIO_Meta.sort_enabled:
            tc.WIDEIO_Meta.sort_enabled.append(f)

    return tc

# TODO: add catching errors
def wideio_timestamped_autoexpiring(
        expire=None, delete_database_entry_on_expire=False, *xargs, **xxargs):
    """timestamp decorator, adds fields created_at, updated_at, expires_at
        expire -- decorator will set default expiration date as object creation + expire (timedelta object)

        note : it will add 2 functions : set_expire(timedelta) and fix(), where fix will make object pernament
    """
    def dec(tc):
        if (not hasattr(tc, "_created_at")):
            created_at = models.DateTimeField(
                auto_now=False,
                auto_now_add=True,
                db_index=True,
                null=True)
            created_at.contribute_to_class(tc, "created_at")
            updated_at = models.DateTimeField(
                auto_now=True,
                auto_now_add=False,
                db_index=True,
                null=True)
            updated_at.contribute_to_class(tc, "updated_at")
            setattr(tc, "_created_at", None)
            # branch on default
        # TODO: check for expires_at to track errors
        if(not hasattr(tc, "_expires_at")):
            if expire:
                # add fields
                expires_at = models.DateTimeField(
                    default=lambda: (
                        datetime.datetime.now() +
                        expire),
                    db_index=True,
                    null=True)
                expires_at.contribute_to_class(tc, "expires_at")
            else:
                # add fields
                expires_at = models.DateTimeField(
                    default=None,
                    db_index=True,
                    null=True)  # TODO: set to none
                expires_at.contribute_to_class(tc, "expires_at")
            # add methods, TODO: make separate class, creating functions

            if delete_database_entry_on_expire:
                setattr(
                    tc.WIDEIO_Meta,
                    "delete_database_entry_on_expire",
                    True)

            # !! Exclude from forms !! #
            if (not hasattr(tc.WIDEIO_Meta, "form_exclude")):
                tc.WIDEIO_Meta.form_exclude = []
            for f in ['expires_at', 'created_at', 'updated_at']:
                if not f in tc.WIDEIO_Meta.form_exclude:
                    tc.WIDEIO_Meta.form_exclude.append(f)

            setattr(
                tc,
                "_expires_at",
                None)  # just to make some python magic possible (in cron job for instance)

            def set_expire(self, expire):
                self.expires_at = datetime.datetime.now() + expire
                self.save()  # very important

            def fix(self):
                self.expires_at = None
                self.save()  # very important
            tc.set_expire = set_expire
            tc.fix = fix
        if not hasattr(tc.WIDEIO_Meta, "sort_enabled"):
            tc.WIDEIO_Meta.sort_enabled = []
        for f in ['created_at', 'updated_at', "expires_at"]:
            if f not in tc.WIDEIO_Meta.sort_enabled:
                tc.WIDEIO_Meta.sort_enabled.append(f)

        return tc
    return dec

