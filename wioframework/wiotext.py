# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
import re
import uuid
import requests
import markdown

from django.utils.safestring import mark_safe
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic

from mml import transductor

from references.models import BibliographicReference, GenericReference, Image, LinkReference
from functools import reduce


# TODO: Objectivise this file
def create_url_reference(g):
    # url.replace(' ','') #< if spaces where allowed  for bug tolerance then
    # remove them
    url = g.group(0)
    #r = requests.head(url)
    # if 'content-type' in r.headers and 'image' in r.headers['content-type']:
    #    pass
    if (url.split(".")[-1].lower() in ["png", "jpg", "jpeg", "gif"]):
        return '<//Image "%s"//>' % url
    elif ("wide.io" in url) and "/view/" in url:
        g = re.match(
            "http(s?)://([a-zA-Z0-9_.-]*)wide.io(([:0-9]+)?)/([a-zA-Z0-9_.-]*)/([a-zA-Z0-9:_.-]*)/view/([a-zA-Z0-9:_.-]*)((/)?)",
            url)
        #  4
        grps = g.groups()
        gr = GenericReference()
        module = grps[4]
        modelname = grps[5]
        key = grps[6]
        ct = ContentType.objects.get(model=modelname)
        gr.gpk_pk = key
        gr.gpk_type = ct
        gr.title = url
        # gr.link = ke#LinkReference.create(gr.id,url)
        gr.save()

        return '<//GenericReference "%s"//>' % gr.id
    else:
        lr = LinkReference.create(url, url)
        return '<//LinkReference "%s"//>' % lr.id
    return


def create_twitter_reference(g):
    # TODO: NOT SURE ABOUT THIS ... LINKS ARE MORE FOR DOCUMENTS...
    twitter_username = g.group(0)
    url = "//twitter.com/" + twitter_username.replace('@', '')
    lr = LinkReference.create(twitter_username, url)
    return '<//LinkReference "%s"//>' % lr.id


def create_dblp_reference(g):
    dblp_url = g.group(0).replace('DBLP:', '')
    r = requests.get('http://dblp.uni-trier.de/rec/bibtex/%s' % dblp_url)
    bibtex = r.text
    bibtex = re.search(
        '<pre>[\s\S]*?</pre>',
        bibtex).group(0).replace(
        '<pre>',
        '').replace(
            '</pre>',
        '')
    ref = BibliographicReference(
        bibtex=unicode(bibtex),
        title=dblp_url)  # TODO: try to do a search first
    ref.save()
    return '<//BibliographicReference "%s"//>' % ref.id


def create_doi_reference(g):
    doi_url = g.group(0).replace('doi:', '')
    headers = {"Accept": "text/bibliography; style=bibtex"}
    r = requests.get('http://dx.doi.org/%s' % doi_url, headers=headers)
    bibtex = r.text
    ref = BibliographicReference(
        bibtex=unicode(bibtex),
        title=doi_url)  # TODO: try do a search first
    ref.save()

    gref = GenericReference()
    gref.title = doi_url
    gref.link = ref
    gref.save()

    return '<//GenericReference "%s"//>' % gref.id

references = (
    (create_url_reference, re.compile(
        '(http|https|ftp|git)\://[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(:[a-zA-Z0-9]*)?/?([a-zA-Z0-9:\-\._\?\,\'/\\\+&amp;%\$#\=~])*')),
    (create_twitter_reference, re.compile(
        "(?<=^|(?<=[^a-zA-Z0-9-_\.]))@([A-Za-z]+[A-Za-z0-9]+)")),
    (create_dblp_reference, re.compile("DBLP:([a-zA-Z0-9/]+)")),
    (create_doi_reference, re.compile(r"doi:([a-zA-Z0-9/.\-]+)"))
)
tag_regexp = re.compile('<//(\s?)([A-Za-z]+) "([^"]+)"(\s?)//>')

_striphtml = re.compile(r'<.*?>')


def striphtml(data):
    return _striphtml.sub('', data)


T2 = {'a': [
    (u"\[http://([^]]+)\]\(([^)]+)\)",
     lambda g:(g.group(1)[1:-1] + g.group(2))),
],
}
CT2 = transductor.Transductor(T2)


def striphtml_but_atags(data):
    if isinstance(data, str):
        data = data.decode('utf8')
    data = CT2.process(data)
    c = re.subn(
        r'<a(.*)href="(.*)"(.*)>',
        lambda x: " %s " %
        (str(
            x[2]),
         ),
        data)[0]
    return _striphtml.sub('', c)


def wiotext_pre(text):
    """
    Extract references from the text based on specified functions and references.
    """
    if text is None:
        return None
    final_string = ""
    text = striphtml_but_atags(text)
    tag_matches = list(tag_regexp.finditer(text))
    l = reduce(lambda b,
               x: b + [x.start(),
                       x.end()],
               tag_matches,
               [0]) + [len(text)]
    # iterate through all element that are not tags.
    for li in range(0, len(l) - 1, 2):
        subtext = text[l[li]:l[li + 1]]
        for function, regexp in references:
            subtext = regexp.sub(function, subtext)
        final_string += subtext
        if li / 2 < len(tag_matches):
            match_obj = tag_matches[li / 2]
            final_string += text[match_obj.start():match_obj.end()]
    return final_string


def shorten_if_too_long(s, maxl=40):
    if (len(s) > maxl):
        return s[:int(maxl / 2) - 3] + "(...)" + s[-(int(maxl / 2) + 2):]
    return s


def wiotextr(g):
    """
    Extract internal references from a block of text
    """
    ref_type = g.group(2).strip()
    ref_content = g.group(3)
    try:
        if ref_type.lower() == 'genericreference':
            gr1 = [GenericReference.objects.get(id=ref_content)]
            return gr1.id
        elif ref_type.lower() == 'internalreference':
            lr1 = [GenericReference.objects.get(id=ref_content).link]
            return lr1
        elif ref_type.lower() == 'linkreference':
            lr1 = [LinkReference.objects.get(id=ref_content)]
            return lr1
        elif ref_type.lower() == 'bibliographicreference':
            biblio_ref = BibliographicReference.objects.get(id=ref_content)
            return [biblio_ref]
    except:
        pass
    return []


def wiotextf(g, request, mode="std"):
    """
    Formats a tag
    """
    ref_type = g.group(2).strip()
    ref_content = g.group(3)
    try:
        if ref_type.lower() == 'genericreference':
            gr1 = GenericReference.objects.get(id=ref_content)
            return mark_safe(gr1.link.html(request, mode))
        elif ref_type.lower() == 'internalreference':
            lr1 = GenericReference.objects.get(id=ref_content).link
            return mark_safe('<a href="%s">WideIO:%s</a>' %
                             (lr1.link.get_action_url('follow'), lr1.title))
        elif ref_type.lower() == 'linkreference':
            lr1 = LinkReference.objects.get(id=ref_content)
            return mark_safe('<div class="wio-reference-link"><a href="%s" data-tip="%s" target="wioext">%s</a></div>' %
                             (lr1.link.get_action_url('follow'), lr1.title, shorten_if_too_long(lr1.title)))
        elif ref_type.lower() == 'bibliographicreference':
            biblio_ref = BibliographicReference.objects.get(id=ref_content)
            return mark_safe('<span class="reference">%s<i class="icon-user" title="Authors: %s"></i><a href="http://dblp.uni-trier.de/rec/bibtex/%s"><i class="icon-tasks"></i></a></span>'
                             % (biblio_ref.get_title(), unicode(' & '.join(biblio_ref.get_author_names())), biblio_ref.title))
        elif ref_type.lower() == 'image':
            return mark_safe(
                '<a href="%s"><img class="embedded-image" src="%s" /></a>' % (ref_content, ref_content))
        elif ref_type.lower() == 'youtube':
            return mark_safe(
                '<div class="youtube-player" data-src="%s"></div>' % ref_content)
        return "[ UNSUPPORTED WIO TEXT TAG ]"
    except:
        return "[ BROKEN REFERENCE ]"


T1 = {'a': [
    (u"([0-9]+)([ \t]*)([.:])([ \t]*)\n", lambda g:g.group(1) + ". ", "a",),
    (u"\n", None, "a"),
    (u".", None, "b")
],
    'b': [("\n", None, "a")]
}
CT1 = transductor.Transductor(T1)


def _wiotext(value, request, mode="mini"):
    """
    DJANGO filter that converts a block of wiotext to HTML
    """
    if value is None:
        return ""
    s = mark_safe("")
    oval = value
    try:
        if (type(value)) in [str]:
            value = value.decode('utf8')
        value = CT1.process(value)
        value = markdown.Markdown().convert(value)
    except Exception as e:
        value = str(str(e).replace('<', '[')) + str(type(value)) + oval

    rg = list(re.finditer('<//(\s?)([A-Za-z]+) "([^"]+)"(\s?)//>', value))
    l = reduce(lambda b, x: b + [x.start(), x.end()], rg, [0]) + [len(value)]
    for li in range(0, len(l), 2):
        s += value[l[li]:l[li + 1]]
        if li + 2 < len(l):
            s += wiotextf(rg[li / 2], request, mode)
    return mark_safe(s)


def wiotext_refs(value):
    refs = []
    if value is None:
        return []
    rg = list(re.finditer('<//(\s?)([A-Za-z]+) "([^"]+)"(\s?)//>', value))
    l = reduce(lambda b, x: b + [x.start(), x.end()], rg, [0]) + [len(value)]
    for li in range(0, len(l), 2):
        if li + 2 < len(l):
            refs += wiotextr(rg[li / 2])
    return refs
