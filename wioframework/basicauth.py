# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# BASIC HTTPS AUTH IS USED ONE OF THE WAY OF DOING AJAX REQUEST
from django.conf import settings
from django.http import HttpResponse
#from django.views.debug import technical_500_response
from django.contrib.auth import authenticate, login, logout
from accounts.models import UserAccount

def basic_challenge(realm=None):
    if realm is None:
        realm = getattr(
            settings,
            'WWW_AUTHENTICATION_REALM',
            (('Restricted Access')))
        # TODO: Make a nice template for a 401 message?
        response = HttpResponse(
            (('Authorization Required')),
            content_type="text/plain")
        response['WWW-Authenticate'] = 'Basic realm="%s"' % (realm)
        response.status_code = 401
        return response


def basic_authenticate(authentication):
    (authmeth, auth) = authentication.split(' ', 1)
    if 'basic' != authmeth.lower():
        return None
    auth = auth.strip().decode('base64')
    username, password = auth.split(':', 1)
    username=username.replace('__atmark__','@') #< QUICKPATCH a replacement for __atmark__ in urls < FIXME: ULTIMATELY NOT RECOMMENDED
    if len(password)>42:
       ul=UserAccount.objects.filter(username=username)
       if ul.count()==1:
           u0=ul[0]
           if u0.get_extended_profile().api_key==password:
               u0.backend=None
               return u0       
       return None 
    else:
      return authenticate(username=username, password=password)


class BasicAuthenticationMiddleware:
    """
    Allow authentication through HTTP Basic Auth
    """

    def _is_secure(self, request):
        if request.is_secure():
            return True
        # Handle the Webfaction case until this gets resolved in the
        # request.is_secure()
        if 'HTTP_X_FORWARDED_SSL' in request.META:
            return request.META['HTTP_X_FORWARDED_SSL'] == 'on'
        if 'REMOTE_ADDR' in request.META:
            return (request.META['REMOTE_ADDR'] == '127.0.0.1') or (
                settings.DEBUG and request.META['REMOTE_ADDR'].startswith("192.168"))
        return False

    def process_request(self, request):
        import time
        request.clock = time.clock()
        request.time = time.time()
        request.is_request_ajax = "_AJAX" in request.REQUEST and request.REQUEST["_AJAX"]
            
        #if hasattr(request, "user") and request.user is not None and request.user.is_superuser and "_SU" in request.GET:
        #        request.user = request.user.__class__.objects.get(username=request.GET("_SU"))        
        
        if (not self._is_secure(request)):
            # assert(0)
            #print "HTTP_AUTH UNSECURE",request.META['PATH_INFO']
            return None  # don't tolerate clear text passwords....

        if 'HTTP_AUTHORIZATION' not in request.META:
            # If the user out of the session as well
         #           logout(request)
         #           return HttpResponse(str(request))
         #           raise Exception,"out of session"
            #print "HTTP_AUTH NOT SET" ,request.META['PATH_INFO']
            if ('WITH_HTTP_AUTH' in request.POST):
                return basic_challenge()
            return None
        user = basic_authenticate(request.META['HTTP_AUTHORIZATION'])
        if user is None:
            return basic_challenge()
        else:
            login(request, user)
            # request.user=user
            request.user=user
            request._cached_user=user            
            request.csrf_processing_done = True
            #return request


class SUAuthenticationMiddleware:
    """
    Allow superusers and developers to simulate other users.
    """
    def process_request(self, request):
        if hasattr(request, "user") and request.user is not None and request.user.is_superuser and "_SU" in request.GET:
                u=request.user.__class__.objects.filter(username=request.GET["_SU"])
                if u.count()==1:
                  request.user =u[0]
                  return

                u=request.user.__class__.objects.filter(email__startswith=request.GET["_SU"])
                if u.count()==1:
                  request.user =u[0]
                  return               
