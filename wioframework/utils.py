# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
import sys
import re
import os
import uuid

from PIL import Image

from mml import transductor
import PIL.ImageOps


def image_to_ascii(path,width=60,height=30):
    import aalib
    screen = aalib.AsciiScreen(width=width, height=height)
    image = Image.open(path)
    image = image.convert('L')
    #image = PIL.ImageOps.invert(image)
    image = image.resize(screen.virtual_size)
    screen.put_image((0, 0), image)
    r = screen.render()
    return r


def image_to_html_ascii(path,width=24,height=16):
    import aalib
    screen = aalib.AsciiScreen(width=width, height=height)
    image = Image.open(path)
    image = image.convert('L')
    #image = PIL.ImageOps.invert(image)
    image = image.resize(screen.virtual_size)
    screen.put_image((0, 0), image)
    r = screen.render().replace('Q',' ')
    im = Image.open(path)
    im = im.convert('RGB')
    im = im.resize((width,height))    
    pixels = list(im.getdata())
    width, height = im.size
    itpixels = iter(pixels)
    rl = "<pre style=\"font-family:courier; text-decoration: none !important;\">\n"
    for y,l in zip(range(height),r.split("\n")):
        cl = ""
        for x,c in zip(range(width),l):
            pr,pg,pb=itpixels.next()
            if pr==0 and pg==0 and pb==0: 
                pr=255 
                pg=255 
                pb=255
            else:
                bc=(pr+pb+pg)/3
                if bc<250:
                        bc2=bc/4
                        pr-=bc2
                        pg-=bc2
                        pb-=bc2
                        pr=max(0,pr)
                        pg=max(0,pg)
                        pb=max(0,pb)
            cl += '<span style="color:#%02x%02x%02x;">%s</span>' % (
                pr,pg,pb,("_" if c==" " else c))
        cl += "\n"
        rl += cl
    rl += "</pre>\n"        
    return rl


def image_to_divs(path):
    im = Image.open(path)
    im = im.convert('RGB')
    im = im.resize((24, 24))
    pixels = list(im.getdata())
    width, height = im.size
    itpixels = iter(pixels)
    #rl="<style>px {height:2px; width:2px; margin:0 0 0 0; padding: 0 0 0 0;}</style><div>"
    rl = "<div>\n"
    for y in range(height):
        cl = "<div>\n"
        for x in range(width):
            cl += '<span style="background-color:#%02x%02x%02x; width:6px; height:6px;max-height:6px;padding:3px;display: inline-block;"></span>' % (
                itpixels.next())
        cl += "</div>\n"
        rl += cl
    rl += "</div>\n"
    return rl


def extract_src(t):
    return re.match("(.*)src=\"([^\"]+)\"(.*)", t).group(2)

HI = ["#", "=", "~", "-", "_", "."]


T1 = {'a': [
    (u"<IMG ([^>]+)>", lambda g:"\n" +
     image_to_ascii(extract_src(g.group(1))) +
     "\n"),
    (u"<p([^>]*)>", "\n"),
    (u"<H([1-6])>([^<>]+)</H([1-6])>", lambda g:unicode(g.group(2)
                                                        ).upper() + "\n" + HI[int(c.group(1)) - 1] * len(cgroup(2)) + "\n"),
    (u"<br([^>]*)>", "\n"),
    (u"<hr([^>]*)>", "-" * 80 + "\n"),
    (u"<style>", "", "c"),
    (u"<script([^>]*)>", "", "d"),
    (u"<", "", "b",),
],
    'b': [
    (u">", "", "a",),
    (".", ""),
],
    'c': [
    (u"</style>", "", "a",),
    (".", ""),
],
    'd': [
    (u"</script>", "", "a",),
    (".", ""),
]

}

CT1 = transductor.Transductor(T1)


def fancy_html_to_text(x):
    return CT1.process(x, 128)


class _Rewriter1:

    def __init__(self):
        self.c = 0
        self.RL = []

    def rewrite_image(self, g, img):
        def read():
            return open(img, "rb").read()
        if img.startswith("http://"):
            return g.group(0)
        else:
            cid = "img" + str(uuid.uuid1()) + "@wide.io"
            fn = "img-%d.%s" % (self.c, img.split(".")[-1])
            self.RL.append((fn, img, cid, read))
            self.c += 1
            return g.group(0).replace(img, "cid:" + cid)


class _Rewriter2:

    def __init__(self):
        self.c = 0
        self.RL = []

    def rewrite_image(self, g, img):
        def read():
            cb = """<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">"""
            x = open(img, "rb").read()
            cb += '<image width="100" height="100" xlink:href="data:image/png;base64,' + \
                base65.b64encode(x) + '">'
            cb += "</svg>"
            return cv
        if img.startswith("http://"):
            return g.group(0)
        else:
            cid = "img" + str(uuid.uuid1()) + "@wide.io"
            fn = "img-%d.%s" % (self.c, "svg")
            self.RL.append((fn, img, cid, read_svg))
            self.c += 1
            return g.group(0).replace(img, "cid:" + cid)


def fancy_html_to_html_and_images(x):
    R = _Rewriter1()
    T2 = {'a': [
        (u"<IMG ([^>]+)>",
         lambda g:R.rewrite_image(g,
                                  extract_src(g.group(1)))),
    ],
    }
    CT2 = transductor.Transductor(T2)
    return (CT2.process(x, 128), R.RL)


T3 = {'a': [
    (u"<IMG ([^>]+)>", lambda g:image_to_html_ascii(extract_src(g.group(1)))),
],
}
CT3 = transductor.Transductor(T3)


def fancy_html_to_html_and_divimages(x):
    return CT3.process(x, 128)


def wio_send_email(request, consent, subject, template, context, to_users,
                   url_notification=None, from_email="no-reply@wide.io", embed_images=True, single_email=False):
    from django.template.loader import render_to_string
    from django.core.mail import EmailMultiAlternatives
    import settings
    ccontext = {
        'to_users': to_users,
        'base': 'bases/email.html',        
        'request': request,
        'settings': settings}
    ccontext.update(context)
    if (not settings.PRODUCTION_MODE) or (settings.DEBUG):
        # OK WE ARE TESTING
        if (request!=None) and request.user.is_staff and  ((request.user.email!=None) and  len(request.user.email)):
            to = [request.user.email]
            tou= [request.user]
        else:
            to = map(lambda x: x[1], settings.ADMINS)
            tou = map(lambda x:(lambda q:(q[0] if q.count() else None)(UserAccount.filter(email=x))),to)
    else:
        to = []
        tou = []
        for t in to_users:
            if type(t) in [str, unicode]:
                if not(t) or not len(t):
                        raise Exception,"Invalid email address"
                to.append(t)
                tou.append((lambda q:(q[0] if q.count() else None)(UserAccount.filter(email=t))))
            else:
              if t==None:
                print "unexpected -  user=None in wio email send"
              else:
                if (not hasattr(t.get_notification_options(), (consent if consent else "other_email_notifications"))) or getattr(
                        t.notification_options, (consent if consent else "other_email_notifications")):
                    if not(t.email) or not len(t.email):
                        raise Exception,"Invalid email address"
                    to.append(t.email)
                    tou.append(t)
    if len(to):
        if single_email:
           to_list=[zip(to,tou)] # a list of that contains lists of tuples (email,user) (email,user)
        else:
           to_list=map(lambda x:[x],zip(to,tou))
        for cto in to_list:
                cto_emails=map(lambda x:x[0],cto)
                cto_users=map(lambda x:x[1],cto)
                ccontext['to_users']=cto_users
                ccontext['to_emails']=cto_emails
                ccontext['to_user']=cto_users[0]
                ccontext['to_email']=cto_emails[0]

                s = render_to_string(template, ccontext)                   
                text_content = fancy_html_to_text(s)
                if embed_images:
                    # html_content,images=fancy_html_to_html_and_divimages(s)
                    html_content, images = fancy_html_to_html_and_divimages(s), None
                else:
                    html_content, images = s, None

                msg = EmailMultiAlternatives(subject, text_content, from_email, cto_emails)

                msg.attach_alternative(html_content, "text/html")
                MIMETYPES = {
                    'png': 'image/png',
                    'jpg': 'image/jpeg',
                }
                if images is not None:
                    for i in images:
                        # msg.attach(i[0],"","")open(i[1],"rb").read(),MIMETYPES[i[0].split(".")[-1]])
                        a = msg._create_mime_attachment(
                            open(i[1], "rb").read(), MIMETYPES[i[0].split(".")[-1]])
                        a.add_header('Content-ID', '<' + i[2] + '>')
                        msg.attachments.append(a)
                from backoffice.lib import log_info
                try:
                    for t in cto_emails:
                        log_info("Sending message %s to %s" % (subject, to))
                except Exception,e:
                    if request:
                      from django.contrib import messages
                      messages.add_message(request, messages.INFO, 'Failed to create log')
                    else:
                      print e
                msg.send()

    if (url_notification is not None):
        from network.models import Announcement
        for t in to_users:
            a = Announcement.create()


if __name__ == "__main__":
    print image_to_ascii(sys.argv[1])    
    sys.exit(0)    
    print image_to_divs(sys.argv[1])    
    print fancy_html_to_text("<html> hello <img src=\"" + sys.argv[1] + "\"> world </html>")
    print fancy_html_to_html_and_images("<html> hello <img src=\"" + sys.argv[1] + "\"> world </html>")
