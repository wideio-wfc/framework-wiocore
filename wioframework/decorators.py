# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# DJANGO uses the shortcut framework which I consider slow and buggy


from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect
from datetime import datetime, time
from django.contrib import messages
from wioframework.jsonresponse import JsonResponse


class PermissionException(Exception):

    def __init__(self, text=None):
        self.txt = text

    def __str__(self):
        return "WIDE IO Permission Exception : " + \
            (self.txt if self.txt else "N/A")


def redirect_path_to_login(path):
    from django.contrib.auth import REDIRECT_FIELD_NAME
    from django.contrib.auth.views import redirect_to_login
    return redirect_to_login(path, "/accounts/login", REDIRECT_FIELD_NAME)


def login_required(pv):
    def lv(request, *args, **xargs):
        if not request.user.is_authenticated():
            if request.REQUEST.get("_JSON") or request.META.get("Content-Type")=="application/json":
                return JsonResponse({"error": "Permission denied"},status=403)
            if "_AJAX" in request.REQUEST:
                return HttpResponse('"You need to be authenticated to access this view"',status=403)
            messages.add_message(request,messages.WARNING,'You need to be logged in to access this page.')
            path = request.build_absolute_uri()
            return redirect_path_to_login(path)
        
        if request.user.wiostate == 'CP':
            if not request.META.get('PATH_INFO','').startswith('/accounts/password/'):
                return HttpResponseRedirect('/accounts/password/change/')
        
        #messages.add_message(request, messages.INFO, 'Login accepted.')
        return pv(request, *args, **xargs)
    
    if (hasattr(pv, "raw")):
        lv.raw = login_required(pv.raw)
    return lv


def user_passes_test(t):
    def upv(pv):
        def lv(request, *args, **xargs):
            if (not request.user.is_authenticated()) or (not t(request.user)):
                if "_JSON" in request.REQUEST and request.REQUEST["_JSON"]:
                    return JsonResponse({"error": "Permission denied"},status=403)
                messages.add_message(
                    request,
                    messages.WARNING,
                    'Your credentials do not allow you to execute the action you tried to perform.')
                path = request.build_absolute_uri()
                return redirect_path_to_login(path)
            if request.user.wiostate == 'CP':
                if not request.META.get('PATH_INFO','').startswith(
                        '/accounts/password/'):
                    return HttpResponseRedirect('/accounts/password/change/')
            try:
                return pv(request, *args, **xargs)
            except PermissionException:  # Duplicate code ^
                if "_JSON" in request.REQUEST and request.REQUEST["_JSON"]:
                    return JsonResponse({"error": "Permission denied"})
                messages.add_message(
                    request,
                    messages.WARNING,
                    'Your credentials do not allow you to execute the action you tried to perform.')
                path = request.build_absolute_uri()
                return redirect_path_to_login(path)
        if (hasattr(pv, "raw")):
            lv.raw = user_passes_test(t)(pv.raw)
        return lv

    return upv


def permission_based(pv):
    def lv(request, *args, **xargs):
        try:
            return pv(request, *args, **xargs)
        except PermissionException:
            if "_JSON" in request.REQUEST and request.REQUEST["_JSON"]:
                return JsonResponse({"error": "Permission denied"})
            if "_AJAX" in request.REQUEST:
                # TODO until redirect_to_login can be fixed for _AJAX, better
                # to not redirect
                return HttpResponse(
                    "You need to be authenticated to access this view")
            messages.add_message(
                request,
                messages.WARNING,
                'Your credentials do not allow you to execute the action you tried to perform.')
            path = request.build_absolute_uri()
            return redirect_path_to_login(path)
    if (hasattr(pv, "raw")):
        lv.raw = permission_based(pv.raw)
    return lv


def all_permissions_from_request(fr):
    return {
        "add": fr,
        #"update":also_check_method("CanWrite",lambda o,r:fr(r)),
        "update": (lambda o, r: fr(r)),
        #"delete":also_check_method("CanWrite",lambda o,r:fr(r)),
        "delete": (lambda o, r: fr(r)),
        "view": (lambda o, r: fr(r)),
        "list": fr
    }


def all_permissions_from_request_rwa(frr, frw, fra):
    return {
        "add": fra,
        #"update":also_check_method("CanWrite",lambda o,r:frw(r)),
        #"delete":also_check_method("CanWrite",lambda o,r:frw(r)),
        "update": (lambda o, r: frw(r)),
        "delete": (lambda o, r: frw(r)),
        "view": (lambda o, r: frr(r)),
        "list": frr
    }


perm_for_admin_only = all_permissions_from_request(
    lambda r: r.user.is_superuser)
perm_for_staff_only = all_permissions_from_request(lambda r: r.user.is_staff)
perm_for_logged_users_only = all_permissions_from_request(
    lambda r: r.user is not None)

perm_write_for_admin_only = all_permissions_from_request_rwa(
    lambda r: r.user is not None,
    lambda r: r.user.is_superuser,
    lambda r: r.user.is_superuser)
perm_write_for_staff_only = all_permissions_from_request_rwa(
    lambda r: r.user is not None,
    lambda r: r.user.is_staff,
    lambda r: r.user.is_staff)
perm_write_for_logged_users_only = all_permissions_from_request_rwa(
    lambda r: r.user is not None,
    lambda r: r.user is not None,
    lambda r: r.user is not None)

perm_read_logged_users_write_for_admin_only = all_permissions_from_request_rwa(
    lambda r: r.user is not None,
    lambda r: r.user.is_superuser,
    lambda r: r.user.is_superuser)
perm_read_logged_users_write_for_staff_only = all_permissions_from_request_rwa(
    lambda r: r.user is not None,
    lambda r: r.user.is_staff,
    lambda r: r.user.is_staff)

perm_777 = all_permissions_from_request(lambda r: True)

for_admin_only = [
    ('all_decorators',
     [permission_based]),
    ('all_permissions',
     perm_for_admin_only)]
for_staff_only = [
    ('all_decorators',
     [permission_based]),
    ('all_permissions',
     perm_for_staff_only)]
for_logged_users_only = [
    ('all_decorators',
     [permission_based]),
    ('all_permissions',
     perm_for_logged_users_only)]

read_all_write_admin_only = [
    ('all_decorators',
     [permission_based]),
    ('all_permissions',
     perm_write_for_admin_only)]
read_all_write_staff_only = [
    ('all_decorators',
     [permission_based]),
    ('all_permissions',
     perm_write_for_staff_only)]
read_all_write_logged_users_only = [
    ('all_decorators',
     [permission_based]),
    ('all_permissions',
     perm_write_for_logged_users_only)]

read_logged_users_write_admin_only = [
    ('all_decorators',
     [permission_based]),
    ('all_permissions',
     perm_read_logged_users_write_for_admin_only)]
read_logged_users_write_staff_only = [
    ('all_decorators',
     [permission_based]),
    ('all_permissions',
     perm_read_logged_users_write_for_staff_only)]

