# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
#
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
#
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- coding: utf-8 -*-
import re
import uuid
import logging

from django.template import Node
from django.template import Variable
import settings
from wioframework.tagslib import register_tag
from django.http import Http404
from urlparse import urlparse
from django.core.urlresolvers import resolve
from django.utils.html import escape


#TODO: add logging for failing partial

@register_tag()
def embed_request(parser, token):
    bits = token.split_contents()

    class TNode(Node):
        def render(self, context):
            realrequest = (Variable(bits[1])).resolve(context)
            next = (Variable(bits[2])).resolve(context)

            class MetaRequest:

                def method(self):
                    print "Length of bits", len(bits)
                    return (("POST" if len(bits) == 4 else "GET") if len(
                        bits <= 4) else Variable(bits[4]).resolve(context))
                DPOST = {
                    '_AJAX': 1} if len(bits) < 4 else Variable(
                    bits[3]).resolve(context)
                DREQUEST = {
                    '_AJAX': 1} if len(bits) < 4 else Variable(
                    bits[3]).resolve(context)
                DGET = {
                    '_AJAX': 1} if len(bits) < 4 else Variable(
                    bits[3]).resolve(context)
                POST = dict(
                    {
                        '_AJAX': 1,
                        '_STAFF_DEBUG': realrequest.REQUEST.get(
                            "_STAFF_DEBUG",
                            "")} if len(bits) < 4 else Variable(
                        bits[3]).resolve(context))
                REQUEST = dict(
                    {
                        '_AJAX': 1,
                        '_STAFF_DEBUG': realrequest.REQUEST.get(
                            "_STAFF_DEBUG",
                            "")} if len(bits) < 4 else Variable(
                        bits[3]).resolve(context))
                GET = dict(
                    {
                        '_AJAX': 1,
                        '_STAFF_DEBUG': realrequest.REQUEST.get(
                            "_STAFF_DEBUG",
                            "")} if len(bits) < 4 else Variable(
                        bits[3]).resolve(context))
                path = next
                user = realrequest.user
                tenant = getattr(realrequest,"tenant",None)
                META = realrequest.META.copy()
                COOKIES = realrequest.COOKIES
                INTERNAL = True
                is_request_ajax = True
                build_absolute_uri = realrequest.build_absolute_uri
            request = MetaRequest()
            from django.contrib.messages.storage import default_storage
            request._messages = default_storage(request)

            try:
                view, args, kwargs = resolve(urlparse(next)[2])
            except:
                return 'URL Resolution Error : ' + str(urlparse(next)[2])

            args = [request] + list(args)
            try:
                return view(*args, **kwargs).content
            except Http404:
                logging.error("Error in HTML component  %r %r %r)" % (
                    bits, args, kwargs))
                if settings.DEBUG:
                    return '[Error with a HTML component %r %r %r]' % (
                    bits, args, kwargs)
                else:
                    return "[Internal Error]"
    return TNode()



@register_tag()
def embed_ajax(parser, token):
    bits = token.split_contents()

    class TNode(Node):
        def render(self, context):
            from urlparse import urlparse
            from django.core.urlresolvers import resolve
            formuuid = str(uuid.uuid1()).replace("-", "")
            realrequest = (Variable(bits[1])).resolve(context)
            next = (Variable(bits[2])).resolve(context)
            xvars = None
            kwargsupdt = {}
            is_post = False
            if (len(bits) >= 4):
                if (len(bits[3]) > 0) and (bits[3] not in [u'""', u"''"]):
                    xvars = (Variable(bits[3]).resolve(context))
                    if(not isinstance(xvars, dict)):
                        xvars = unicode(xvars)
                        if (xvars[0] == '$'):
                            xvars = re.sub(
                                r"\{\{([^}]+)\}\}",
                                lambda m: str(
                                    Variable(
                                        m.group(1)).resolve(context)),
                                xvars)
                            xvars = eval(xvars[1:])
                    xvars.update({'_AJAX': 1})
                    is_post = True
            if (len(bits) >= 5):
                kwargsupdt = (Variable(bits[4]).resolve(context))

            class MetaRequest:
                def method(self):
                        # print "Length of bits", len(bits)
                        # return ("POST" if (len(bits)>=4) else "GET") if
                        # len(bits<=4) else
                        # (Variable(bits[4])).resolve(context)
                    return ("POST" if (len(bits) >= 4) else "GET") if is_post else (
                        Variable(bits[4])).resolve(context)
                POST = dict({'_AJAX': 1, '_STAFF_DEBUG': realrequest.REQUEST.get(
                    "_STAFF_DEBUG", "")} if (xvars is None) else xvars)
                REQUEST = dict({'_AJAX': 1, '_STAFF_DEBUG': realrequest.REQUEST.get(
                    "_STAFF_DEBUG", "")} if (xvars is None) else xvars)
                GET = dict({'_AJAX': 1, '_STAFF_DEBUG': realrequest.REQUEST.get(
                    "_STAFF_DEBUG", "")} if (xvars is None) else xvars)
                COOKIES = realrequest.COOKIES
                user = realrequest.user
                tenant = getattr(realrequest,"tenant",None)
                META = realrequest.META.copy()
                INTERNAL = True
                path = next
                is_request_ajax = True
                build_absolute_uri = realrequest.build_absolute_uri

            request = MetaRequest()
            # if this is embedded ajax these will already be shown in main page
            #from django.contrib.messages.storage import default_storage
            #request._messages = default_storage(request)

            try:
                view, args, kwargs = resolve(urlparse(next)[2])
            except Exception as e:
                # return "<pre>"+'URL Resolution Error : ' +
                # str(urlparse(next)[2]) + "|" + repr(e)+"</pre>"
                if request.user.is_staff:
                    return escape(
                        'URL Resolution Error : ' + str(urlparse(next)[2]) + "|" + repr(e))
                else:
                    return "(Error parts of this page are missing)"

            args = [request] + list(args)
            kwargs.update(kwargsupdt)
            try:
                return "<div id='form%s'>%s</div>" % (
                    formuuid, view(*args, **kwargs).content)
            except Exception as e:
                logging.error("Error 2 in HTML component  %r %r %r %r %r)" % (
                    bits, args, kwargs,e,view))
                if settings.DEBUG:
                    return '[Error(2) with a HTML component %r %r %r %r %r]' % (
                    bits, args, kwargs,e,view)
                else:
                    return "[Internal Error]"

                # if request.user.is_staff or request.user.is_superuser:
                #     return escape(
                #         '(Error with a HTML component %r %r %r %r %r)' % (bits, args, kwargs, e, view))
                # else:
                #     return "(Error parts of this page are missing)"

    return TNode()

#register.tag("embed_ajax", embed_ajax)
#register.tag("partial_ajax", embed_ajax)


@register_tag()
def rel_list(parser, token):
    bits = token.split_contents()

    class TNode(Node):

        def render(self, context):
            formuuid = str(uuid.uuid1()).replace("-", "")
            realrequest = Variable(bits[1]).resolve(context)
            cobject = Variable(bits[2]).resolve(context)
            add_constraint = None
            transform = lambda x: x
            xkwargs = None
            if len(bits) > 4:
                add_constraint = Variable(bits[4]).resolve(context)
                if len(add_constraint):
                    add_constraint = eval(add_constraint)
                else:
                    add_constraint = None
            if len(bits) > 5:
                transform = Variable(bits[5]).resolve(context)
                if len(transform):
                    transform = eval(transform)
                else:
                    transform = lambda x: x
            if len(bits) > 6:
                xkwargs = Variable(bits[6]).resolve(context)
                if len(xkwargs) and xkwargs[0] == "$":
                    xkwargs = eval(xkwargs[1:])

            oid = cobject.id
            dareq = {
                '_AJAX': 1,
                'oid': oid,
                'formuuid': formuuid,
                '_STAFF_DEBUG': realrequest.REQUEST.get(
                    "_STAFF_DEBUG",
                    "")}
            if add_constraint is not None:
                dareq.update(add_constraint)

            next = ""
            try:
                next = cobject.get_rel_list_url(
                    Variable(
                        bits[3]).resolve(context))
            except Exception as e:
                return 'URL Resolution Error 1 : ' + repr(e)

            class MetaRequest:
                def method(self):
                    return "GET"
                POST = dict(dareq)
                REQUEST = dict(dareq)
                GET = dict(dareq)
                user = realrequest.user
                tenant = getattr(realrequest,"tenant",None)
                META = realrequest.META.copy()
                COOKIES = realrequest.COOKIES
                INTERNAL = True
                path = next
                is_request_ajax = True
                build_absolute_uri = realrequest.build_absolute_uri

            request = MetaRequest()
            try:
                view, args, kwargs = resolve(urlparse(next)[2])
            except Exception as e:
                return 'URL Resolution Error 2 : ' + next + \
                    " ___ " + str(urlparse(next)[2]) + "|" + repr(e)

            args = [request] + list(args)
            kwargs['elem_view'] = transform
            if xkwargs is not None:
                kwargs.update(xkwargs)
            try:
                if request.user.is_staff:
                    if len(request.REQUEST.get("_STAFF_DEBUG", "")) and int(
                            request.REQUEST.get("_STAFF_DEBUG", "0")):
                        content = view(*args, **kwargs).content
                        return u"<div id='form%s' >%s</div><a href=\"%s\">(embed view link)%r %r\r</a>" % (
                            formuuid, content, next, escape(args), escape(kwargs))
                    else:
                        return "<div id='form%s' >%s</div>" % (
                            formuuid, view(*args, **kwargs).content)
                else:
                    return "<div id='form%s' >%s</div>" % (
                        formuuid, view(*args, **kwargs).content)
            except Http404:
                logging.error("Error(3) in HTML component  %r %r %r)" % (
                    bits, args, kwargs))
                if settings.DEBUG:
                    return '[Error(3) with a HTML component %r %r %r]' % (
                    bits, args, kwargs)
                else:
                    return "[Internal Error]"

    return TNode()




@register_tag()
def rel_list_m2m(parser, token):
    bits = token.split_contents()

    class TNode(Node):

        def render(self, context):
            from urlparse import urlparse
            from django.core.urlresolvers import resolve
            formuuid = str(uuid.uuid1()).replace("-", "")
            realrequest = Variable(bits[1]).resolve(context)
            cobject = Variable(bits[2]).resolve(context)
            oid = cobject.id
            transform = lambda x: x
            dareq = {'_AJAX': 1, '_NO_M2M':0,'oid': oid, 'formuuid': formuuid}

            class MetaRequest:
                def method(self):
                    return "GET"
                # POST=dict(dareq)
                REQUEST = dareq
                GET = dareq
                user = realrequest.user
                tenant = getattr(realrequest,"tenant",None)
                META = realrequest.META.copy()
                INTERNAL = True
            xrequest = MetaRequest()
            try:
                fieldname = Variable(bits[3]).resolve(context)
                try:
                    daurl = getattr(
                        cobject,
                        fieldname).through.get_add_url() + "?_AJAX=1&" + getattr(
                        cobject,
                        fieldname).source_field_name + "=" + cobject.id + "&on_success=hideMainModal()"
                except:
                    daurl = None
                next = cobject.get_rel_list_url(
                    (Variable(bits[3])).resolve(context))
                view, args, kwargs = resolve(urlparse(next)[2])
                args = [xrequest] + list(args)
                if len(bits) > 5:
                    transform = Variable(bits[5]).resolve(context)
                    transform = eval(transform)
                kwargs['elem_view'] = transform
                formuuid = unicode(formuuid)
                VR = view(*args, **kwargs).content.decode('utf8')
                A = u"<div id='form" + formuuid + u"'>" + VR + u"</div>"
                B = ""
                if (daurl):
                    B = """<a onclick='showMainModal("%s",refresh_page);'
                              class='btn btn-mini'
                              style='margin-left:auto;margin-right:auto;'
                              >Create relation
                          </a>
                          """ % (daurl,)
                else:
                    if (realrequest.user.is_staff and (
                            realrequest.REQUEST.get("_STAFF_DEBUG", ""))):
                        B = "<div class='staff-only'> Relations not handled  </div>"
                return A + B
            except Http404:
                logging.error("Error(4) in HTML component  %r %r %r)" % (
                    bits, args, kwargs))
                if settings.DEBUG:
                    return '[Error(4) with a HTML component %r %r %r]' % (
                    bits, args, kwargs)
                else:
                    return "[Internal Error]"
    return TNode()


