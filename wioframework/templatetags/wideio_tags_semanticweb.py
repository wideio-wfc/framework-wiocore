# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
#
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
#
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- coding: utf-8 -*-

import re
import inspect
from django.template import Node
from django.template import Variable
from wioframework.tagslib import register_tag


#
# class OrderedMeta(type):
#     @classmethod
#     def __prepare__(metacls, name, bases):
#         return OrderedDict()
#
#     def __new__(cls, name, bases, clsdict):
#         c = type.__new__(cls, name, bases, clsdict)
#         c._orderedKeys = clsdict.keys()
#         return c
#
# class Person(metaclass=OrderedMeta):
#     name = None
#     date_of_birth = None
#     nationality = None
#     gender = None
#     address = None
#     comment = None

def ordered_attrs(m):
    """
    Returns the attributes of an object according to the original order in which they were declared in source
    """
    res = []
    for n in dir(m):
        try:
            res.append((inspect.getsourcelines(getattr(m, n))[-1], n, getattr(m,n)))
        except:
            pass
    res=sorted(res, key=lambda k: k[0])
    return map(lambda x: (x[1], x[2]), res)


@register_tag(type="filter")
def RDFa(value, **kwargs):
    """
    Generates proper HTML description associated with an instance of class -

    In the same way as "schema.org" demonstrates sample code for all types of objects.
    This helps to get standardised class names for semantic element of an object.
    Fields appear in the same order as in in the class declaration, and making usage of
    the semantic web.
    """
    T0 = """<div class="%s-wrapper">%s</div>"""
    T1 = """<span %s>%s</span>"""
    if isinstance(type(value), models.Model):
        for k, v in value.__ordered_dict__:
            attrs = [
                ("class", "prope-" + v),
                ("property", v),
                ("content", value_to_rdfa(v))
            ]
            attrs = " ".join(["%s=\"%s\"" % (kk, vv) for kk, vv in attrs])

        return T1 % (attrs, "\n".join(res))
    if type(value) == dict:
        res = ""
        for k, v in value:
            res += T0 % ("item-" + k, RDFa(v))

        return T % (type(value).__name__, "".join(res))
    elif type(value) == list:
        return "\n".join(map(RDFa, value))
    else:
        return value
