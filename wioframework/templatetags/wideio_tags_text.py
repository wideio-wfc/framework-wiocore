# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
#
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
#
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- coding: utf-8 -*-
import json
import math
import re
import settings
import uuid
import datetime
from markdown import  Markdown

from django.utils.safestring import mark_safe
from wioframework.tagslib import register_tag
from wioframework.wiotext import wiotext_pre

ipsum_lorem=[
"""Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc tincidunt magna quis ante dictum, non vulputate eros tempus. In hac habitasse platea dictumst. Sed egestas vel lectus nec commodo. Fusce eu ornare lorem. Suspendisse sit amet arcu massa. Phasellus lobortis quis justo a bibendum. Sed ut quam eget risus mollis pretium. Aliquam erat volutpat. Duis eget consectetur urna.""",
"""Vestibulum efficitur volutpat accumsan. Maecenas rhoncus imperdiet lobortis. Maecenas quis accumsan mi. Maecenas malesuada vel turpis ac gravida. Nulla eu nunc eu mauris tempus dignissim vitae sed lorem. Ut nec augue eu enim pellentesque sollicitudin. Fusce placerat elit molestie consectetur dignissim. Sed id posuere enim.""",
"""Aenean vitae lacus vitae arcu porttitor aliquet sit amet sed libero. Quisque vel felis nulla. Ut at nunc quis arcu ultricies rutrum. Vestibulum sollicitudin facilisis sem, ullamcorper accumsan lacus posuere in. Ut eu volutpat eros. Duis hendrerit ante at enim aliquam, vel iaculis nulla maximus. Nam facilisis tortor quis congue ultrices.""",
"""Praesent blandit porttitor enim, et mattis enim eleifend at. Curabitur egestas eu magna consectetur euismod. Pellentesque suscipit molestie lacinia. Curabitur consectetur ultricies urna ut bibendum. Aenean condimentum, odio mattis molestie porttitor, libero nulla iaculis ante, non laoreet elit justo ullamcorper turpis. Nam facilisis lorem ut lacus mattis, sed tincidunt ante mollis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Vivamus lacus nisl, pellentesque et urna quis, vulputate dignissim augue. In congue blandit mattis. Nunc vitae consequat felis. Phasellus egestas accumsan ante, sed rhoncus lacus mattis ut.""",
"""Pellentesque fringilla sit amet tellus vitae ullamcorper. Praesent a gravida nisi. Aliquam faucibus hendrerit lectus, ut gravida nisi scelerisque ac. Donec orci neque, blandit sit amet arcu ac, consectetur sodales dolor. Sed accumsan neque varius tortor dignissim ultrices. Vestibulum a feugiat magna, sed porta nulla. Cras laoreet nisi purus, et imperdiet enim fermentum ac. Integer id vestibulum nisl. Sed sed diam ipsum. Fusce ac vehicula elit."""
]
@register_tag(type="filter")
def prettydate(d):
    diff = datetime.datetime.now() - d
    s = diff.seconds
    if diff.days > 7 or diff.days < 0:
        return d.strftime('%Y/%m/%d %H:%M')
    elif diff.days == 1:
        return '1 day ago'
    elif diff.days > 1:
        return '{} days ago'.format(diff.days)
    elif s <= 1:
        return 'just now'
    elif s < 60:
        return '{} seconds ago'.format(s)
    elif s < 120:
        return '1 minute ago'
    elif s < 3600:
        return '{} minutes ago'.format(s / 60)
    elif s < 7200:
        return '1 hour ago'
    else:
        return '{} hours ago'.format(s / 3600)


@register_tag(type="filter")
def tojson(v, replace=True):
    return mark_safe(str(
        json.dumps(v).replace('"', "'")
        if replace
        else json.dumps(v)
    ))


@register_tag(type="filter")
def xintcomma(v):
    v = str(v)
    lv = len(v)
    return ",".join([v[max(s, 0):(s + 3)]
                     for s in range(lv - int(math.ceil(lv / 3.0) * 3), lv, 3)])





@register_tag(type="filter")
def unslugify(value):
    return value.replace('_', ' ').replace('-', ' ').capitalize()

@register_tag(type="filter")
def nodrafts(value):
    return value.filter(wiostate='V')


@register_tag(type="filter")
def slugifyalt(value):
    return value.lower().replace(' ', '_')



@register_tag(type="filter")
def rst(value):
  from docutils.core import publish_parts
  return (unicode(publish_parts(value,writer_name="html")['fragment']))


@register_tag(type="filter")
def orloremipsum(value, request):
    if request.user.is_staff:
        if len(value.strip()) == 0:
            return ipsum_lorem
    return value

@register_tag(type="autotag")
def ipsumlorem(n=1,**kwargs):
    return "\n".join([ "<p>%s</p>"%(p,) for p in ipsum_lorem[:n]])

@register_tag(type="autotag")
def loremipsum(n=1,**kwargs):
    return "\n".join([ "<p>%s</p>"%(p,) for p in ipsum_lorem[:n]])






@register_tag(type="filter")
def append(value,arg1):
  return value+arg1

@register_tag(type="filter")
def sformat(value,arg1):
  return arg1%(value,)


from wioframework.wiotext import wiotextf, _wiotext, CT1


wiotext=register_tag(type="filter")(_wiotext)


def striphtml(data):
    p = re.compile(r'<.*?>')
    return p.sub('', data)

@register_tag(type="filter")
def wiosimpletext(value, request, mode="mini"):
    if value is None:
        return ""
    s = ""
    value = CT1.process(value)
    import markdown
    value = markdown.Markdown().convert(value)

    rg = list(re.finditer('<//(\s?)([A-Za-z]+) "([^"]+)"(\s?)//>', value))
    l = reduce(lambda b, x: b + [x.start(), x.end()], rg, [0]) + [len(value)]
    for li in range(0, len(l), 2):
        s += value[l[li]:l[li + 1]]
        if li + 2 < len(l):
            s += ""
    return mark_safe(striphtml(s))


@register_tag(type="filter")
def markdown(value):
    if value is None:
        return ""
    value = Markdown().convert(value)
    return mark_safe('<div class="markdown-text">' + value + "</div>")


@register_tag(type="filter")
def wlen(value):
    if value is None:
        return "0"
    return str(len(value))


@register_tag(type="filter")
def wiofilter(value, request, mode="mini"):
    if value is None:
        return ""
    value = wiotext_pre(value)
    s = mark_safe("")
    rg = list(re.finditer('<//(\s?)([A-Za-z]+) "([^"]+)"(\s?)//>', value))
    l = reduce(lambda b, x: b + [x.start(), x.end()], rg, [0]) + [len(value)]
    for li in range(0, len(l), 2):
        s += value[l[li]:l[li + 1]]
        if li + 2 < len(l):
            s += wiotextf(rg[li / 2], request, mode)
    return mark_safe(s)


@register_tag(type="filter")
def dotransform(value, transform):
    return mark_safe(transform(value))


@register_tag(type="filter")
def uuid1(v):
    return str(uuid.uuid1())

