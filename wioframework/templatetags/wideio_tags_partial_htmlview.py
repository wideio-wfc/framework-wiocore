# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
#
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
#
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- coding: utf-8 -*-
from django.template import Node
from django.template import Variable
import logging
from django.core.exceptions import ObjectDoesNotExist
from django.template import Node
from django.template import Variable
from wioframework.tagslib import register_tag
from django.utils.html import escape
from django.http import Http404


@register_tag()
def html_view(parser, token):
    """
    Display the view of an object
    """
    bits = token.split_contents()

    class TNode(Node):
        def render(self, context):
            mode = "std"
            transform = lambda x: x
            realrequest = Variable("request").resolve(context)
            model = Variable(bits[1]).resolve(context)
            if type(model) in [str,unicode]:
              if realrequest.user.is_staff:
                return ("Error in HTMLVIEW:" + "Model should not be specified as a string in HTML VIEW "+model)
              else:
                return "[Internal Error]"
            if len(bits) > 2:
                mode = Variable(bits[2]).resolve(context)
            if len(bits) > 3:
                if len(bits[3]) and (bits[3] not in [u"''", u'""']):
                    parent_object = Variable(bits[3]).resolve(context)
            if len(bits) > 4:
                transform = context[
                    bits[4]]  # Variable(bits[4]).resolve(context)
                if not callable(transform):
                    raise Exception, "transform is not callable"
            if model is not None and hasattr(model, 'html'):
                try:
                    return transform(model.html(realrequest, mode))
                except Exception as e:
                    return "Error in HTMLVIEW:" + str(e)
            else:
                try:
                  if isinstance(model, tuple) and hasattr(model[0], 'html'):
                    from django.shortcuts import RequestContext
                    from django.template.loader import render_to_string
                    prefix = ""
                    import settings

                    p = "models/list_of_" + model[0]._meta.object_name.lower() + "s/" + mode + ".html"
                    return render_to_string(p, {'x': model[1].filter(wiostate='V'), 'y': model[2],
                                                "parent_object": parent_object, 'request': realrequest,
                                                'settings': settings},
                                            context_instance=RequestContext(realrequest)
                                            )
                except Exception,e:
                  logging.error("HTML_VIEW_E1 %r"%(e,))

                if realrequest.user.is_staff:
                    try:
                      if isinstance(model, tuple):
                        model = model[0]
                      if not hasattr(model, "_meta"):
                        return escape(
                            (model)) + "(missing template or invalid reference for " + repr(model) + ")" + bits[1]
                    except Exception,e:
                      print "HTML_VIEW_E2",repr((e,model))
                    from wioframework.amodels import Model
                    return Model.html(model,realrequest,mode)
                    return escape((model)) + "(missing template for " + (
                        model._meta.object_name.lower() if (model is not None and hasattr(model, '_meta')) else bits[1]) + ")"
                return "(None)"
    return TNode()


@register_tag()
def generic_html_view(parser, token):
    """
    Display the view of an object referenced by a genericfkey
    """

    bits = token.split_contents()
    from django.contrib.contenttypes.models import ContentType

    class TNode(Node):
        def render(self, context):
            mode = "std"
            realrequest = (Variable("request")).resolve(context)
            model = (Variable(bits[1])).resolve(context)

            if (len(bits) > 2):
                mode = (Variable(bits[2])).resolve(context)

            content_type = None
            try:
                content_type = ContentType.objects.get(
                    pk=model.content_type_id)
                if not content_type.model_class():
                    return "[ERROR CONTENT TYPE MODEL CLASS !]"
                obj = content_type.get_object_for_this_type(pk=model.object_pk)
            except (ObjectDoesNotExist, ValueError):
                return "[ERROR OBJECT DOES NOT EXIST]" + str(content_type)

            if (obj is not None):
                return obj.html(realrequest, mode)
            else:
                return "(None)"
    return TNode()


