# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
#
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
#
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- coding: utf-8 -*-
from django.template import Node
from django.template import Variable
from wioframework.tagslib import register_tag

import settings
import uuid

from wideio_tags_text import unslugify

def proper_title(s):
    return unslugify(s).capitalize()



# def rel_actions(parser, token):
#     bits = token.split_contents()
#
#     class TNode(Node):
#
#         def render(self, context):
#             import hashlib
#             import random
#             from urlparse import urlparse
#             from django.core.urlresolvers import resolve
#             formuuid = hashlib.md5(str(random.random())).hexdigest()
#             realrequest = (Variable("request")).resolve(context)
#             return "[Add]"
#     return TNode()
#
# register.tag("rel_actions", rel_actions)

@register_tag()
def actionbox(parser, token):
    bits = token.split_contents()

    class TNode(Node):

        def render(self, context):
            r = ''
            mode = "std"
            action_filter = lambda x: True
            request = (Variable("request")).resolve(context)
            cobject = (Variable(bits[1])).resolve(context)
            style = ["btn-mini", "icon", "text"]
            cuuid = str(uuid.uuid1()).replace('-', '')
            if (len(bits) > 2):
                style = (Variable(bits[2])).resolve(context)
                if (type(style) not in [list, tuple]):
                    if style.strip()[0] in ["[", "("]:
                        style = eval(style)
                    else:
                        style = str(style).split(",")
            if (len(bits) > 3):
                action_filter_s = (Variable(bits[3])).resolve(context)
                try:
                    max_level = int(action_filter_s)
                    action_filter = lambda x: (x <= max_level)
                except:
                    action_filter = eval("lambda x:" + action_filter_s)

            iconCls = 'glyphicon glyphicon-chevron-down'
            if "menu" in style:
                r += '<a class="easyui-euimenubutton" data-options="duration:10000,euimenu:\'#mb%s\',iconCls:\'%s\'"></a>\n' % (
                    cuuid,iconCls)
                r += '<div class="hidden"><div id="mb%s">' % (cuuid,)
            admm = '<a class="easyui-euimenubutton staff-only" data-options="duration:10000,euimenu:\'#mba%s\',iconCls:\'%s\'"></a>\n' % (
                cuuid,iconCls)
            admm += '<div class="hidden"><div id="mba%s">' % (cuuid,)
            rb = ''
            rs = ''
            rss = ''
            ri = ''
            if hasattr(cobject, "WIDEIO_Meta"):
                if hasattr(cobject.WIDEIO_Meta, "Actions"):
                    for m in sorted(
                            cobject.__class__.WIDEIO_Meta.Actions._act_list):
                        try:
                            ca = getattr(cobject.WIDEIO_Meta.Actions, m)
                            if (action_filter(ca)) and (
                                    ca.possible(cobject, request)):
                                staff_only = False
                                superuser_only = False
                                if request.user.is_staff:
                                    nu = request.user
                                    nu.is_staff = False
                                    staff_only = not ca.possible(
                                        cobject,
                                        request)
                                    if not staff_only and nu.is_superuser:
                                        nu.is_superuser = False
                                        superuser_only = not ca.possible(
                                            cobject,
                                            request)
                                        nu.is_superuser = True
                                    nu.is_staff = True
                                if hasattr(ca,"thref"):
                                  thref=ca.thref
                                else:
                                  thref = ( "data-href" if ca.meta["mimetype"] == "application/javascript" else "data-posthref")
                                title = ""
                                icon_txt = ""
                                if (superuser_only or staff_only) or "icon" in style:
                                    if "icon" in ca.meta:
                                        if '.' in ca.meta["icon"]:
                                            icon_txt = '<img class="custom-icon" border=0; src="%simages/icons/%s" alt="icon" />' % (
                                                settings.MEDIA_URL, ca.meta["icon"])
                                        else:
                                            icon_txt = '<i class="%s"></i>' % (
                                                ca.meta["icon"],)
                                title += icon_txt
                                if (superuser_only or staff_only) or "text" in style:
                                    title += proper_title(ca.act_name)
                                if (title == ""):
                                    title = "@"
                                extraclass = 'model-action ' + ca.meta.get("xclass", "")
                                extraattr = ca.meta.get("xattrs", "")
                                # print style[0]
                                if not (staff_only or superuser_only):
                                  if "tip" in map(str, style):
                                      extraattr += "data-tip=\"%s\"" % (
                                          proper_title(ca.act_name),)
                                      extraclass += "tip-below "
                                  if str(style[0]) == "btn":
                                      extraclass += "btn btn-default"
                                  elif str(style[0]) == "btn-mini":
                                      extraclass += "btn btn-default btn-mini "
                                  elif str(style[0]) == "btn-large":
                                      extraclass += "btn btn-default btn-large "

                                gq=""
                                if "data-modal-link" in extraattr:
                                    thref = 'href'
                                    gq="?_BASE=ajax&_AJAX=1"
                                elif ca.act_name == "delete":
                                    thref = 'data-modal-link href'
                                else:
                                    extraclass += "action-btn"
                                if staff_only:
                                    extraclass += " staff-only"
                                if superuser_only:
                                    extraclass += " superuser-only"
                                ct = str(
                                    """<a %s="%s" %sclass="%s" >%s</a>""" %
                                    (thref,
                                     cobject.get_action_url(
                                         ca.act_name)+gq,
                                        extraattr,
                                        extraclass,
                                        title))  # +repr(ca.meta)
                                if (superuser_only or staff_only) or ("menu" in style):
                                    ct = "<div>" + ct + "</div>"
                                if superuser_only:
                                    rss += ct
                                else:
                                    if staff_only:
                                        rs += ct
                                    else:
                                        rb += ct
                            else:
                                pass
                        except Exception as e:
                            if request.user.is_staff:
                                rs += "<div><a title=\"%s\">error in action %s</a></div>" % (
                                    str(e).replace('"', '\''), ca.act_name)

            if rb == "" and ("menu" in style):
                r = ""
            else:
                r += rb
                if "menu" in style:
                    r += "</div></div>"
            x = rs + rss + ri
            if len(x):
                admm += x
                admm += "</div></div>"
                r += admm
            return '<span class="actions">' + r + '</span>'
    return TNode()
