# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- coding: utf-8 -*-
import sys
import hashlib
import base64
import urllib
import traceback
import math
import re
import uuid
import json
import logging
from functools import reduce
from urlparse import urlparse

from django import template

from django.core.urlresolvers import resolve
from django.http import Http404
from django.template import Library, Node, TemplateSyntaxError
from django.template import Variable
from django.template.loader import render_to_string
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext as _
from django.utils.html import escape


import settings
from django.shortcuts import render_to_response, RequestContext

register = template.Library()

from wioframework.tagslib import import_tags_from

register=import_tags_from(register, "wioframework.templatetags.wideio_tags_actions")
register=import_tags_from(register, "wioframework.templatetags.wideio_tags_components")
register=import_tags_from(register, "wioframework.templatetags.wideio_tags_coreprg")
register=import_tags_from(register, "wioframework.templatetags.wideio_tags_framework")
register=import_tags_from(register, "wioframework.templatetags.wideio_tags_partial")
register=import_tags_from(register, "wioframework.templatetags.wideio_tags_partial_htmlview")
register=import_tags_from(register, "wioframework.templatetags.wideio_tags_service")
register=import_tags_from(register, "wioframework.templatetags.wideio_tags_text")
register=import_tags_from(register, "backoffice.tags")


##
## -------------- still defined here ----------
##

def counter_model(parser, token):
    bits = token.split_contents()

    class TNode(Node):
        def render(self, context):
            module = (Variable(bits[1])).resolve(context)
            modelname = (Variable(bits[2])).resolve(context)
            modelclass = getattr(
                __import__(
                    "aiosciweb1." +
                    module +
                    ".models",
                    fromlist=[
                        "aiosciweb1",
                        str(module)]),
                modelname)
            return unicode(modelclass.objects.filter(wiostate='V').count())
    return TNode()

register.tag("counter_model", counter_model)




def ref_problem(parser, token):
    bits = token.split_contents()

    class TNode(Node):
        def render(self, context):
            problem = Variable(bits[1]).resolve(context)
            from science import models as scimo
            p = scimo.Question.objects.get(name=problem)
            return '<a href="%s">%s</a>' % (p.get_view_url(), problem)
    return TNode()


register.tag("ref_problem", ref_problem)




def _CanDo(parser, token):
    bits = token.split_contents()

    class TNode(Node):

        def render(self, context):
            subattr = None
            request = (Variable("request")).resolve(context)
            try:
                permissions = (Variable("permissions")).resolve(context)
            except:
                permissions = None
            cobject = (Variable(bits[1])).resolve(context)
            access = Variable(bits[2]).resolve(context)
            if (bits[3] != "as"):
                raise Exception()
            tovar = bits[4]
            if (access in ["list", "add"]):
                if permissions is None or permissions[access](request):
                    if (hasattr(cobject, "can_" + access)):
                        try:
                            context[tovar] = (
                                getattr(
                                    cobject,
                                    "can_" +
                                    access))(request)
                        except:
                            context[tovar] = False
                    else:
                        context[tovar] = True
                else:
                    context[tovar] = False
            if (access in ["update", "view", "delete"]):
                # print permissions
                if permissions is None or permissions[
                        access](cobject, request):
                    if (hasattr(cobject, "can_" + access)):
                        try:
                            context[tovar] = (
                                getattr(
                                    cobject,
                                    "can_" +
                                    access))(request)
                        except:
                            context[tovar] = False
                    else:
                        context[tovar] = True
                else:
                    context[tovar] = False
            return ""
    return TNode()

register.tag("check_possible", _CanDo)




def _editable(parser, token):
    """
    Unifiy view and update
    """
    bits = token.split_contents()

    class TNode(Node):

        def render(self, context):
            obj = Variable(bits[1]).resolve(context)
            attr = Variable(bits[2]).resolve(context)
            request = Variable("request").resolve(context)
            var = getattr(obj, attr)
            if var is None:
                var = "_"
            
            try:
              cu=obj.can_update(request)
            except Exception,e:
              sys.stderr.write(repr(e)+"\n")
              cu=False
            if cu:
                return """<span class="editable"  data-url="%s" data-type="textarea" data-name="%s">%s</span>""" % (
                    obj.get_update_url(), attr, var)
            else:
                return var
    return TNode()

register.tag("editable", _editable)




from django.apps.registry import apps


def _counter(parser, token):
    bits = token.split_contents()

    class TNode(Node):

        def render(self, context):
            subattr = None
            an = Variable(bits[1]).resolve(context)
            mn = Variable(bits[2]).resolve(context)
            flt= Variable(bits[3]).resolve(context)
            flt=eval(flt)
            
            return apps.get_model(an,mn).objects.filter(**flt).count()
    return TNode()

register.tag("counter", _counter)

class Javascript:
    def __init__(self, v):
        self.text = v
    def __repr__(self):
        return self.text


class Unicode:
    def __init__(self, v):
        self.text = v
    def __repr__(self):
        return u'"%s"' % (self.text)


def _param_update(parser, token):
    bits = token.split_contents()

    class TNode(Node):
        def render(self, context):
            subattr = None
            updt = (Variable(bits[1])).resolve(context)
            request = Variable("request").resolve(context)
            d = dict(request.GET.items())
            if (not isinstance(updt, dict)):
                updt = re.sub(
                    r"\{\{([^}]+)\}\}",
                    lambda m: str(
                        Variable(
                            m.group(1)).resolve(context)),
                    updt)
                updt = eval(updt, {'Javascript': Javascript}, {})
            d.update(updt)

            return "$.param({" + ",".join(map(lambda x: ("\"%s\":%r" % (
                x[0], (Unicode(x[1]) if (isinstance(x[1], unicode)) else x[1]))), d.items())) + "})"
    return TNode()

register.tag("param_update", _param_update)

@register.filter
def nodraft(q):
    return q.filter(wiostate='V')

def for_scale(image,w):
  return (image.for_scale(int(w), int(w)) if hasattr(image,"for_scale") else image)
register.filter("for_scale", for_scale, is_safe=True)
