# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
#
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
#
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- coding: utf-8 -*-
# all these wouldn't exist if we were using jinja
import json
import logging
from django.template import Node
from django.template import Variable
from wioframework.tagslib import register_tag
from django.utils.html import escape


@register_tag(name="getattr")
def _getattr(parser, token):
    bits = token.split_contents()

    class TNode(Node):
        def render(self, context):
            subattr = None
            try:
                var = Variable(bits[1]).resolve(context)
                attr = Variable(bits[2]).resolve(context)
            except Exception, e:
                return "(system error in template!%s)" % (escape(e)[:100],)
            tovar = None
            if len(bits) > 3:
                if bits[3] != "as":
                    subattr = Variable(bits[3]).resolve(context)
                    if len(bits) > 4:
                        if bits[4] != "as":
                            raise Exception()
                        tovar = bits[5]
                else:
                    if len(bits) > 3:
                        if bits[3] != "as":
                            raise Exception()
                        tovar = bits[4]
            try:
                var = getattr(var, attr)
            except Exception as e:
                request = Variable("request").resolve(context)
                if request.user.is_staff:
                    return str(e)
                else:
                    return "[system error]"
            if subattr:
                var = getattr(var, subattr)
            if callable(var):
                try:
                    var = var()
                except:
                    pass
            if tovar is not None:
                context[tovar] = var
                return ""
            else:
                return var

    return TNode()


@register_tag(name="getitem")
def _getitem(parser, token):
    bits = token.split_contents()

    class TNode(Node):

        def render(self, context):
            subattr = None
            var = Variable(bits[1]).resolve(context)
            attr = Variable(bits[2]).resolve(context)
            tovar = None
            else_value = None
            if len(bits) > 3:
                if bits[3] != "as":
                    subattr = Variable(bits[3]).resolve(context)
                    if len(bits) > 5:
                        tovar = bits[5]
                else:
                    if len(bits) > 4:
                        tovar = bits[4]
                        if len(bits) > 6:
                            if bits[5] == "else":
                                else_value = bits[6]

            try:
                var = var[attr]
            except Exception as e:
                var = "**** ERROR: %r ****" % (e,)
                if else_value is not None:
                    var = ""
                    context[tovar] = else_value
                return var
            if subattr:
                var = getattr(var, subattr)
            if tovar is not None:
                context[tovar] = var
                return ""
            else:
                return var

    return TNode()


@register_tag()
def docall1(parser, token):
    bits = token.split_contents()

    class TNode(Node):
        def render(self, context):
            try:
                obj = ((Variable(bits[1])).resolve(context))
                meth = ((Variable(bits[2])).resolve(context))
                arg1 = (Variable(bits[3])).resolve(context)
                if obj:
                    if hasattr(obj, meth):
                        val = (getattr(obj, meth))(arg1)
                    else:
                        val = obj[meth](arg1)
                else:
                    val = meth(arg1)
                tovar = None
                if len(bits) > 4:
                    if bits[4] != "as":
                        raise Exception("Invalid Syntax")
                    else:
                        if len(bits) > 5:
                            tovar = bits[5]

                if tovar is not None:
                    context[tovar] = val
                    return ""
                else:
                    return val
            except Exception as e:
                return "(_docall1 : error " + repr(e) + ")"

    return TNode()


@register_tag(type="filter")
def call1(value, arg1):
    if value is None:
        return ""
    return value(arg1)


@register_tag(type="filter")
def toobject(value, arg1, arg2):
    from django.apps import registry
    try:
        return registry.get_model(arg1, arg2).objects.get(id=value)
    except:
        return None


@register_tag(type="filter")
def get_class(value):
    return value.__class__.__name__


@register_tag(type="autotag")
def as_dict(d, **kwargs):
    if not isinstance(d, dict):
        if hasattr(d, "items"):
            d = dict(d.items())
        else:
            d = d.__dict__
            if callable(d):
                d = d()
    try:
        return json.dumps(d)
    except Exception as e:
        logging.error("Error during template rendering")
        return json.dumps(simple_dict(d))


def simple_dict(d):
    def transform(el):
        if type(el) in [int, str, unicode, bool, float]:
            return el
        elif isinstance(el, dict):
            return simple_dict(el)
        elif isinstance(el, list):
            return map(transform, el)
        elif isinstance(el, tuple):
            return tuple(map(transform, el))
        else:
            return None

    nd = []
    for i in d.items():
        if i[0][0] != "_":
            nd.append((i[0], transform(i[1])))
    return dict(nd)
