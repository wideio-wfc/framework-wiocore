# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
#
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
#
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- coding: utf-8 -*-

# TODO: Probably to deprecate to move to JS
# PURE HTML
import re
import uuid
from django.template import Node
from django.template import Variable
from wioframework.tagslib import register_tag
from django.utils.html import escape


@register_tag(type="autotag")
def wideio_progressbar(pname,upurl,value,**kwargs):
    value = value * 100.
    return render_to_string("progressbar.html", {
        'progressbarid': hashlib.md5(pname).hexdigest(),
        'upurl': upurl,
        'value': value
    })


@register_tag()
def collapsible(content, title):
    if (len(title) == 0):
        title = "_"
    return ("""<div >
            <div class="accordion-heading">
              <a class="accordion-toggle" data-toggle="collapse"  href="#collapse1%(uuid)s">
                  %(title)s
              </a>
            </div>
            <div id="collapse1%(uuid)s" class="collapse in" >
                <div class="accordion-inner">
                  %(content)s
                </div>
            </div>
       </div>
    """) % {"content": content, "title": title , "uuid": uuid.uuid1()}


@register_tag()
def begincollapsible(parser, token):
    bits = token.split_contents()
    nodelist = parser.parse(('endcollapsible',))
    parser.delete_first_token()

    class TNode(Node):
        def render(self, context):
            value = nodelist.render(context)
            r = collapsible(
                value,
                """<i class="icon-collapse"></i>""" +
                Variable(
                    bits[1]).resolve(context))
            return r
    return TNode()


@register_tag()
def dict_table(parser, token):
    bits = token.split_contents()

    class TNode(Node):
        def render(self, context):
            dtable = Variable(bits[1]).resolve(context)
            if (not isinstance(dtable, dict)):
                if callable(dtable.__dict__):
                    dtable = dtable.__dict__()
                else:
                    dtable = dtable.__dict__
            r = ["<table>"]
            c = 0
            for e in dtable.items():
                try:
                    r.append(
                        "<tr class='%s-row'><td>%s</td><td>%s</td></tr>\n" %
                        (("even" if (
                            c %
                            2 == 0) else "odd"), e[0], escape(
                            "%r" %
                            (e[1],))))
                except:
                    r.append(
                        "<tr class='%s-row'><td>%s</td><td>%r</td></tr>\n" %
                        (("even" if (
                            c %
                            2 == 0) else "odd"),
                            e[0],
                            "***ERROR***"))
                c += 1
            r.append("</table>")
            r = "\n".join(r)
            title = dtable
            if (isinstance(title, dict)):
                if "title" in title:
                    title = title["title"]
                elif "name" in title:
                    title = title["name"]
            else:
                title = unicode(dtable)
            r = collapsible(r, """<i class="icon-collapse"></i>Data""")
            return r

    return TNode()


@register_tag(type="filter")
def wrapwith(text, selector="div.wrapped"):
    parsed = {'class': [], 'id': '', 'attr': {}}
    words = re.split(r'\W+', selector)
    symbols = re.split(r'\w+', selector)
    for pair in zip(words, symbols):
        (word, symbol) = pair
        if symbol == "":
            tag = word
        elif symbol == ".":
            parsed['class'].append(word)
            lastattr = parsed['class']
        elif symbol == "#":
            parsed['id'] = word
        elif symbol == '[':
            toParse = word
        elif symbol == '=' and toParse:
            parsed['attr'][toParse] = word
            lastattr = parsed['attr'][toParse]
        elif symbol == ']':
            del toParse
            del lastattr
        elif type(lastattr) is list:
            lastattr[-1] += symbol + word
        elif lastattr:
            lastattr += symbol + word

    # clean object
    args = ' id="%s"' % parsed['id'] if parsed['id'] else ""
    args += ' class="' + \
        " ".join(parsed['class']) + '"' if parsed['class'] else ""
    args += ''.join(map(lambda(n, v): ' ' + n + '="' + v +
                        '"', parsed['attr'].items())) if parsed['attr'] else ""
    return "<" + tag + args + ">" + text + "</" + tag + ">"
