# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################


import os
import datetime

if __name__=="__main__":
        os.environ["DJANGO_SETTINGS_MODULE"] = 'settings'
        import settings
        from django.apps.registry import apps
        from django.conf import global_settings
        apps.populate(settings.INSTALLED_APPS)

from django.apps.registry import apps        
get_models = apps.get_models
models=get_models()

import wioframework.utils
import settings
import logging

from django_cron.base import cronScheduler, CronJob
from django.http import HttpResponse


class RemoveAllDeletedObjects(CronJob):
    run_every = 600
    def job(self):
        for m in models:
          if hasattr(m,"wiostate") and hasattr(m,"updated_at"):
            try:
              toberemoved=m.objects.filter(wiostate='D',updated_at__lt=datetime.datetime.now()-datetime.timedelta(1))
              print "del check",m,toberemoved.count(),"/",m.objects.all().count()
            except Exception,e:
              print e
            #toberemoved.delete()

cronScheduler.register(RemoveAllDeletedObjects)


def keep_if_ok(a0):
              try:
                for f in filter(lambda x:type(x).__name__ in ["ForeignKey","OneToOneField"],a0._meta.fields):
                  getattr(a0,f.name)
              except Exception,e:
                if hasattr(a0,"get_"+f.name):
                    try:
                        setattr(a0,f.name,getattr(a0,"get_"+f.name)())
                        a0.save()
                        return keep_if_ok(a0)
                    except:
                        pass
                return False
              return True


                                                                                                                                                                                                            
class WarnForBrokenObjects(CronJob):
    run_every = 60*60*24
    def job(self,cl=False):
        rl=[]
        for m in models:
          if hasattr(m,"wiostate"):
            for n in m.objects.all():
              if not keep_if_ok(n):
                 if cl:
                    print ((n.get_view_url(),n))                     
                 else:
                   rl.append((n.get_view_url(),n))
        if len(rl): 
          utils.wio_send_mail(None, None, "broken refs found in production", "mails/admin_broken_refs.html", {'rl':rl}, map(lambda x:x[0],settings.ADMINS))
            #toberemoved.delete()

cronScheduler.register(WarnForBrokenObjects)

if __name__=="__main__":
        logging.info("BROKEN REFS")
        wfbo=WarnForBrokenObjects()
        wfbo.job(True)

        logging.debug("DRAFT OBJS")
        for m in models:
          if hasattr(m,"wiostate") and hasattr(m,"updated_at"):
              for o in m.objects.filter(wiostate__startswith='U'):
                  logging.debug("%r,%r"%(o, o.get_view_url()))

        logging.debug("OBJ AWAITING MODERATION")
        for m in models:
          if hasattr(m,"wiostate") and hasattr(m,"updated_at"):
              for o in m.objects.filter(wiostate__startswith='S'):
                  logging.debug("%r,%r"%(o, o.get_view_url()))

        logging.debug("OBJ AWAITING DELETION")
        for m in models:
          if hasattr(m,"wiostate") and hasattr(m,"updated_at"):
              for o in m.objects.filter(wiostate__startswith='D'):
                  logging.debug("%r,%r"%(o, o.get_view_url()))


