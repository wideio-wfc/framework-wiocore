import mock
import sys
import dis


def cf(*args,**kwargs):
    @mock.patch("__main__.g") # force reimport of the module?
    def cfg(g):
        g.side_effect=h
        r=f(*args,**kwargs)
        return r
    return cfg()

def make_cf(f,i,scenario="mock"):
    def cf(*args,**kwargs):
        patch_target=i[0]
        if not "." in i[0]:
            patch_target=(sys.modules[f.__module__].__name__+"."+i[0])
        @mock.patch(patch_target) # force reimport of the module?
        def cfg(i0):
            i0.side_effect=i[1]
            print i[1],f
            r=f(*args,**kwargs)
            setattr(f,"mockobj_"+scenario,i0)
            return r
        return cfg()
    return cf


def make_cfr(f,i,scenario="mock"):
    # mock all function call according to right scenario
    pass

def multimock(**kwargs):
    def dec(f):
        for i in kwargs.items():
            rf=f
            for j in i[1].items():
                x=make_cf(rf,j,i[0])
                rf=x
            setattr(f,"mock_"+i[0],rf)
        return f
    return dec


import ast

def getFunctionName(func):
    if hasattr(func, "id"):
        return func.id
    else:
        if isinstance(func, ast.Attribute):
            return func.attr
        return func.name


class FunctionInliner(ast.NodeTransformer):
    def __init__(self, functions_to_inline):
        self.all_calls=[]

    def visit_Expr(self, node):
        node = self.generic_visit(node)
        if isinstance(node.value, ast.Assign):
            return node.value
        return node

    def visit_Call(self, node):
        func = node.func
        self.all_calls.append(node)

import imp
def find_module(self, name, path=None):
        file, pathname, description = imp.find_module(name, path)
        src = file.read()
        tree = ast.parse(src)
        tree = ParentNodeTransformer().visit(tree)
        print "found funcs: %s" % (function_discoverered.functions,)
        


if __name__=="__main__":
    def g(x):
        return "g"

    def h(x):
        return "h"


    H=G=lambda x:x
    H0=G0=lambda x:2
    H1=G1=lambda x:3




    @multimock(
        a={
            "__main__.G":G0,
            "__main__.H":H0
        },
        b={
            "G":G1,
            "H":H1
        }

    )
    def F(x):
        return G(x)*H(x)*math.cos(0)


    import math


    print f(3)
    print cf(3)
    print "F",F(1)
    print "Fa", F.mock_a(1)
    print "Fb",F.mock_b(1)
    print list_calls(f)
