# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
from settings import *

##
# A DIRECT CONNECTION VIA THE MONGO API MAY BE NECESSARY FOR THE REAL NOSQL STUFFS
##
# TODO : FIX ME PUT IN ANOTHER FILE TO AVOID OVER DUPLICATED CONNEXCTIONS
#
#
'''
try:
  import pymongo
  from bson.objectid import ObjectId
  mongo_url=None
  d=dict(MONGO_CONNECT.items())
  d["HOST"]="127.0.0.1"
  d["PORT"]=27017
  d["DATABASE"]=DATABASENAME
  d.update(MONGO_CONNECT)
  if (d.has_key("USER")):
      mongo_url=r"mongodb://%(USER)s:%(PASSWORD)s@%(HOST)s:%(PORT)s/%(DATABASE)s"%d
  else:
      mongo_url=r"mongodb://%(HOST)s:%(PORT)s/%(DATABASE)s"%d
  mongo_client = pymongo.MongoClient(mongo_url)

  mongo_db=mongo_client[DATABASENAME]
  print mongo_db
except Exception,e:
  print "Unable to create direct mongodb connection",e
  pass
'''

try:
    import settings
    import sys
    import os
    sys.path.append(os.path.join(settings.CWD, "mongoapi2sql", "mongo"))
    import mongo_nuodb
    import mongo_connector
    dbid = 'default'
    mongo_dbb = mongo_connector.NuodbConnector()
    mongo_dbc = mongo_dbb.connect(settings.DATABASES[dbid]["NAME"],
                                  settings.DATABASES[dbid]["HOST"],
                                  settings.DATABASES[dbid]["DBA_USER"],
                                  settings.DATABASES[dbid]["DBA_PASSWORD"],
                                  {'schema': settings.DATABASES[
                                      dbid]["SCHEMA"]}
                                  )
    # mongo_db=mongo_dbb.collections
    mongo_db = mongo_dbb
    ObjectId = lambda x: x
except Exception as e:
    mongo_db = None
    ObjectId = lambda x: x
    print "mongo db connection error", e
