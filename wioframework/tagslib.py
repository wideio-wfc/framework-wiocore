from django.template import Node
from django.template import Variable

def autotag(f):
    def tagf(parser, token):
        bits = token.split_contents()

        class TNode(Node):
            def render(self, context):
                return f(*(map(lambda x: Variable(x).resolve(context), bits[1:])),
                            django_context=context, django_parser=parser, django_token=token
                            )

        return TNode()
    return tagf

def register_tag(type="tag", name=None):
    """
    Decorator adding metadata so that template tags are registered by the main template_taggs file.
    :param type:
    :param name:
    :return:
    """
    def dec(f):
        _type=type
        if _type == "autotag":
            f=autotag(f)
            _type="tag"
        setattr(f, "template_tag_type", _type)
        if name:
            setattr(f, "template_tag_name", name)
        return f

    return dec


def import_tags_from(register, module):
    m = __import__(module, fromlist=module.split(".")[:-1])
    for e in dir(m):
        v = getattr(m, e)
        if hasattr(v, "template_tag_type"):
            name = e
            if hasattr(v, "template_tag_name"):
                name = v.template_tag_name
            getattr(register, v.template_tag_type)(name, v)
    return register