%[divert NULL]
|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
                         ..-.:.:...
                      :.-- -     ..:...
                  :.:. -             -.:...
              :.:. -                     ..:...
          :.:. .            _;:__.          ...-...
      :.:. .               :;    -+_    -|       .--...
  :.-- .                    -=      -~-.-           -.:...
-:...              ___.      -=_                        -.--
...    .          =;  --=_     :=                   ..   ...
.-.      . .              ~-___=;               . -      .:.
...           -.                             -.          ...
.:.                .                    . .              .:.
...                   -.             -.                  ...
.:.                      ...    . -                -~4>  .:.
...       _^+_.              -.                       2  ...
.:.           ~,              .                 /'   _(  .:.
....           <              -          +'  ^LJ>   _^   ...
.:..          _);             -     _   J   _/  ~~-'    .:.
....        _&i^i             .   _~_, <(   .^           ...
 :.       _v>^  <             .  _X~'  -s,               .:.
 :.             -=            .   S      ^'              ...
 :....           -=_  ,       .   2                    ..-.:
    :....          -^^        .                    .-.:. .
       ..:...                 .                 ..:. -
           -.:...             .           . :.-- -
                :....         .         -.-- .
                   -.:...     .    ..:.: -
                        -.:......--. -
                            -.:. .

Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
http://wide.io/
----------------------------------------------------------------------
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License.
    
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.
    
You should have received a copy of the GNU General Public License
along with this program.  If not, see http://www.gnu.org/licenses/
This work is released under GPL v 3.0
----------------------------------------------------------------------
For all information : copyrights@wide.io
----------------------------------------------------------------------

|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
%[/divert]

%[RS]
def str(x):
    return x.toString()

def int(x):
    return parseInt(parseFloat(x))

class GenericClass:
    def init(self, obj):
        self.obj = obj
        self.wid = guid()
        window.wiowidgetmap[self.wid] = self
        self.obj.attr({dataWiowidgetid: self.wid})
    def find(self):
        return self.obj.find.apply(self.obj, arguments)

class GenericList(GenericClass):
    def __init__(self, obj, defaultparams=None):
        self.init.apply(self, arguments)
        self.list_url = obj.attr("data-url") # list_url
        if not defaultparams:
            if obj.parent().is('.page-content'):
                defaultparams = request.GET
            else:
                defaultparams = {}
        self.defaultparams = defaultparams
        self.search = ""
        self.sortby = []
        self.$ = self.obj
        self.ready_for_activate=True
        self.request = self.getRefreshRequest()
        try:
          self.list = obj.find('.wiolst')
          self.list.attr({dataWiowidgetid: self.wid})
          self.options = {}
          self.on_refresh_handlers = []
          rebind_all(self)
        except:
          self.ready_for_activate=false        
        self.activate()
        self.find('.carousel').on('slide.bs.carousel', def(e):
            nextH = $(e.relatedTarget).height()
            $(this).find('.active.item').parent().animate({ height: nextH }, 500)
        )

    def reactivate(self):
        if self.ready_for_activate:
          self.set_event_handlers()

    def activate(self):
        self.$.addClass('jsactivated')
        self.reactivate()

    def cleanText(self, text):
        return $.trim((text or "").replace(new RegExp('\\s+', 'g'), ' '))

    def getBaseRefreshData(self):
        return {
            _AJAX: 2,
            _PAGE: 1,
            _search: self.cleanText( self.$.find(".search-input").val() ),
            _sortby: self.$.find(".sortby-input").val(),
            _INCLUDE_DRAFTS: self.$.find(".include-drafts-input").is(":checked") ? 1 : 0,
            _VIEW_OPTIONS: JSON.stringify($.extend(self.$.data('view_options'), {
                pager_disable_perpage: self.$.data('pager_disable_perpage'),
                add_enabled: self.$.data('add')
            }))
        }

    def getRefreshRequest(self, add_params):
        self.options = $.extend(self.options, add_params)
        
        self.request = {
            url: self.list_url,
            data: $.extend(self.getBaseRefreshData(), self.defaultparams, self.options)
        }
        del self.request.data["_TOTALPAGES"]
        return self.request

    def ajax(self, request):
        self.$.addClass('waiting')
        if self.currentAjax and self.currentAjax.state() == "pending":
            self.currentAjax.abort()
        return self.currentAjax = $.ajax(request)

    def replace(self, selector, old_scope, new_scope):
        new_dom = new_scope.find( selector )
        old_dom = old_scope.find( selector )
        return old_dom.html(new_dom.html()).data(new_dom.data())

    def refresh(self, add_params={}, timeout=0):
        if self.isRefreshing:
            return
        request = self.getRefreshRequest(add_params)

        request.complete = def(data):
            dom = $('<div>').html(data.responseText)
            old_dom = self.replace(".list-base", self.$, dom)
            self.onrefresh(old_dom)
        
        if self.refreshTimeout:
            clearTimeout(self.refreshTimeout)
        if timeout:
            self.refreshTimeout = setTimeout(def():
                self.ajax(request)
            , timeout)
        else:
            self.ajax(request)

    def merge(self, position, add_params={}):
        request = self.getRefreshRequest(add_params)

        request.complete = def(data):
            dom = $('<div>').html(data.responseText)
            selector = ".list-base"
            new_dom = dom.find( selector )
            new_list = new_dom.find('> ul > li')
            old_dom = self.$.find( selector ).data(new_dom.data())
            position.hide()
            position.after( new_list )
            self.onrefresh(new_list)

        self.ajax(request)
        
    def onrefresh(self, scope):
        self.isRefreshing = True
        try:
            self.$.removeClass('waiting')
            wideio_activate_everything(scope) # global function rebinding all JS on dom
            self.reactivate()
            self.options_from_dom()
            for handler in self.on_refresh_handlers:
                handler()
            self.$.trigger('refresh')
        except:
            pass
        self.isRefreshing = False

    def set_activated_btn(self, element):
        element.addClass('activated')

    def bind(self, sel_or_dom, ev, fn):
        if typeof sel_or_dom == 'string':
            dom = self.$.find(sel_or_dom)
        else:
            dom = sel_or_dom
        dom.off(ev).on(ev, fn)
        self.set_activated_btn(dom)

    def option(self, name):
        return int(self.options[name])

    def options_from_dom(self):
        dom = self.$
        try:
            data = dom.data( dom.find('.list-base').data() ).data()
            if data.pages:
                self.options["_TOTALPAGES"] = int(data.pages)
            if data.page:
                self.options["_PAGE"] = int(data.page)
            if data.perpage:
                self.options["_PERPAGE"] = int(data.perpage) or 3
            if data.sortby:
                self.options["_sortby"] = data.sortby
            if data.hidenavbar:
                self.options["_HIDE_NAVBAR"] = data.hidenavbar
            if data.infinite:
                self.options["_INFINITE"] = data.infinite
            if data.viewmode:
                self.options["_VIEW_MODE"] = data.viewmode
        except:
            pass

    def diff(self, req1, req2):
        diff = []
        $.each(req1["data"], def(key):
            if req1["data"][ key ] != req2["data"][ key ]:
                diff.push( key )
        )
        return diff

    def refreshIfChanged(self, params):
        oldrequest = self.request
        newrequest = self.getRefreshRequest(params)
        if self.diff(oldrequest, newrequest).length >= 1:
            self.refresh.apply(self, arguments)
 
    def set_event_handlers(self):
        self.options_from_dom()

        self.bind(self.$, 'refresh', def():
            self.refresh()
        )

        inputs = self.$.find("input.search-input, input.include-drafts-input").not('.activated')
        self.bind(inputs, "keyup change", def(ev):
            self.refreshIfChanged({_PAGE: 1}, 250)
        )

        self.bind("input.sortby-input", "change", def(ev):
            self.refreshIfChanged({_PAGE: 1})
        )

        self.bind("li.b-first-page a", "click", def(): 
            self.refreshIfChanged({_PAGE: 1})
        )
        
        self.bind("li.b-prev-page a", "click", def():
            self.refresh({_PAGE: self.option('_PAGE')-1})
        )
        
        self.bind("li.b-last-page a", "click", def():
            if self.options['_PAGE'] < self.options['_TOTALPAGES']:
                self.refreshIfChanged({_PAGE: self.option('_TOTALPAGES')})
        )
        
        self.bind("li.placeholder > a.next-page-replace", "click", def(ev):
            ev.stopPropagation()
            ev.preventDefault()
            li = $(this).parent()
            if self.options['_PAGE'] < self.options['_TOTALPAGES']:
                self.merge(li, {_PAGE: self.option('_PAGE') + 1})
        )

        self.bind("li.b-next-page a", "click", def():
            if self.options['_PAGE'] < self.options['_TOTALPAGES']:
                self.refresh({_PAGE: self.option('_PAGE')+1})
        )
        
        self.bind("li.b-go-page a", "click", def(ev):
            self.refresh({_PAGE: int($(ev.target).parent().data("target-page"))})
        )

        self.bind(".perpage > .pagination li > a", 'click', def(ev):
            value = int($(ev.target).text())
            self.refresh({_PAGE: 1, _PERPAGE: value, _PAGINATED: true})
            ev.stopPropagation()
            ev.preventDefault()
        )
        
        sort_ul = self.$.find("ul.sort-dropdown")
        self.bind(sort_ul.find("li"), 'click', def(ev):
            target = $(ev.target)
            elem = target.closest('li')
            if sort_ul.children(':first').data("label") == elem.data("label"):
                glyph = elem.find('.label.glyphicon')
                direction = elem.data("direction")
                glyph.removeClass('glyphicon-chevron-'+(direction=='+'?'down':'up'))
                direction = (direction=='+') ? "-" : "+"
                elem.data({direction: direction})
                glyph.addClass('glyphicon-chevron-'+(direction=='+'?'down':'up'))
            sort_ul.children().attr({"data-tip": "sort according to this criteria"})
            elem.attr({"data-tip": "click to change direction"})
            sort_ul.prepend(elem)
            sortby = sort_ul.children().map(
                def(i, e):
                    data = $(e).data()
                    return (data.direction == "+" ? "" : "-") + data.label
            ).toArray().join(",")
            self.$.find(".sortby-input").val(sortby)
            self.refresh({_PAGE: 1, _sortby: sortby})
            ev.stopPropagation()
            ev.preventDefault()
        )
wideio_activate_classes('.root-gen-list', GenericList)
%[/RS]