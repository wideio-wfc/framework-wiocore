# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
import yaml
import json

# string
# number
# int
# boolean
# object
# datetime
# date
# time


"""
In this file we define a standard format for models that can work with models but also with objects that exist on the client and the server side without necessary being models

"""

import re


def capitalcase(string):
    return ''.join([s[0].upper() + s[1:].lower() for s in string.split("_")])


def underscored(string):
    res = ""
    wasupper = True
    for c in string:
        if not wasupper:
            if c.isupper():
                res += '_'
                wasupper = True
            else:
                wasupper = False
        else:
            wasupper = c.isupper()
        res += c.lower()
    return res

###
# TRANSFORM USEFUL FOR CHANGING ONE TEMPATE LANGUAGE INTO ANOTHER
###


def prefix_variable(txt, variable_name="object"):
    return re.sub(r'\{\{([^}]*)\}\}', lambda x: '{{' +
                  variable_name + '.' + x.group(1).strip() + '}}', txt)


def deprefix(prefix, txt):
    if txt.startswith(prefix):
        return txt[len(prefix):]
    else:
        return txt


def unprefix_variable(txt, variable_name="object"):
    return re.sub(r'\{\{([^}]*)\}\}', lambda x: '{{' +
                  deprefix(variable_name + ".", x.group(1).strip()) + '}}', txt)


def laget(a, k, d):
    for a in args.items():
        if a[0] == k:
            return a[1]
    return d


def django_string_args(args):
    attrs = []
    for a in args.items():
        if a[0] in ["max_length", "default"]:
            attrs.append("%s=%r" % (a[0], a[1]))
    return ",".join(attrs)


DjangoConverterRev = {
    ###
    # COREFIELD FIELD TYPES
    ###

    "BooleanField": ("bool", []),
    "CharField": ("char[]", ['max_length', 'default']),
    "TextField": ("string", ['max_length', 'default']),
    "URLField": ("string", ['max_length', 'default'], {'django_type': "URLField"}),
    "IntegerField": ("int", []),
    "ForeignKey": ("biref", ['rel']),
    "ManyToManyKey": ("list<biref>", ['rel']),
    ###
    # EXTENDED FIELD TYPES
    ###
    'RerankableParagraphField': ("rerankable")
}

DjangoConverter = {
    ###
    # COREFIELD FIELD TYPES
    ###

    "bool": lambda a: "BooleanField(%s)" % ("",),
    "int": lambda a: "IntegerField(%s)" % ("",),
    "string": lambda a: "%s(%s)" % (laget(a, "django_type", "TextField"), django_string_args(a),),
    "char[]": lambda a: "CharField(%s)" % (django_string_args(a),),
    "float": lambda a: "FloatField(%s)" % ("",),
    "ref": lambda a: "ForeignKey(%s)" % ("",),
    # < bidrectional reference (i.e.possible to get the set of...)
    "biref": lambda a: "ForeignKey(%s)" % ("",),
    # < bidrectional reference (i.e.possible to get the set of...)
    "list<biref>": lambda a: "ManyToManyKey(%s)" % ("",),
    ###
    # EXTENDED FIELD TYPES
    ###
    "rerankable": lambda a: "RerankableParagraphField(%s)" % ("",)
}

GeddyConverter = {
    "int": lambda a: "int",
    "string": lambda a: "\"string\"" % (django_string_args(a)),
    "char[]": lambda a: "\"string\"",
    "float": lambda a: "number",
    "ref": lambda a: "",
    #"rerankable":lambda a:"RerankableParagraphField()",
}


# FIELD STARTING BY DJANGO ARE DJANGO SPECIFIC ETC...

class ModelTemplate:

    def __init__(self):
        self.descriptor = None
        self.fields = []
        self.virtual_fields = []
        self.actions = []
        self.server_side_events = []
        self.client_side_events = []

    def read_json(self, string):
        self.descriptor = json.loads(string)

    def read_yaml(self, string):
        self.descriptor = yaml.load(string)

    def read_django(self, lines):
        self.descriptor = {}
        for l in filter(
                lambda x: len(x.strip()) and x.strip()[0] != '#', lines.split("\n")):
            g = re.match(
                r"([a-zA-Z_0-9]+)([ \t]*)=([ \t]*)([a-zA-Z_0-9.]+)\((.*)\)",
                l.strip())

            def args_kwargs(*args, **kwargs):
                return args, kwargs
            try:
                da, kw = eval("args_kwargs(" + g.group(5) + ")")
            except:
                FL = [
                    lambda x:re.sub(
                        r"ForeignKey\(([A-Z][A-Za-z_]*)([, )])",
                        lambda G:"ForeignKey(\"%s%s\"" %
                        (G.group(1),
                         G.group(2)),
                        x),
                    lambda x:re.sub(
                        r"ManyToManyKey\(([A-Z][A-Za-z_]*)([, )])",
                        lambda G:"ManyToManyKey(\"%s%s\"" %
                        (G.group(1),
                         G.group(2)),
                        x),
                ]
                da, kw = eval(
                    "args_kwargs(" + reduce(lambda b, f: f(b), FL, g.group(5)) + ")")
            dcr = DjangoConverterRev[(g.group(4))]
            da = zip(dcr[1][:len(da)], da)
            kw.update(dict(da))
            if (len(dcr) > 2):
                kw.update(dcr[2])
            self.descriptor[g.group(1)] = {'type': dcr[0], 'attrs': kw}

    def emit_django(self, outputf):
        outputf("### " + ",".join(self.descriptor.keys()) + "\n")
        for k in self.descriptor.keys():
            # print self.descriptor[k]
            t = self.descriptor[k]['type'].strip()
            a = self.descriptor[k].get('attrs', {})
            outputf(unicode(k + "=" + DjangoConverter[t](a)) + "\n")

    def emit_geddy(self, string, outputf):
        for k in self.descriptor.keys():
            t = self.descriptor[k]['type'].strip()
            a = self.descriptor[k].get('attrs', {})
            outputf(GeddyConverter[t](a) + "\n")

    def emit_jaydata(self, string, outputf):
        for k in self.descriptor.keys():
            t = self.descriptor[k]['type'].strip()
            a = self.descriptor[k].get('attrs', {})
            outputf(JaydataConverter[t](a) + "\n")

if __name__ == "__main__":
    m = ModelTemplate()
    m.read_django("""
    ## detailed content_accebility
    detail_visibility = ForeignKey('PermissionStrategy', related_name="detail_visibility_rel")
    ## is this displayed in normal search
    listing_visibility = ForeignKey('PermissionStrategy', related_name="listing_visibility_rel")
""")
    print m.descriptor
