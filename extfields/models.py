# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- coding: utf-8 -*-

MODELS = []

import extfields
import os
import sys
ep0 = extfields.__path__[0]
for d in os.listdir(ep0):
    if (os.path.isdir(os.path.join(ep0, d))):
        #sys.stderr.write("?? %s\n"%(d,))
        if os.path.exists(os.path.join(ep0, d, "models.py")):
            #sys.stderr.write(">> %s\n"%(d,))
            try:
                m = __import__(
                    "extfields.%s.models" %
                    (d), fromlist=[
                        "extfields", d])
            except Exception as e:
                sys.stderr.write("internal exeception\n")
                raise 
            __module = locals()
            #sys.stderr.write("ok %s\n"%(d,))
            for f in dir(m):
                if f not in __module:
                    __module[f] = getattr(m, f)
                else:
                    if f == "MODELS":
                        __module[f] = __module[f] + getattr(m, f)

for m in MODELS:
    m.app_name = "extfields"
    if hasattr(m._meta, "model_name"):
        m._meta.model_name = m.__name__  # "models"
    else:
        m._meta.module_name = m.__name__
    m._meta.app_label = "extfields"
    m._meta.db_table = ("%s_%s" % (m.app_name, m.__name__)).lower()
    m.__module__ = "extfields"
    #print (m._meta.__dict__)
    #import sys
    # sys.stdin.readline()


def on_load_postprocess(cache):
    # THIS WILL CREATE A CICULAR DEP... THIS NEEDS TO BE DONE SOMEWHERE ELSE
    #from django.db.models import loading as ldr
    try:
        if callable(cache.app_labels['wioframework']):
            cache.app_labels['wioframework'] = []
        #  cache.app_labels['lib']=cache.app_labels['lib']()
        if not m in cache.app_labels['wioframework']:
            cache.app_labels['wioframework'].append(m)
        cache.register_models('wioframework', *MODELS)
        # print cache.app_labels['lib']
    except Exception as e:
        print e
