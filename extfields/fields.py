# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- coding: utf-8 -*-
from django import forms
from django.template.loader import render_to_string
from settings import SITE_URL
#from django.forms.fields import Field

# Let us override field's default behaviour by permitting preliminary (that is, incomplete or missing) values
# class ExtendedField(Field):
# def __init__(self, *args, **kwargs):
#super(ExtendedField,self).__init__(*args, **kwargs)
#self.preliminary = False

import copy
from django.db import models
from django.core.serializers.json import DjangoJSONEncoder
from django.utils.translation import ugettext_lazy as _
try:
    from django.utils import six
except ImportError:
    import six


from wioframework import wjson as json

from django.forms import fields
try:
    from django.forms.utils import ValidationError
except ImportError:
    from django.forms.util import ValidationError


from wioframework.fields import *


class SelectPlus(forms.Select):

    def render(self, name, *args, **kwargs):
        original = super(SelectPlus, self).render(name, *args, **kwargs)
        if not self.model_name:
            self.model_name = name
        added = render_to_string(
            "generic/add_model_entry_button.html",
            {
                'added_model': self.model_name.lower(),
                'SITE_URL': SITE_URL})
        return original + added

    def __init__(self, model_name=None, *args, **kwargs):
        super(SelectPlus, self).__init__(*args, **kwargs)
        self.model_name = model_name


class MultipleSelectPlus(forms.SelectMultiple):

    def render(self, name, *args, **kwargs):
        original = super(
            MultipleSelectPlus,
            self).render(
            name,
            *args,
            **kwargs)
        if not self.model_name:
            self.model_name = name
        added = render_to_string(
            "generic/add_model_entry_button.html", {'added_model': name.lower(), 'SITE_URL': SITE_URL})
        return original + added

    def __init__(self, model_name=None, *args, **kwargs):
        super(MultipleSelectPlus, self).__init__(*args, **kwargs)
        self.model_name = model_name


# class ResizedImageFieldFile(ImageFieldFile):

    # def save(self, name, content, save=True):
      # try:
        #new_content = StringIO()
        # content.file.seek(0)

        #img = Image.open(content.file)
        # img.thumbnail((
        # self.field.max_width,
        # self.field.max_height
        #), Image.ANTIALIAS)
        #img.save(new_content, format=self.field.format)

        #new_content = ContentFile(new_content.getvalue())
        #new_name = _update_ext(name, self.field.format.lower())

        #super(ResizedImageFieldFile, self).save(new_name, new_content, save)
      # except Exception,e:
        # TODO : LOG ERROR
        # content=None


import extfields
import os
import time
ep0 = extfields.__path__[0]
for d in os.listdir(ep0):
    if (os.path.isdir(os.path.join(ep0, d))):
        if os.path.exists(os.path.join(ep0, d, "fields.py")):
            m = __import__(
                "extfields.%s.fields" %
                (d), fromlist=[
                    "extfields", d])
            __module = locals()
            for f in dir(m):
                if f not in __module:
                    c = getattr(m, f)
                    if hasattr(c, "__name__") and c.__name__.endswith(
                            "Field") and "django.db.models.fields" not in c.__module__:
                        # print c.__module__
                        # time.sleep(1)
                        c.__module__ = "extfields.fields"
                    __module[f] = c
                    # m.app_name="lib"
                    # m._meta.app_name="lib"


try:
    from south.modelsinspector import add_introspection_rules
    __module = locals()
    for m in filter(lambda x: x[0].endswith("Field"), __module.items()):
        add_introspection_rules(
            [], [
                "^extfields\.fields\." + m[0]])

    #add_introspection_rules([], ["^lib\.fields\.(JSONField|JSONCharField)"])
except ImportError:
    pass
