/******************************************************************************
* |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
*                          ..-.:.:...
*                       :.-- -     ..:...
*                   :.:. -             -.:...
*               :.:. -                     ..:...
*           :.:. .            _;:__.          ...-...
*       :.:. .               :;    -+_    -|       .--...
*   :.-- .                    -=      -~-.-           -.:...
* -:...              ___.      -=_                        -.--
* ...    .          =;  --=_     :=                   ..   ...
* .-.      . .              ~-___=;               . -      .:.
* ...           -.                             -.          ...
* .:.                .                    . .              .:.
* ...                   -.             -.                  ...
* .:.                      ...    . -                -~4>  .:.
* ...       _^+_.              -.                       2  ...
* .:.           ~,              .                 /'   _(  .:.
* ....           <              -          +'  ^LJ>   _^   ...
* .:..          _);             -     _   J   _/  ~~-'    .:.
* ....        _&i^i             .   _~_, <(   .^           ...
*  :.       _v>^  <             .  _X~'  -s,               .:.
*  :.             -=            .   S      ^'              ...
*  :....           -=_  ,       .   2                    ..-.:
*     :....          -^^        .                    .-.:. .
*        ..:...                 .                 ..:. -
*            -.:...             .           . :.-- -
*                 :....         .         -.-- .
*                    -.:...     .    ..:.: -
*                         -.:......--. -
*                             -.:. .
* 
* Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
* http://wide.io/
* ----------------------------------------------------------------------
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License.
*     
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*     
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see http://www.gnu.org/licenses/
* This work is released under GPL v 3.0
* ----------------------------------------------------------------------
* For all information : copyrights@wide.io
* ----------------------------------------------------------------------
* 
* |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
******************************************************************************/
// DEPRECATED TO BE MOVED TO WIDE IOJS



$(window).load(function() { 
	$("#loader").delay(500).fadeOut(); 
	$(".mask").delay(1000).fadeOut("slow");
});



// ----------------------------------------------- BASE LIBRARY FUNCTIONS ------------------------------------------------------------------------
function loadCss(url) {
    var link = document.createElement("link");
    link.type = "text/css";
    link.rel = "stylesheet";
    link.href = url;
    document.getElementsByTagName("head")[0].appendChild(link);
}


function init() {
    $('input.date-input').datepicker();

    $('.list-elements .sortby-buttons').each(function (index, elem) { new SortbyButtons(elem); });

    update_components();

    $(document).ajaxComplete(function(event, xhr, settings) {
        if (settings.url !== '/socialnetwork/notificationcount/') {
            update_components();
        }
    });
    
}

function update_list(element) {
    element = $(element);
    search = element.children('.search-query').val();
    if (search === undefined) {
        search = '';
    }
    var new_ajax_url = element.attr('data-url') + '?' + 'AJAX=2&PAGE=1'
                                                      + '&search=' + search
                                                      + '&sortby=' + element.attr('data-sortby');
    element.children('.list-elements-inner').load(new_ajax_url);
}



/* Invoked when the elements of a sortable filter chain are re-arranged (onDrop) */
// function reorderIndex(args) {
//   console.log("Index reordered");
//   console.log(args);
//   var list = self.element.parents('.list-elements');
//   list.attr('data-sortby', request);
//   update_list(list);
// }





function SortbyChains(element) {
  var self = this;
  this.element = $(element);
}

// functions related to sort
function SortbyButtons(element) {
    var self = this;
    this.element = $(element);
    this.buttons = this.element.find('.sortby-button');
    this.cycle_order = ['asc', 'desc'];

    if (this.buttons.length > 1) {
        this.cycle_order = ['asc', 'desc', ''];
    }

    function update_sortby() {
        var request = '';
        self.buttons.each(function (idx, button) {
            var button = $(button);
            button.find('.sortby-button-icon').removeClass('icon-arrow-up').removeClass('icon-arrow-down');
            var order_by = self.cycle_order[button.attr('data-orderby')];
	    console.log(order_by);
            if (order_by === 'asc') {
                request += button.attr('data-label') + ':';
                button.find('.sortby-button-icon').addClass('icon-arrow-up');
            }
            else if (order_by === 'desc') {
                request += '-' + button.attr('data-label') + ':';
                button.find('.sortby-button-icon').addClass('icon-arrow-down');
            }
        });
	console.log(request);
        var list = self.element.parents('.list-elements');
        list.attr('data-sortby', request);
        update_list(list);
    }

    function click(event) {
        var button = $(this);
        var orderby = parseInt(button.attr('data-orderby'));
	console.log(orderby);	
        orderby += 1;
        if (orderby >= self.cycle_order.length) {
            orderby = 0;
        }
        button.attr('data-orderby', orderby);
        update_sortby();
    }
    this.buttons.click(click);
}

$(document).ready(init);
