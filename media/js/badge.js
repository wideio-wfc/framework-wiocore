/******************************************************************************
* |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
*                          ..-.:.:...
*                       :.-- -     ..:...
*                   :.:. -             -.:...
*               :.:. -                     ..:...
*           :.:. .            _;:__.          ...-...
*       :.:. .               :;    -+_    -|       .--...
*   :.-- .                    -=      -~-.-           -.:...
* -:...              ___.      -=_                        -.--
* ...    .          =;  --=_     :=                   ..   ...
* .-.      . .              ~-___=;               . -      .:.
* ...           -.                             -.          ...
* .:.                .                    . .              .:.
* ...                   -.             -.                  ...
* .:.                      ...    . -                -~4>  .:.
* ...       _^+_.              -.                       2  ...
* .:.           ~,              .                 /'   _(  .:.
* ....           <              -          +'  ^LJ>   _^   ...
* .:..          _);             -     _   J   _/  ~~-'    .:.
* ....        _&i^i             .   _~_, <(   .^           ...
*  :.       _v>^  <             .  _X~'  -s,               .:.
*  :.             -=            .   S      ^'              ...
*  :....           -=_  ,       .   2                    ..-.:
*     :....          -^^        .                    .-.:. .
*        ..:...                 .                 ..:. -
*            -.:...             .           . :.-- -
*                 :....         .         -.-- .
*                    -.:...     .    ..:.: -
*                         -.:......--. -
*                             -.:. .
* 
* Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
* http://wide.io/
* ----------------------------------------------------------------------
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License.
*     
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*     
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see http://www.gnu.org/licenses/
* This work is released under GPL v 3.0
* ----------------------------------------------------------------------
* For all information : copyrights@wide.io
* ----------------------------------------------------------------------
* 
* |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
******************************************************************************/
function wideio_xhr_load(url, callback) {
        var xhr;
         
        if(typeof XMLHttpRequest !== 'undefined') xhr = new XMLHttpRequest();
        else {
            var versions = ["MSXML2.XmlHttp.5.0", 
                            "MSXML2.XmlHttp.4.0",
                            "MSXML2.XmlHttp.3.0", 
                            "MSXML2.XmlHttp.2.0",
                            "Microsoft.XmlHttp"]
 
             for(var i = 0, len = versions.length; i < len; i++) {
                try {
                    xhr = new ActiveXObject(versions[i]);
                    break;
                }
                catch(e){}
             } // end for
        }
         
        function ensureReadiness() {
            if(xhr.readyState < 4) {
                return;
            }
             
            if(xhr.status !== 200) {
                return;
            }
 
            // all is well  
            if(xhr.readyState === 4) {
                callback(xhr);
            }           
        }
         
        xhr.onreadystatechange = ensureReadiness;
         
        xhr.open('GET', url, true);
        xhr.send('');
/*xmlhttp.open("POST","ajax_test.asp",true);
xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
xmlhttp.send("fname=Henry&lname=Ford");
*/
}

function wideio_load_algorithm_css(link) {
    if(window.wideio_loaded_algorithm_css) return;
    var css = document.createElement('link'), domain;
    domain = link.protocol + '//' + link.hostname + (link.port ? (':' + link.port) : '');
    css.href=domain + "/media/scss/wideio/algorithm.css";
    css.rel = "stylesheet";
    document.head.appendChild(css);
    window.wideio_loaded_algorithm_css = true;
}

function wideio_on_dom_loaded() {
    var l=document.getElementsByClassName('wideio-link');
    for (var i=0;i<l.length;i++) {
        var j=i,
            link=l[j],
            href=link.getAttribute('href'),
            mode=link.getAttribute('data-mode');
        wideio_load_algorithm_css(link);
        if ((mode == undefined) ||(mode.length==0)) {
            mode="std";
        }
        var div = document.createElement('div');
        div.innerHTML="[loading ...]";
        link.parentNode.replaceChild(div,link)
        wideio_xhr_load(href+"?_AJAX=1&_VIEW="+mode, function (data) {
            console.log(data);
            div.innerHTML=data.response;
        });
    }
}

document.addEventListener('DOMContentLoaded', wideio_on_dom_loaded, false);