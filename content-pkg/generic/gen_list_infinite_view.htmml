%[divert NULL]
|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
                         ..-.:.:...
                      :.-- -     ..:...
                  :.:. -             -.:...
              :.:. -                     ..:...
          :.:. .            _;:__.          ...-...
      :.:. .               :;    -+_    -|       .--...
  :.-- .                    -=      -~-.-           -.:...
-:...              ___.      -=_                        -.--
...    .          =;  --=_     :=                   ..   ...
.-.      . .              ~-___=;               . -      .:.
...           -.                             -.          ...
.:.                .                    . .              .:.
...                   -.             -.                  ...
.:.                      ...    . -                -~4>  .:.
...       _^+_.              -.                       2  ...
.:.           ~,              .                 /'   _(  .:.
....           <              -          +'  ^LJ>   _^   ...
.:..          _);             -     _   J   _/  ~~-'    .:.
....        _&i^i             .   _~_, <(   .^           ...
 :.       _v>^  <             .  _X~'  -s,               .:.
 :.             -=            .   S      ^'              ...
 :....           -=_  ,       .   2                    ..-.:
    :....          -^^        .                    .-.:. .
       ..:...                 .                 ..:. -
           -.:...             .           . :.-- -
                :....         .         -.-- .
                   -.:...     .    ..:.: -
                        -.:......--. -
                            -.:. .

Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
http://wide.io/
----------------------------------------------------------------------
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License.
    
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.
    
You should have received a copy of the GNU General Public License
along with this program.  If not, see http://www.gnu.org/licenses/
This work is released under GPL v 3.0
----------------------------------------------------------------------
For all information : copyrights@wide.io
----------------------------------------------------------------------

|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
%[/divert]
%[PAGE]

%[winclude "templates/generic/gen_list.mml"]

{% if wholepage %}
<div class="wideio-list" data-url="{{ list_url }}">
        %[comment] INITIAL BLURB COMMENT RELATED TO THAT SECTION %[/comment]
        {% blurbtext "{{counter_p}}" %}

        %[comment] SEARCH CONTROL %[/comment]
        {% if search_enabled or sort_enabled and not hide_navbar %}
                <div class="navbar">
                        <div class="navbar-inner">
                                {% if search_enabled %}
                                        <input class="search-input search-query" type="text" placeholder="Search by {{ search_enabled }}">
                                {% else %}
                                        <input class="search-input" type="hidden" />
                                {% endif %}
                                
                                {%if sort_enabled %}
                                        <input class="sortby-input" type="hidden" />
                                
                                        <div class="btn-group dropright" data-for="inner">
                                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                        Sort by <span class="glyphicon glyphicon-chevron-down"></span>          
                                                </button>
                                                
                                                <ul class="dropdown-menu list-inline celltable sort-dropdown">
                                                        {% for f in sort_enabled %}
                                                                {% getitem sort_default f as sort %}
                                                                <li data-label="{{ f }}" data-direction="+" data-orderby="{{ sort }}">
                                                                        <a>
                                                                                <span class="label label-primary glyphicon glyphicon-chevron-down">{{f|safe}}</span>
                                                                        </a>
                                                                </li>
                                                        {% endfor %}
                                                </ul>
                                        </div>

                                {% else %}
                                        <input class="sortby-input" type="hidden" />
                                {% endif %}

                                {% if request.user.is_authenticated %}
                                        <input class="include-drafts-input" type="checkbox" data-tip="include drafts" /> 
                                {% endif %}
                        </div>
                </div>
        {% endif %}
{% endif %}

<div class="root-gen-list" data-page="{{page}}" data-perpage="{{perpage}}" data-pages="{{pages}}" data-sortby="{{sortby}}">
        {%if hits %}
                <ul class="wiolst">
                        {% for f in object_list %}
                                <li id="uuid-{{f.id}}"> {% html_view f view_mode '' transform %} 
                                        {% if f.wiostate == '' %}<div class="ribbon-wrapper-red"><div class="ribbon-red">BUG</div></div>{% endif %}
                                        {% if f.wiostate.0 == 'U' %}<div class="ribbon-wrapper-red"> <div class="ribbon-red">DRAFT</div></div>{% endif %}
                                </li>
                        {% endfor %}
                        {% if is_infinite_view and is_paginated and next <= pages %}
                                <li class="placeholder"><a class="next-page-replace" data-target="{{next}}"> Fetch more... </a></li>
                        {% endif %}
                </ul>
                {% if not is_infinite_view %}
                        <div class="hit-count">{{ hits }} {%if hits > 1 %}{{ counter_p }} {% else %} {{ counter_s }} {% endif %}</div>
                        {% endif %}
        {% else %}
            %[no_results /]
        {% endif %}
        
        %[comment] PAGER %[/comment]
        {% if is_paginated and not is_infinite_view %}
                <div class="pagination-panel">
                        <div>{{ pages }} pages ({{ perpage }} {{ counter_p }} per page).</div>
                        <div>
                                <ul class="pagination pagination-sm">
                                        <li class="b-first-page" data-target="{{first}}"><a> First </a></li>
                                        <li class="b-prev-page" data-target="{{prev}}"><a> Prev </a></li>
                                        {% for p in page_range %}
                                                {% ifequal p page %}
                                                        <li class="disabled"><a class="ctrpage"> {{ page }} </a></li>
                                                {% else %}
                                                        <li class="b-go-page" data-target-page="{{ p }}"><a class="ctrpage"> {{ p }} </a></li>
                                                {% endifequal %}
                                        {% endfor %}
                                        <li class="b-next-page" data-target="{{next}}"><a> Next </a></li>
                                        <li class="b-last-page" data-target="{{last}}"><a> Last </a></li>
                                </ul>
                        </div>
                        <div>
                                <ul class="pagination pagination-sm">
                                        <li class="b-5pp"><a> 5 </a></li>
                                        <li class="b-10pp"><a> 10 </a></li>
                                        <li class="b-25pp" ><a> 25 </a></li>
                                        <li class="b-50pp"><a> 50 </a></li>
                                </ul>
                        </div>
                </div>
        {% endif %}

        %[gen_list_add_form /]
</div>

{% if wholepage %}
</div>
{% endif %}
%[/PAGE]