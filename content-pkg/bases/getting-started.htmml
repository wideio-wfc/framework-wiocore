%[divert NULL]
|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
                         ..-.:.:...
                      :.-- -     ..:...
                  :.:. -             -.:...
              :.:. -                     ..:...
          :.:. .            _;:__.          ...-...
      :.:. .               :;    -+_    -|       .--...
  :.-- .                    -=      -~-.-           -.:...
-:...              ___.      -=_                        -.--
...    .          =;  --=_     :=                   ..   ...
.-.      . .              ~-___=;               . -      .:.
...           -.                             -.          ...
.:.                .                    . .              .:.
...                   -.             -.                  ...
.:.                      ...    . -                -~4>  .:.
...       _^+_.              -.                       2  ...
.:.           ~,              .                 /'   _(  .:.
....           <              -          +'  ^LJ>   _^   ...
.:..          _);             -     _   J   _/  ~~-'    .:.
....        _&i^i             .   _~_, <(   .^           ...
 :.       _v>^  <             .  _X~'  -s,               .:.
 :.             -=            .   S      ^'              ...
 :....           -=_  ,       .   2                    ..-.:
    :....          -^^        .                    .-.:. .
       ..:...                 .                 ..:. -
           -.:...             .           . :.-- -
                :....         .         -.-- .
                   -.:...     .    ..:.: -
                        -.:......--. -
                            -.:. .

Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
http://wide.io/
----------------------------------------------------------------------
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License.
    
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.
    
You should have received a copy of the GNU General Public License
along with this program.  If not, see http://www.gnu.org/licenses/
This work is released under GPL v 3.0
----------------------------------------------------------------------
For all information : copyrights@wide.io
----------------------------------------------------------------------

|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
%[/divert]
<section id="getting-started" class="getting-started-section section-wrapper">

<div class="section-header wow fadeIn animated" data-wow-offset="120" data-wow-duration="1.5s">
<h2 class="dark-text">
How It Works
</h2>

<div class="section-description">
Using WIDE IO is as easy as 1, 2, 3. The platform is easy to use, has quality algorithms and supports easy integration of algorithms in the form of APIs.
</div>

</div>
<div class="row infographic">
<div class="col-md-6 col-sm-6">
<div class="icon-circle">
<i class="large-logo ion-ios7-plus-outline image-section image-on-left wow pulse animated" data-wow-offset="10" data-wow-duration="1.5s">
</i>

</div>

</div>

<div class="col-md-6 col-sm-6">
<div  class="details wow fadeInRight animated" data-wow-offset="10" data-wow-duration="1.5s">
<h3>
1) Register
</h3>

Register yourself at WIDE IO. Registration is free, fast and easy, and no payment is required for registration.

<a href="/accounts/login/" class="callout-link">
Sign up to get started 
<i class="ion-arrow-right-c"></i>
</a>
</div>

</div>
</div>

<div class="row infographic">
<div class="col-md-6 col-sm-6 col-md-push-6 col-sm-push-6">
<i class="large-logo ion-ios7-search image-section image-on-right wow pulse animated" data-wow-offset="10" data-wow-duration="1.5s">
</i>
</div>

<div class="col-md-6 col-sm-6 col-md-pull-6 col-sm-pull-6">
<div class="details wow fadeInLeft animated" data-wow-offset="10" data-wow-duration="1.5s">
<h3>
2) Find
</h3>

Easily find algorithms for your software using our advanced search tool. Search for algorithms based on function, rank and/or price. You can also test the algorithms online.

<a href="/accounts/login/" class="callout-link">
Sign up to get started 
<i class="ion-arrow-right-c">
</i>
</a>
</div>
</div>
</div>

<div class="row infographic">
<div class="col-md-6 col-sm-6 ">
<div class="icon-circle">
<i class="large-logo ion-ios7-cog-outline image-section image-on-left wow pulse animated" data-wow-offset="10" data-wow-duration="1.5s">
</i>

</div>
</div>

<div class="col-md-6 col-sm-6">

        <div class="details wow fadeInRight animated" data-wow-offset="10" data-wow-duration="1.5s">
                <h3>3) Use</h3>
                Download the API for the desired algorithm. The APIs are easy to integrate into any code and you only pay when you call an API.
                <a href="/accounts/login/" class="callout-link">
                        Sign up to get started <i class="ion-arrow-right-c"></i>
                </a>
        </div>
</div>
</div>

<div class="row">
<p class="if-researcher"> If you are a researcher and wish to upload your algorithm(s), please get in touch with us at algo@wide.io </p>
</div>

</section>